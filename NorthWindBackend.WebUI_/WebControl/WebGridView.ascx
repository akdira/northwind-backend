﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebGridView.ascx.cs" Inherits="NorthWindBackend.WebUI.WebControl.WebGridView" %>

<style type="text/css">
    .previous_disable, .next_disable {
        cursor: not-allowed;
        background-color: darkgray;
    }

    .previous_disable, .next_disable, .previous, .next-paging {
        padding: 2px 8px;
    }
</style>
<div class="gridMain" id="gridBorder" runat="server" style="">
    <div id="headerLabel" runat="server">
        <div class="head clearfix">
            <div class="isw-list" style="margin: 10px 0 0 10px">
            </div>
            <h1>
                <asp:Label ID="lblGridTitle" runat="server" Text="" CssClass="gridTitleLabel" />
            </h1>
        </div>
    </div>
    <div id="gridContainer" runat="server" style="border-bottom: solid 0px #ddd; margin-bottom: 10px">
        <div id="gridTitle" class="" runat="server" role="grid" style="">
            <asp:Panel ID="searchBox" runat="server" DefaultButton="btnAdd" CssClass="row-fluid clearfix" >
                <div class="block">
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="form-horizontal">
                            <div class="form-group">
                            <label class="col-md-1 control-label" for="ddlSearchCriteria">Criteria</label>
                            <div class="col-md-3">
                                <asp:DropDownList ID="ddlSearchCriteria" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div> 
                            <div class="col-md-3">
                                <asp:TextBox ID="txtSearchCriteria" runat="server" placeholder="Search Value" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="btnAdd_Click"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn-success" OnClick="btnClear_Click"
                                    Visible="false" style="display: none;"/>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"">
                            <asp:Repeater ID="criterias" runat="server" Visible="true" OnItemCreated="criterias_ItemCreated">
                                <ItemTemplate>
                                    <asp:Literal ID="Literal1" runat="server" Text="Find" />&nbsp;<asp:Label
                                        ID="lblDesc" runat="server" Text='<%# Bind("Desc") %>'></asp:Label>&nbsp;<asp:Literal
                                            ID="Literal5" runat="server" Text="is containing" />&nbsp;
                                    <asp:Label ID="lblValue" runat="server" Text='<%# Bind("Value") %>'></asp:Label>&nbsp;
                                    <asp:LinkButton ID="lb" runat="server" CommandName="Remove" CommandArgument='<%# Bind("Key") %>'
                                        ForeColor="blue" Text="Remove" OnClick="LinkRemove_Click"></asp:LinkButton>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    <hr style="height: 5px; margin-top: 5px; margin-bottom: 0px;" />
                                </SeparatorTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <asp:Panel ID="PanelLegend" runat="server" CssClass="displaylegend" Style="margin-left: 10px">
        </asp:Panel> 
        <div style="margin-top: 15px;">
            <div id="gridContent" runat="server" class="" style="overflow-x: scroll; border: solid 0px #ccc;"> 
                <asp:GridView ID="grid" runat="server" AutoGenerateColumns="False" EmptyDataText=""
                    AllowPaging="True" AllowSorting="True" EnableViewState="True" OnRowDataBound="grid_RowDataBound"
                    OnSorting="grid_Sorting" ShowFooter="False" Width="100%" OnPageIndexChanging="grid_PageIndexChanging"
                    CssClass="table" ShowHeaderWhenEmpty="true" OnRowDeleting="grid_RowDeleting"
                    OnRowEditing="grid_RowEditing" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowUpdating="grid_RowUpdating"
                    OnRowCommand="grid_RowCommand" OnDataBound="grid_DataBound" OnSelectedIndexChanged="grid_SelectedIndexChanged"
                    GridLines="None" Style="padding: 0px;">
                    <RowStyle CssClass="" Wrap="False" VerticalAlign="Middle" />
                    <AlternatingRowStyle CssClass="" Wrap="False" VerticalAlign="Middle" BackColor="#fcfdfd" />
                    <HeaderStyle CssClass="headerstyle" Wrap="False" VerticalAlign="Middle" Height="25px" />
                    <FooterStyle CssClass="" Wrap="False" VerticalAlign="Middle" />
                    <EmptyDataRowStyle CssClass="empty" />
                    <PagerSettings Visible="False" />
                </asp:GridView>
            </div>
        </div>
        <div id="dataPager" runat="server" style="margin-top: 10px;">
            <asp:Label ID="lblTotalRecords" runat="server" Text="Total Rows:"
                CssClass="totalRecords" Style="float: left; margin-top: 6px" />
            <div id="paging" runat="server" Style="float: right">
                <asp:Label ID="lblShowRows" runat="server" Text="Page Size" />
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                    Style="width: 60px; margin-top: 6px; height: 24px;border-radius: 4px">
                    <asp:ListItem Value="5" />
                    <asp:ListItem Value="10" Selected="True" /> 
                    <asp:ListItem Value="25" />
                    <asp:ListItem Value="50" />
                    <asp:ListItem Value="100" />
                </asp:DropDownList>
                &nbsp; &nbsp;
                <asp:Literal ID="Literal3" runat="server" Text="Page " />
                <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" OnTextChanged="GoToPage_TextChanged"
                    CssClass="gotopage" Style="width: 50px; margin-top: 6px; font-size: 12px; border-radius: 4px; padding-left: 4px;" />
                <asp:Literal ID="Literal4" runat="server" Text="" />
                <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                &nbsp;
                <div class="btn-group">
                    <asp:LinkButton ID="btnPrev" runat="server" ToolTip="Prev"
                        OnClick="btnPrev_OnClick" CssClass="btn btn-default" ><i class="fa fa-chevron-left"></i> Prev</asp:LinkButton>
                    <asp:LinkButton ID="btnNext" runat="server" CommandName="Page" ToolTip="Next"
                        CommandArgument="Next" CssClass="btn btn-default" OnClick="btnNext_OnClick">Next <i class="fa fa-chevron-right"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</div>
