﻿using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using NorthWindBackend.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.MasterPages
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!Page.IsPostBack)
            {
                SetMenuAuthorization();
            }
            LinkLogo.HRef = GetBaseUrl();
            
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[Constants.USER_ID] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else if (!CheckAuthorization())
            {
                Response.Redirect("~/Pages/Home/Dashboard.aspx");
            }
            else
            {
                var user = (Users)Session[Constants.CURRENT_USER];
                if(user != null)
                {
                    LabelUserName.Text = user.UserName;
                    LabelUserNameRole.Text = string.Format("{0} - {1}", user.UserName, "");
                    var role = Session[Constants.USER_ROLE] as Role;
                    if(role != null)
                    {
                        LabelUserNameRole.Text = string.Format("{0} - {1}", user.UserName, role.RoleName);
                    }
                    
                } 
            }
        } 

        protected void ButtonLogout_Click(object sender, EventArgs e)
        { 
            var loginLog = Session[Constants.LOGIN_LOG] as LoginLog;
            if(loginLog != null)
            {
                loginLog.LoginOutTime = DateTime.Now;
                var facade = new LoginLogFacade();
                facade.Update(loginLog);
                var lFacade = new LoginUserFacade();
                lFacade.DeleteByUserName(Commons.GetCurrentLoginUser()); 
            } 
            Session.Abandon();
            Session.Clear();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            

        }

        public void SetModuleName(string name)
        {
            ModuleName.Text = name;
        }

        private void SetMenuAuthorization()
        {
            string baseUrl = GetBaseUrl();
            StringBuilder sb = new StringBuilder();

            var mFacade = new MenuFacade();
            var param = new Dictionary<string, object>();
            var rootMenu = mFacade.GetListMenuByUserID(0, Commons.GetCurrentLoginUser());
            foreach (var rm in rootMenu)
            {
                if (rm.MenuHashChild)
                {
                    var level1 = mFacade.GetListMenuByUserID(rm.MenuID, Commons.GetCurrentLoginUser());
                    sb.AppendFormat("<li class='treeview'><a href='#'><i class='{1}'></i><span>{0}</span><i class='fa fa-angle-left pull-right'></i></a>", rm.MenuName, string.IsNullOrEmpty(rm.MenuIconString) ? "fa fa-folder" : rm.MenuIconString);
                    sb.AppendFormat("<ul class='treeview-menu'>", rm.MenuID);

                    foreach (var lev1 in level1)
                    {
                        sb.Append("<li>");
                        if (lev1.MenuHashChild)
                        {
                            var level2 = mFacade.GetListMenuByUserID(lev1.MenuID, Commons.GetCurrentLoginUser());
                            sb.AppendFormat("<a href='#'><i class='{1}'></i><span>{0}</span><i class='fa fa-angle-left pull-right'></i></a>", lev1.MenuName, string.IsNullOrEmpty(lev1.MenuIconString) ? "fa fa-folder" : lev1.MenuIconString);
                            sb.AppendFormat("<ul class='treeview-menu'>");
                            foreach (var lev2 in level2)
                            {
                                sb.AppendFormat("<li><a href='{0}'><i class='{2}'>&nbsp</i>{1}</a></li>", baseUrl + lev2.MenuPath, lev2.MenuName, string.IsNullOrEmpty(lev2.MenuIconString) ? "fa fa-folder" : lev2.MenuIconString);
                            }
                            sb.Append("</ul>");
                        }
                        else
                        {
                            sb.AppendFormat("<a href='{0}'><i class='{2}'>&nbsp</i>{1}</a>", baseUrl + lev1.MenuPath, lev1.MenuName, string.IsNullOrEmpty(lev1.MenuIconString)? "fa fa-folder" : lev1.MenuIconString);
                        }
                        sb.Append("</li>");
                    }
                    sb.Append("</ul>");
                    sb.Append("</li>");
                }
                else
                {
                    sb.AppendFormat("<li><a href='{0}'><i class='{2}'></i><span>{1}</span></a></li>", rm.MenuPath.Equals("#") ? "#" : baseUrl + rm.MenuPath, rm.MenuName, string.IsNullOrEmpty(rm.MenuIconString) ? "fa fa-folder" : rm.MenuIconString);
                }
            }

            string script = string.Format("<script type='text/javascript'>$('.sidebar-menu').append(\"{0}\");</script>", sb);
            ScriptManager.RegisterStartupScript(this, typeof(string), "onload", script, false);
        }

        private string GetBaseUrl()
        {
            string[] uri = Request.Url.AbsoluteUri.Split(new string[] { "/" }, 0);
            string baseUrl = Request.Url.Port == 80 ? uri[0] + "//" + uri[2] + "/" + uri[3] + "/" : uri[0] + "//" + uri[2] + "/";
            return baseUrl;
        }

        public bool CheckAuthorization()
        {
            var facade = new MenuFacade();
            string url = Request.RawUrl.TrimStart('/');
            var menu = facade.GetAuthorizedMenuByCriteria(url, Commons.GetCurrentLoginUser());
            return true;
            //return menu != null;
        }

    }
}