﻿using NorthWindBackend.WebUI.Pages.MasterPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.Home
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Dashboard");
        }
    }
}