<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="ApplicationParameterGroupList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Admin.ApplicationParameterGroupList" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Application Parameter', 3);
        }

    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Parameter Aplikasi</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridViewApplicationParameterGroupList" runat="server" AllowPaging="true" DefaultSortExpression="ParamGroupID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("ParamGroupID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("ParamGroupID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("ParamGroupID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle Width="11.1%" />
                                        <ItemStyle Width="11.1%" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Group ID" DataField="ParamGroupID" SortExpression="ParamGroupID">
                                        <HeaderStyle Width="11.1%" />
                                        <ItemStyle Width="11.1%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Nama Group" DataField="ParamGroupName" SortExpression="ParamGroupName">
                                        <HeaderStyle Width="15%" />
                                        <ItemStyle Width="15%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Deskripsi" DataField="ParamGroupDescription" SortExpression="ParamGroupDescription">
                                        <HeaderStyle Width="20%" />
                                        <ItemStyle Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Kode" DataField="ParamGroupCode" SortExpression="ParamGroupCode">
                                        <HeaderStyle Width="11.1%" />
                                        <ItemStyle Width="11.1%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Bisa Tambah" DataField="ParamGroupAddNewFlag" SortExpression="ParamGroupAddNewFlag">
                                        <HeaderStyle Width="11.1%" />
                                        <ItemStyle Width="11.1%" />
                                    </asp:BoundField>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="applicationparametergroupdetail" class="modal fade" role="dialog">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Parameter Aplikasi Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1-1" data-toggle="tab">Parameter Detail</a></li>
                            <li><a href="#tab_2-2" data-toggle="tab">Parameter Item</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1-1">
                                <div class="row">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-12 form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">GroupID</label>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="TextParamGroupID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">Nama Group </label>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="TextParamGroupName" runat="server" CssClass="form-control" MaxLength="125" placeholder="Harus diisi (Maks 125 charater)"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">Deskripsi </label>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="TextParamGroupDescription" runat="server" CssClass="form-control" placeholder="Harus diisi(Maks 125 charater)" MaxLength="125"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">Kode </label>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="TextParamGroupCode" runat="server" CssClass="form-control" MaxLength="3" placeholder="Harus diisi (Maks 3 charater)"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">Bisa Tambah </label>
                                                    <div class="col-md-8 checkbox">
                                                        <label>
                                                            <asp:CheckBox ID="CheckParamGroupAddNewFlag" runat="server" />
                                                            Aktif</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">ModifiedBy </label>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="TextModifiedBy" runat="server" CssClass="form-control freeze"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left">ModifiedDate </label>
                                                    <div class="col-md-4">
                                                        <div class="input-group date">
                                                            <asp:TextBox ID="TextModifiedDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                            <div class="tab-pane" id="tab_2-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <grv:WebGridView ID="GridViewApplicationParameterList" runat="server" AllowPaging="true" DefaultSortExpression="ParamID"
                                                    DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("ParamID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                                                <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("ParamID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                                                <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("ParamID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="10%" />
                                                            <ItemStyle Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="ParamID" DataField="ParamID" SortExpression="ParamID">
                                                            <HeaderStyle Width="10%" />
                                                            <ItemStyle Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ParamCode" DataField="ParamCode" SortExpression="ParamCode">
                                                            <HeaderStyle Width="10%" />
                                                            <ItemStyle Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ParamName" DataField="ParamName" SortExpression="ParamName">
                                                            <HeaderStyle Width="30%" />
                                                            <ItemStyle Width="30%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ParamValue" DataField="ParamValue" SortExpression="ParamValue">
                                                            <HeaderStyle Width="10%" />
                                                            <ItemStyle Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ParamDefault" DataField="ParamDefault" SortExpression="ParamDefault">
                                                            <HeaderStyle Width="10%" />
                                                            <ItemStyle Width="10%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ParamStatus" DataField="ParamStatus" SortExpression="ParamStatus">
                                                            <HeaderStyle Width="10%" />
                                                            <ItemStyle Width="10%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </grv:WebGridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
                            <asp:LinkButton ID="ButtonClose" runat="server" CssClass="btn btn-danger" OnClientClick="$('#applicationparametergroupdetail').modal('hide')"><i class="fa fa-remove">&nbsp;</i>Tutup</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>


        </div>

    </div>
</asp:Content>
