using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.Pages.MasterPages;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class ErrorLogList : System.Web.UI.Page
    {  
        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Admin");
             
            GridViewErrorLogList.PageFunction = PageFunction;

            if (!Page.IsPostBack)
            {
                GridViewErrorLogList.DisplayData();
            }
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            ErrorLogDetail1.DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            ErrorLogDetail1.PageState = PageState.View;
            ErrorLogDetail1.ProcessDetail();
            Message.RunScript(this, "$('#errorlogdetail').modal()");
        } 

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            ErrorLogDetail1.DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            ErrorLogDetail1.PageState = PageState.Edit;
            ErrorLogDetail1.ProcessDetail();
            Message.RunScript(this, "$('#errorlogdetail').modal()"); 
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ErrorLogDetail1.DataID = 0;
            ErrorLogDetail1.PageState = PageState.New;
            ErrorLogDetail1.ProcessDetail();
            Message.RunScript(this, "$('#errorlogdetail').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete(dataID);
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridViewErrorLogList;
            var facade = new ErrorLogFacade(); 
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);            
        } 
		
		public void ProcessDelete(int dataID)
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ErrorLogFacade();
                ActionResult result = facade.DataDelete(dataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridViewErrorLogList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        } 

        private bool ValidOnDelete()
        {
            return true;
        } 
    }
}
