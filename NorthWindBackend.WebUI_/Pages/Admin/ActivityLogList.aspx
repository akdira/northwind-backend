<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="ActivityLogList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Admin.ActivityLogList" %>
<%@ Register Src="~/Pages/Admin/ActivityLogDetail.ascx" TagName="ActivityLogDetail" TagPrefix="dtl" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Activity Log', 3);

            $("#buttonTest").click(function () {
                $('#<%=ButtonAdd.ClientID%>').click();
            });
            
        } 
        
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">ActivityLog List</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>                            
                            <button id="buttonTest" class="btn btn-primary btn-sm">Click Me</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridViewActivityLogList" runat="server" AllowPaging="true" DefaultSortExpression="ActivityID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("ActivityID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("ActivityID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("ActivityID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
									<asp:BoundField HeaderText="ActivityID" DataField="ActivityID" SortExpression="ActivityID" >
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ActivityUser" DataField="ActivityUser" SortExpression="ActivityUser" >
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ActivityIP" DataField="ActivityIP" SortExpression="ActivityIP" >
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ActivityDescription" DataField="ActivityDescription" SortExpression="ActivityDescription" >
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ActivityDate" DataField="ActivityDate" SortExpression="ActivityDate" DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="false">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="activitylogdetail" class="modal fade" role="dialog">
        <dtl:ActivityLogDetail ID="ActivityLogDetail1" runat="server" />
    </div>
</asp:Content>
