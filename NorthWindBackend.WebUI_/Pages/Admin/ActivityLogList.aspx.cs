using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.Pages.MasterPages;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class ActivityLogList : System.Web.UI.Page
    {  
        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Admin");
             
            GridViewActivityLogList.PageFunction = PageFunction;

            if (!Page.IsPostBack)
            {
                GridViewActivityLogList.DisplayData();
            }
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            ActivityLogDetail1.DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            ActivityLogDetail1.PageState = PageState.View;
            ActivityLogDetail1.ProcessDetail();
            Message.RunScript(this, "$('#activitylogdetail').modal()");
        } 

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            ActivityLogDetail1.DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            ActivityLogDetail1.PageState = PageState.Edit;
            ActivityLogDetail1.ProcessDetail();
            Message.RunScript(this, "$('#activitylogdetail').modal()"); 
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ActivityLogDetail1.DataID = 0;
            ActivityLogDetail1.PageState = PageState.New;
            ActivityLogDetail1.ProcessDetail();
            Message.RunScript(this, "$('#activitylogdetail').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete(dataID);
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridViewActivityLogList;
            var facade = new ActivityLogFacade(); 
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);            
        } 
		
		public void ProcessDelete(int dataID)
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = facade.DataDelete(dataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridViewActivityLogList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        } 

        private bool ValidOnDelete()
        {
            return true;
        } 
    }
}
