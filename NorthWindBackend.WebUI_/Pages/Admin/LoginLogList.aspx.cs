using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.Pages.MasterPages;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class LoginLogList : System.Web.UI.Page
    { 
        private PageState PageStateDetail
        {
            get
            {
                if (ViewState["PAGESTATE_DETAIL"] == null)
                {
                    ViewState["PAGESTATE_DETAIL"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE_DETAIL"];
            }
            set
            {
                ViewState["PAGESTATE_DETAIL"] = value;
            }
        }

        private LoginLog OLoginLog
        {
            get
            {
                if(ViewState["OLOGINLOG"] == null)
                {
                    return null;
                }
               return  ViewState["OLOGINLOG"] as LoginLog;
            }
            set
            {
                ViewState["OLOGINLOG"] = value;
            }
        }

        private int DataID
        {
            get
            {
                if(ViewState["DATA_ID"] == null)
                {
                    return 0;
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Admin");
             
            GridViewLoginLogList.PageFunction = PageFunction;

            if (!Page.IsPostBack)
            {
                GridViewLoginLogList.DisplayData();
            }
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            PageStateDetail = PageState.View;
            ProcessDetail();
            Message.RunScript(this, "$('#loginlogdetail').modal()");
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessSave();
            }
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            PageStateDetail = PageState.Edit;
            ProcessDetail();
            Message.RunScript(this, "$('#loginlogdetail').modal()"); 
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            DataID = 0;
            PageStateDetail = PageState.New;
            ProcessDetail();
            Message.RunScript(this, "$('#loginlogdetail').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridViewLoginLogList;
            var facade = new LoginLogFacade(); 
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);            
        } 

        private void ProcessDetail()
        {
            switch (PageStateDetail)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OLoginLog = new LoginLog();
        }

        private void PopulateControlsInNewState()
        {
            TextLoginID.Text = "";
			TextLoginUser.Text = "";
			TextLoginIP.Text = "";
			TextLoginTime.Text = "";
			TextLoginOutTime.Text = "";
			TextLoginDescription.Text = "";
			
        }

        private void AdjustControlInNewState()
        {
            TextLoginID.ReadOnly = false;
			TextLoginUser.ReadOnly = false;
			TextLoginIP.ReadOnly = false;
			TextLoginTime.ReadOnly = false;
			TextLoginOutTime.ReadOnly = false;
			TextLoginDescription.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
            var facade = new LoginLogFacade();
            OLoginLog = facade.GetObject(DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OLoginLog != null)
            {
                TextLoginID.Text = OLoginLog.LoginID + "";
				TextLoginUser.Text = OLoginLog.LoginUser;
				TextLoginIP.Text = OLoginLog.LoginIP;
				TextLoginTime.Text = Commons.GetDateString(OLoginLog.LoginTime);
				TextLoginOutTime.Text = Commons.GetDateString(OLoginLog.LoginOutTime);
				TextLoginDescription.Text = OLoginLog.LoginDescription;
				
            }
        }

        private void AdjustControlInEditState()
        {
            TextLoginID.ReadOnly = false;
			TextLoginUser.ReadOnly = false;
			TextLoginIP.ReadOnly = false;
			TextLoginTime.ReadOnly = false;
			TextLoginOutTime.ReadOnly = false;
			TextLoginDescription.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new LoginLogFacade();
            OLoginLog = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OLoginLog != null)
            {
                TextLoginID.Text = OLoginLog.LoginID + "";
				TextLoginUser.Text = OLoginLog.LoginUser;
				TextLoginIP.Text = OLoginLog.LoginIP;
				TextLoginTime.Text = Commons.GetDateString(OLoginLog.LoginTime);
				TextLoginOutTime.Text = Commons.GetDateString(OLoginLog.LoginOutTime);
				TextLoginDescription.Text = OLoginLog.LoginDescription;
				
            }
        }

        private void AdjustControlInViewState()
        {
            TextLoginID.ReadOnly = true;
			TextLoginUser.ReadOnly = true;
			TextLoginIP.ReadOnly = true;
			TextLoginTime.ReadOnly = true;
			TextLoginOutTime.ReadOnly = true;
			TextLoginDescription.ReadOnly = true;
			
            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new LoginLogFacade();
                ActionResult result = null;
                PopulateObject();
                if(PageStateDetail == PageState.New)
                {
                    result = facade.DataInsert(OLoginLog);
                }
                else
                {
                    result = facade.DataUpdate(OLoginLog);
                }
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage, "$('#loginlogdetail').modal('hide')");
                    GridViewLoginLogList.DisplayData();
                }
                else
                {
                    Message.Show(this, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
					OLoginLog.LoginUser = TextLoginUser.Text;
				OLoginLog.LoginIP = TextLoginIP.Text;
				OLoginLog.LoginTime = Commons.GetDateTime(TextLoginTime.Text);
				OLoginLog.LoginOutTime = Commons.GetDateTime(TextLoginOutTime.Text);
				OLoginLog.LoginDescription = TextLoginDescription.Text;
				
            }
            catch (Exception ex) { }
        } 

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty; 
			if (string.IsNullOrEmpty(TextLoginUser.Text))
            { 
                msgList.Add(new ErrorMessage(TextLoginUser.ClientID, "LoginUser harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextLoginIP.Text))
            { 
                msgList.Add(new ErrorMessage(TextLoginIP.ClientID, "LoginIP harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextLoginTime.Text))
            { 
                msgList.Add(new ErrorMessage(TextLoginTime.ClientID, "LoginTime harus diisi!", true));
            } 
			 

            if (msgList.Count == 0)
            {
                return true;
            }
            else { 
                Message.ShowErrorMessage(this, msgList); 
                return false;
            }
        }

        private void ProcessDelete()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new LoginLogFacade();
                ActionResult result = facade.DataDelete(DataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridViewLoginLogList.DisplayData();
                }
                else
                {
                    Message.Show(this, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        } 
    }
}
