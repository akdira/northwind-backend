﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace NorthWind.Entity
{
    [TableName("Customers")]
    [PrimaryKey("CustomerID")]
    [ExplicitColumns]
    [Serializable]

    public class Customers
    {
        [Column]
        public string CustomerID { get; set; }

        [Required] [Column]
        public string CompanyName { get; set; }

        [Required] [Column]
        public string ContactName { get; set; }

        [Column]
        public string ContactTitle { get; set; }

        [Required] [Column]
        public string Address { get; set; }

        [Required] [Column]
        public string City { get; set; }

        [Required] [Column]
        public string Region { get; set; } 

        [Required] [Column]
        public string PostalCode { get; set; }

        [Required] [Column]
        public string Country { get; set; }

        [Required] [Column]
        public string Phone { get; set; }

        [Column]
        public string Fax { get; set; }


    }
}
