﻿
/*
'===============================================================================
'  PetaPoco Entity Class
'  Generated By Muhamad Nur
'
'===============================================================================
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWindBackend.Entity
{
    [PetaPoco.TableName("Role")]
    [PetaPoco.PrimaryKey("RoleID")]
    [PetaPoco.ExplicitColumns]
    [Serializable]
    public class Role
    {
        #region Property

        [PetaPoco.Column]
        public int RoleID { get; set; }

        [PetaPoco.Column]
        public string RoleName { get; set; }

        [PetaPoco.Column]
        public bool RoleStatus { get; set; }

        [PetaPoco.Column]
        public string CreatedBy { get; set; }

        [PetaPoco.Column]
        public DateTime CreatedDate { get; set; }

        [PetaPoco.Column]
        public string ModifiedBy { get; set; }

        [PetaPoco.Column]
        public DateTime ModifiedDate { get; set; }

        #endregion
    }
}
