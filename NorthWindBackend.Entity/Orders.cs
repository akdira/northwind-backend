﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace NorthWindBackend.Entity
{
    [TableName("Orders")]
    [PrimaryKey("OrderID")]
    [ExplicitColumns]
    [Serializable]

    public class Orders
    {
        [Column]
        public int OrderID { get; set; }

        [Required] [Column]
        public string CustomerID { get; set; }

        [Required] [Column]
        public int EmployeeID { get; set; }

        [Column]
        public DateTime OrderDate { get; set; }

        [Required] [Column]
        public DateTime RequiredDate { get; set; }

        [Column]
        public DateTime ShippedDate { get; set; }

        [Column]
        public int ShipVia { get; set; }

        [Column]
        public double Freight { get; set; }

        [Column]
        public string ShipName { get; set; }

        [Column]
        public string ShipAddress { get; set; }

        [Column]
        public string ShipCity { get; set; }

        [Column]
        public string ShipRegion { get; set; }

        [Column]
        public string ShipPostalCode { get; set; }

        [Column]
        public string ShipCountry { get; set; }

    }
}
