
/*
'===============================================================================
'  PetaPoco Entity Class
'  Generated By Muhamad Nur
'
'===============================================================================
*/ 

using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWindBackend.Entity
{
	[PetaPoco.TableName("Users")]
	[PetaPoco.PrimaryKey("UserID")]
	[PetaPoco.ExplicitColumns]
    [Serializable]
	public class Users 
	{		
		#region Property
		
		[PetaPoco.Column]	
		public string UserID{ get; set;}
		
		[PetaPoco.Column]	
		public string UserName{ get; set;}
		
		[PetaPoco.Column]	
		public string UserPassword{ get; set;}
		
		[PetaPoco.Column]	
		public string UserEmail{ get; set;}
		
		[PetaPoco.Column]	
		public string UserPosition{ get; set;}
		
		[PetaPoco.Column]	
		public string UserManager{ get; set;}
		
		[PetaPoco.Column]	
		public bool UserStatus{ get; set;}
		
		[PetaPoco.Column]	
		public int? DepartmentID{ get; set;}

        private byte[] _photo;
		[PetaPoco.Column]	
		public byte[] Phote{
            get
            {
                if (_photo == null)
                {
                    _photo = new byte[0];
                }
                return _photo;
            }
            set { _photo = value; }
        } 

		[PetaPoco.Column]	
		public string CreatedBy{ get; set;}
		
		[PetaPoco.Column]	
		public DateTime CreatedDate{ get; set;}
		
		[PetaPoco.Column]	
		public string ModifiedBy{ get; set;}
		
		[PetaPoco.Column]	
		public DateTime ModifiedDate{ get; set;}
		
		#endregion		
	}
}
