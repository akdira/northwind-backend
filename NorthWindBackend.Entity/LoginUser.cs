﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWindBackend.Entity
{
    [PetaPoco.TableName("LoginUser")]
    [PetaPoco.PrimaryKey("UserName")]
    [PetaPoco.ExplicitColumns]
    [Serializable]
    public class LoginUser
    {
        [PetaPoco.Column]
        public string UserName { get; set; }

        [PetaPoco.Column]
        public string SessionID { get; set; }
        [PetaPoco.Column]
        public string HostAddress { get; set; }

    }
}
