﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthWind.Entity
{
    [TableName("MA_AppParameter")]
    [PrimaryKey("ID")]
    [ExplicitColumns]
    [Serializable]

    public class AppParameter
    {
        
        [Column] [Key]
        public int? ID { get; set; }

        [Column] [Required]
        public string ParameterName { get; set; }

        [Column] [Required]
        public int Min { get; set; }

        [Column] [Required]
        public int Max { get; set; }
        
    }
}
