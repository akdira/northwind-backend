﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace NorthWindBackend.Entity
{
    [TableName("Shippers")]
    [PrimaryKey("ShipperID")]
    [ExplicitColumns]
    [Serializable]

    public class Shippers
    {
        [Column]
        public int ShipperID { get; set; }

        [Required] [Column]
        public string CompanyName { get; set; }

        [Column]
        public int Phone { get; set; }

    }
}
