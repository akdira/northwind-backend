﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthWind.Entity
{
    [TableName("Employees")]
    [PrimaryKey("EmployeeID")]
    [ExplicitColumns]
    [Serializable]

    public class Employees
    {
        
        [Column]
        public int EmployeeID { get; set; }

        [Column] [Required]
        public string FirstName { get; set; }

        [Column]
        public string LastName { get; set; }

        [Column]
        public string Title { get; set; }

        [Column]
        public string TitleOfCourtesy { get; set; }


        [Column] [Required]
        public string BirthDate { get; set; }

        [Column]
        public string HireDate { get; set; }


        [Column] [Required]
        public string Address { get; set; }


        [Column] [Required]
        public string City { get; set; }


        [Column] [Required]
        public string Region { get; set; }

        [Column]
        public string PostalCode { get; set; }
        
        [Column] [Required]
        public string Country { get; set; }

        [Column]
        public string HomePhone { get; set; }

        [Column]
        public string Extension { get; set; }

        [Column]
        public byte[] Photo { get; set; }

        [Column]
        public string Notes { get; set; }

        [Column]
        public int ReportsTo { get; set; }

        [Column]
        public string PhotoPath { get; set; }

    }
}
