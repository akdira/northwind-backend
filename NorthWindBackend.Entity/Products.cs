﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace NorthWindBackend.Entity
{
    [TableName("Products")]
    [PrimaryKey("ProductID")]
    [ExplicitColumns]
    [Serializable]

    public class Products
    {
        [Column]
        public int ProductID { get; set; }

        [Required] [Column]
        public string ProductName { get; set; }

        [Column]
        public int SupplierID { get; set; }

        [Column]
        public int CategoryID { get; set; }

        [Required] [Column]
        public string QuantityPerUnit { get; set; }

        [Column]
        public double UnitPrice { get; set; }

        [Column]
        public int UnitsInStock { get; set; }

        [Column]
        public int UnitsOnOrder { get; set; }

        [Required] [Column]
        public int ReorderLevel { get; set; }

        [Required] [Column]
        public bool Discontinued { get; set; }

    }
}
