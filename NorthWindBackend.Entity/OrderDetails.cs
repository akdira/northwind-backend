﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace NorthWindBackend.Entity
{
    [TableName("Order Details")]
    [PrimaryKey("OrderID,ProductID",autoIncrement =false)]
    [ExplicitColumns]
    [Serializable]

    public class OrderDetails
    {
        [Column]
        public int OrderID { get; set; }

        [Column] [Required]
        public int ProductID { get; set; }

        [Column] [Required]
        public double UnitPrice { get; set; }

        [Column] [Required]
        public int Quantity { get; set; }

        [Column]
        public double Discount { get; set; }


    }
}
