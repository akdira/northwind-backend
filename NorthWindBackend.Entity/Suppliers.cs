﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace NorthWindBackend.Entity
{
    [TableName("Suppliers")]
    [PrimaryKey("SupplierID")]
    [ExplicitColumns]
    [Serializable]

    public class Suppliers
    {
        [Column]
        public int SupplierID { get; set; }

        [Required] [Column]
        public string CompanyName { get; set; }

        [Required] [Column]
        public string ContactName { get; set; }

        [Column]
        public string ContactTitle { get; set; }

        [Column]
        public string Address { get; set; }

        [Column]
        public string City { get; set; }

        [Column]
        public string Region { get; set; }

        [Column]
        public string PostalCode { get; set; }

        [Column]
        public string Country { get; set; }

        [Column]
        public string Phone { get; set; }

        [Column]
        public string Fax { get; set; }

        [Column]
        public string HomePage { get; set; }

    }
}
