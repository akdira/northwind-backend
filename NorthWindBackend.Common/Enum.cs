﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWindBackend.Common
{
    public enum PageState
    {
        New = 0,
        View = 1,
        Edit = 2
    }

    public enum TaskStatus
    {
        Active = 1,
        REvise = 2,
        Complete = 3,
        Rejecte = 7
    }

    public enum IncidentStatus
    {
        Active = 1,
        REvise = 2,
        Complete = 3,
        Rejecte = 7
    }
}
