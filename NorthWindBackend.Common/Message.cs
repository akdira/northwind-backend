﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.Common
{
    public class Message
    {
        public static void Show(Control page, string message)
        {
            message = Commons.ClearMessage(message);
            ScriptManager.RegisterStartupScript(page, typeof(string), "onClick", string.Format("alert('{0}');", message), true);
        }

        public static void Show(Control page, string message, string script)
        {
            message = Commons.ClearMessage(message);
            ScriptManager.RegisterStartupScript(page, typeof(string), "onClick", string.Format("alert('{0}'); {1}", message, script), true);
        }

        public static void RunScript(Control page, string script)
        {
            ScriptManager.RegisterStartupScript(page, typeof(string), "onClick", string.Format("{0}", script), true);
        }

        public static void ShowErrorMessage(Control page, List<ErrorMessage> msgList)
        {
            string script = "";
            string msg = "";
            foreach (ErrorMessage m in msgList)
            {
                msg += "<p>" + m.Message + "</p>";
                if (m.IsDate)
                {
                    script += "$('#" + m.ControlID + "').parent().parent().parent().addClass('has-error');";
                }
                else
                {
                    script += "$('#" + m.ControlID + "').parent().parent().addClass('has-error');";
                }

            }
            script += "$('#error-message').html('" + msg + "'); $('#error-dialog').modal()";
            ScriptManager.RegisterStartupScript(page, typeof(string), "onClick", script, true);
        }
    }

    public class ErrorMessage
    {
        public ErrorMessage() { }

        public ErrorMessage(string controlID, string message)
        {
            ControlID = controlID;
            Message = message;
            IsDate = false;
        }

        public ErrorMessage(string controlID, string message, bool isDate)
        {
            ControlID = controlID;
            Message = message;
            IsDate = isDate;
        }
        public string ControlID { get; set; }

        public string Message { get; set; }

        public bool IsDate { get; set; }
    }
}
