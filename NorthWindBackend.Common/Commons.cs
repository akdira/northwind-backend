﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace NorthWindBackend.Common
{
    public class Commons
    {
        public static string GetDomainName()
        {
            return ConfigurationManager.AppSettings["DomainName"];
        }

        public static int GetInt(object obj)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static string GetCurrentLoginUser()
        {
            if (HttpContext.Current.Session != null && HttpContext.Current.Session[Constants.USER_ID] != null)
                return HttpContext.Current.Session[Constants.USER_ID].ToString();
            else
                return "";
        }

        public static string ClearMessage(string message)
        {
            if (message == null) return "";
            return message.Replace("'", " ").Replace("\r", "").Replace("\n", "");
        }

        public static string GetHostAddress()
        {
            return HttpContext.Current.Request.ServerVariables[Constants.REMOTE_ADDRESS].ToString();
        }

        public static void PopulateDropDownValue(DropDownList dropDownList, DataTable dataSource, string dataValueField, string dataTextField, bool addEmptyTopIndex = true)
        {
            if (dataSource.Rows.Count > 0)
            {
                dropDownList.DataValueField = dataValueField;
                dropDownList.DataTextField = dataTextField;
                dropDownList.DataSource = dataSource;
                dropDownList.DataBind();

                if (addEmptyTopIndex == true)
                {
                    dropDownList.Items.Insert(0, new ListItem("", ""));
                }

                if (dataSource.Rows.Count > 0)
                    dropDownList.SelectedIndex = 0;
            }

        }



        public static void PopulateDropDownValue(DropDownList dropDownList, IList dataSource, string dataValueField, string dataTextField, bool addEmptyTopIndex = true)
        {
            if (dataSource.Count > 0)
            {
                dropDownList.DataValueField = dataValueField;
                dropDownList.DataTextField = dataTextField;
                dropDownList.DataSource = dataSource;
                dropDownList.DataBind();
                if (dataSource.Count > 0)
                    dropDownList.SelectedIndex = 0;

                if (addEmptyTopIndex == true)
                {
                    dropDownList.Items.Insert(0, new ListItem("", ""));
                }
            }
        }

        public static DateTime GetDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch (Exception ex) { }
            return DateTime.Now;
        }

        public static DateTime? GetDateTime(TextBox tb)
        {
            try
            {
                return Convert.ToDateTime(tb.Text);
            }
            catch (Exception ex) { }
            return null;
        }

        public static double GetDouble(object obj)
        {
            try
            {
                return Convert.ToDouble(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static decimal GetDecimal(object obj)
        {
            try
            {
                return Convert.ToDecimal(obj);
            }
            catch (Exception ex) { }
            return 0;
        }



        public static bool GetBoolean(object obj)
        {
            try
            {
                return Convert.ToBoolean(obj);
            }
            catch (Exception ex) { }
            return false;
        }

        public static short GetShort(object obj)
        {
            try
            {
                return Convert.ToInt16(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static void MoveDocument(string ID, DataTable AttachmentDataTable)
        {
            try
            {
                string fileName = "";
                string tempFolder = ConfigurationManager.AppSettings.Get("TempFolder");
                string documentFolder = ConfigurationManager.AppSettings.Get("DocumentFolder");
                string sourceFile = "";
                string folderName = ID;
                string folderPath = documentFolder + folderName;
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                foreach (DataRow r in AttachmentDataTable.Rows)
                {
                    fileName = r["AttachmentFileName"].ToString();
                    sourceFile = tempFolder + fileName;
                    if (File.Exists(sourceFile))
                    {

                        File.Move(sourceFile, folderPath + "\\" + fileName);
                    }
                }
            }
            catch (Exception ex) { }
        }

        public static string GetDateString(DateTime? dateTime)
        {
            try
            {
                if (dateTime == null)
                {
                    return "";
                }
                return GetDateTime(dateTime).ToString(Constants.DATE_FORMAT_STRING);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static string GetDateString(DateTime dateTime)
        {
            try
            {
                if (dateTime == null)
                {
                    return "";
                }
                return GetDateTime(dateTime).ToString(Constants.DATE_FORMAT_STRING);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static string GetDateTimeString(DateTime? dateTime)
        {
            try
            {
                if (dateTime == null)
                {
                    return "";
                }
                return GetDateTime(dateTime).ToString(Constants.DATETIME_FORMAT_STRING);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static string GetDateTimeString(DateTime dateTime)
        {
            try
            {
                if (dateTime == null)
                {
                    return "";
                }
                return GetDateTime(dateTime).ToString(Constants.DATETIME_FORMAT_STRING);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static string GetTimeHoursString(DateTime dateTime)
        {
            try
            {
                if (dateTime == null)
                {
                    return "";
                }
                return GetDateTime(dateTime).ToString(Constants.TIMEHOURS_FORMAT_STRING);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static string GetTimeMinutesString(DateTime dateTime)
        {
            try
            {
                if (dateTime == null)
                {
                    return "";
                }
                return GetDateTime(dateTime).ToString(Constants.TIMEMINUTES_FORMAT_STRING);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public static string GetCurrentLoginUserName()
        {
            try
            {
                return HttpContext.Current.Session[Constants.CURRENT_USER_NAME].ToString();
            }
            catch (Exception ex)
            {

            }
            return "";
        }

        public static long GetLong(string value)
        {
            try
            {
                return long.Parse(value);
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        public static long GetLong(object value)
        {
            try
            {
                return long.Parse(value.ToString());
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        public static byte[] GetBinaryFromObject(object oVendor)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream();
            formatter.Serialize(mStream, oVendor);
            byte[] data = mStream.GetBuffer();
            mStream.Close();
            mStream = null;
            formatter = null;
            return data;
        }

        public static object GetObjectFromBinary(byte[] data)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream mStream = new MemoryStream(data);
            object obj = formatter.Deserialize(mStream);
            mStream.Close();
            mStream = null;
            formatter = null;
            return obj;
        }

        public static void SetBeginEndOfMonth(TextBox txtPeriodFrom, TextBox txtPeriodTo)
        {
            DateTime now = DateTime.Now;
            txtPeriodFrom.Text = "01-" + now.ToString("MMM-yyyy");
            txtPeriodTo.Text = DateTime.DaysInMonth(now.Year, now.Month) + "-" + now.ToString("MMM-yyyy");
        }

        public static string GetConfigValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static string GetFirstDayThisYear()
        {
            return "01-Jan-" + DateTime.Now.Year + " 00:00:00";
        }

        public static string GetLastDayThisYear()
        {
            return "31-Dec-" + DateTime.Now.Year + " 23:59:59";
        }

        public static string GetLocalDay(string date)
        {
            var culture = new System.Globalization.CultureInfo(ConfigurationManager.AppSettings["CultureInfo"]);

            return culture.DateTimeFormat.GetDayName(DateTime.Parse(date).DayOfWeek);
        }

        public static void PopulateCheckBoxListValue(CheckBoxList checkBoxList, DataTable dataSource, string dataValueField, string dataTextField)
        {
            if (dataSource.Rows.Count > 0)
            {
                checkBoxList.DataValueField = dataValueField;
                checkBoxList.DataTextField = dataTextField;
                checkBoxList.DataSource = dataSource;
                checkBoxList.DataBind();
            }

        }

        public static string Terbilang(decimal d)
        {
            double amount = GetDouble(d);

            string word = "";
            double divisor = 1000000000000.00; double large_amount = 0;
            double tiny_amount = 0;
            double dividen = 0; double dummy = 0;
            string weight1 = ""; string unit = ""; string follower = "";
            string[] prefix = { "SE", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            string[] sufix = { "SATU ", "DUA ", "TIGA ", "EMPAT ", "LIMA ", "ENAM ", "TUJUH ", "DELAPAN ", "SEMBILAN " };
            large_amount = Math.Abs(Math.Truncate(amount));
            tiny_amount = Math.Round((Math.Abs(amount) - large_amount) * 100);
            if (large_amount > divisor)
                return "Melebihi Batas";
            while (divisor >= 1)
            {
                dividen = Math.Truncate(large_amount / divisor);
                large_amount = large_amount % divisor;
                unit = "";
                if (dividen > 0)
                {
                    if (divisor == 1000000000000.00)
                        unit = "TRILYUN ";
                    else
                        if (divisor == 1000000000.00)
                        unit = "MILYAR ";
                    else
                            if (divisor == 1000000.00)
                        unit = "JUTA ";
                    else
                                if (divisor == 1000.00)
                        unit = "RIBU ";
                }
                weight1 = "";
                dummy = dividen;
                if (dummy >= 100)
                    weight1 = prefix[(int)Math.Truncate(dummy / 100) - 1] + "RATUS ";
                dummy = dividen % 100;
                if (dummy < 10)
                {
                    if (dummy == 1 && unit == "RIBU ")
                        weight1 += "SE";
                    else
                        if (dummy > 0)
                        weight1 += sufix[(int)dummy - 1];
                }
                else
                    if (dummy >= 11 && dummy <= 19)
                {
                    weight1 += prefix[(int)(dummy % 10) - 1] + "BELAS ";
                }
                else
                {
                    weight1 += prefix[(int)Math.Truncate(dummy / 10) - 1] + "PULUH ";
                    if (dummy % 10 > 0)
                        weight1 += sufix[(int)(dummy % 10) - 1];
                }
                word += weight1 + unit;
                divisor /= 1000.00;
            }
            if (Math.Truncate(amount) == 0)
                word = "NOL ";
            follower = "";
            if (tiny_amount < 10)
            {
                if (tiny_amount > 0)
                    follower = "KOMA NOL " + sufix[(int)tiny_amount - 1];
            }
            else
            {
                follower = "KOMA " + sufix[(int)Math.Truncate(tiny_amount / 10) - 1];
                if (tiny_amount % 10 > 0)
                    follower += sufix[(int)(tiny_amount % 10) - 1];
            }
            word += follower;
            if (amount < 0)
            {
                word = "MINUS " + word + " ";
            }
            else
            {
                word = word + " ";
            }
            return word.Trim();
        }

    }
}
