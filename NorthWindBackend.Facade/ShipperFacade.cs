﻿using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NorthWindBackend.Facade
{
    public class ShipperFacade : BaseFacade<Shippers>
    {

        public DataTable PageDataAll()
        {
            string sql = @"SELECT * FROM [dbo].[Shippers]";
            return DB.FetchDataTable(new Sql(sql));
        }

        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Shippers] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public ActionResult DataInsert(Shippers oShipper)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(oShipper);

                    InsertActivityLog("Insert Shippers ID " + oShipper.ShipperID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Insert data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("ShipperList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataUpdate(Shippers oShipper)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Update(oShipper);

                    InsertActivityLog("Update Shippers ID " + oShipper.ShipperID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("ShipperList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataDelete(int dataID)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Delete(dataID);

                    InsertActivityLog("Delete Shippers ID " + dataID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Hapus data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("ShipperList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }
    }
}
