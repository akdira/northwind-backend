﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using PetaPoco;

namespace NorthWindBackend.Facade
{
    public class LoginUserFacade : BaseFacade<LoginUser>
    {
        public void DeleteByUserName(string userName)
        {
            DB.Delete<LoginUser>(new Sql().Where("UserName = @0", userName));
        }

        public void InsertLoginUser(LoginUser loginUser)
        {
            if(DB.SingleOrDefault<LoginUser>(new Sql().Where("UserName = @0", loginUser.UserName)) == null)
            {
                DB.Insert("LoginUser", "UserName", false,loginUser);
            }
        }
    }
}
