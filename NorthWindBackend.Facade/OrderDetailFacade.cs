﻿using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthWindBackend.Facade
{
    public class OrderDetailFacade : BaseFacade<OrderDetails>
    {

        public int InsertMass(List<OrderDetails> OrderDetailsData)
        {
            foreach(var OneDetail in OrderDetailsData)
            {
                var facade = new OrderDetailFacade();
                facade.Insert(OneDetail);
            }
            return 1;
        }

        public int Clean(int OrderID)
        {
            return
            DB.Execute(@"DELETE FROM [dbo].[Order Details]
                         WHERE [dbo].[Order Details].[OrderID] = '" + OrderID + "' ");

        }
        public Page PageOrderDataByCriteria(int OrderID, string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Order Details] 
                           WHERE [dbo].[Order Details].[OrderID] = '" + OrderID + "' AND " +
                           criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public List<OrderDetailsMap> PageRuntime(int OrderID, string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT [dbo].[Order Details].[OrderID],
                                  [dbo].[Order Details].[ProductID],
                                  [dbo].[Products].[ProductName],
                                  [dbo].[Order Details].[UnitPrice],
                                  [dbo].[Order Details].[Quantity],
                                  [dbo].[Order Details].[Discount]

                           FROM [dbo].[Order Details]
                           LEFT JOIN  [dbo].[Products] ON [dbo].[Products].[ProductID] = [dbo].[Order Details].[ProductID]
                           WHERE [dbo].[Order Details].[OrderID]=@0 AND " + criteria;


            return DB.Fetch<OrderDetailsMap>(sql, OrderID);
        }

        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Order Details] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public ActionResult DataInsert(OrderDetails oOrderDetail)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(oOrderDetail);

                    InsertActivityLog("Insert Order Details ID " + oOrderDetail.OrderID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Insert data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("Order DetailList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataUpdate(OrderDetails oOrderDetail)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Update(oOrderDetail);

                    InsertActivityLog("Update Order Details ID " + oOrderDetail.OrderID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("Order DetailList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataDelete(int dataID)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Delete(dataID);

                    InsertActivityLog("Delete Order Details ID " + dataID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Hapus data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("Order DetailList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }
    }
}
