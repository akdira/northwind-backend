﻿using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NorthWindBackend.Facade
{
    public class ProductFacade : BaseFacade<Products>
    {

        public Page PageDataByCriteria_2(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Products] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT [ProductID],
                                  [ProductName],
                                  [dbo].[Suppliers].[CompanyName] AS [Supplier],
                                  [dbo].[Categories].[CategoryName] AS [Category],
                                  [QuantityPerUnit],
                                  [UnitPrice],
                                  [UnitsInStock],
                                  [UnitsOnOrder],
                                  [ReorderLevel],
                                  [Discontinued] 
                             FROM [dbo].[Products] 
                             LEFT JOIN [dbo].[Categories] ON [dbo].[Categories].[CategoryID] = [dbo].[Products].[CategoryID]
                             LEFT JOIN [dbo].[Suppliers] ON [dbo].[Suppliers].[SupplierID] = [dbo].[Products].[SupplierID]
                             WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public ActionResult DataInsert(Products oProduct)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(oProduct);

                    InsertActivityLog("Insert Products ID " + oProduct.ProductID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Insert data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("ProductList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataUpdate(Products oProduct)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Update(oProduct);

                    InsertActivityLog("Update Products ID " + oProduct.ProductID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("ProductList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataDelete(int dataID)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Delete(dataID);

                    InsertActivityLog("Delete Products ID " + dataID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Hapus data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("ProductList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }
    }
}
