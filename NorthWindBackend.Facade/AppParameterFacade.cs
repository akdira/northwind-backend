﻿using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NorthWind.Entity;
using System.Reflection;

namespace NorthWindBackend.Facade
{
    public class AppParameterFacade : BaseFacade<AppParameter>
    {

        public List<AppParameter> ListAll()
        {
            string sql = @"SELECT * FROM [dbo].[MA_AppParameter]";
            return DB.Fetch<AppParameter>(sql);
        }

        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[AppParameter] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public ActionResult DataInsert(AppParameter oAppParameter)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(oAppParameter);

                    InsertActivityLog("Insert AppParameter ID " + oAppParameter.ID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Insert data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("AppParameterList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataUpdate(AppParameter oAppParameter)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Update(oAppParameter);

                    InsertActivityLog("Update AppParameter ID " + oAppParameter.ID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("AppParameterList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }
        
        public ActionResult DataDelete(int dataID)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Delete(dataID);

                    InsertActivityLog("Delete AppParameter ID " + dataID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Hapus data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("AppParameterList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }
    }
}
