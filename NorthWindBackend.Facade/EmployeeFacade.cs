﻿using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NorthWind.Entity;
using System.Reflection;

namespace NorthWindBackend.Facade
{
    public class EmployeeFacade : BaseFacade<Employees>
    {

        public List<Employees> ListAll()
        {
            string sql = @"SELECT * FROM [dbo].[Employees]";
            return DB.Fetch<Employees>(sql);
        }

        public Page PageDataByCriteria(string criteria, int page, int perPage, string orderBy)
        {
            string sql = @"SELECT * FROM [dbo].[Employees] WHERE " + criteria;
            return DB.Page(page, perPage, new Sql(sql).OrderBy(orderBy));
        }

        public ActionResult DataInsert(Employees oEmployee)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Insert(oEmployee);

                    InsertActivityLog("Insert Employees ID " + oEmployee.EmployeeID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Insert data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("EmployeeList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataUpdate(Employees oEmployee)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Update(oEmployee);

                    InsertActivityLog("Update Employees ID " + oEmployee.EmployeeID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("EmployeeList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataInsertPartial(Employees oEmployee)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    
                    //PropertyInfo[] PropertiesObject = oEmployee.GetType().GetProperties();
                    //int fields_count = 0;
                    //string[] fields_name = new string[];
                    //foreach (PropertyInfo OneProp in PropertiesObject)
                    //{
                    //    if(Convert.ToString(OneProp.GetValue(OneProp, null)) != null || OneProp.GetValue != "")
                    //    fields_count++;
                    //    fields_count = OneProp
                    //}

                    DB.Update("Employees", "EmployeeID", oEmployee, new string[] { "EmployeeID",
                                                                                   "FirstName",
                                                                                   "LastName",
                                                                                   "Title",
                                                                                   "TitleOfCourtesy",
                                                                                   "BirthDate",
                                                                                   "HireDate",
                                                                                   "Address",
                                                                                   "City",
                                                                                   "Region",
                                                                                   "PostalCode",
                                                                                   "Country",
                                                                                   "HomePhone",
                                                                                   "Extension",
                                                                                   "Notes",
                                                                                   "ReportsTo",
                                                                                   "PhotoPath",
                                                                                 });

                    InsertActivityLog("Update Employees ID " + oEmployee.EmployeeID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("EmployeeList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataUpdatePartial(Employees oEmployee)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {

                    //PropertyInfo[] PropertiesObject = oEmployee.GetType().GetProperties();
                    //int fields_count = 0;
                    //string[] fields_name = new string[];
                    //foreach (PropertyInfo OneProp in PropertiesObject)
                    //{
                    //    if(Convert.ToString(OneProp.GetValue(OneProp, null)) != null || OneProp.GetValue != "")
                    //    fields_count++;
                    //    fields_count = OneProp
                    //}

                    DB.Update("Employees", "EmployeeID", oEmployee, new string[] { "EmployeeID",
                                                                                   "FirstName",
                                                                                   "LastName",
                                                                                   "Title",
                                                                                   "TitleOfCourtesy",
                                                                                   "BirthDate",
                                                                                   "HireDate",
                                                                                   "Address",
                                                                                   "City",
                                                                                   "Region",
                                                                                   "PostalCode",
                                                                                   "Country",
                                                                                   "HomePhone",
                                                                                   "Extension",
                                                                                   "Notes",
                                                                                   "ReportsTo",
                                                                                   "PhotoPath",
                                                                                 });

                    InsertActivityLog("Update Employees ID " + oEmployee.EmployeeID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Update data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("EmployeeList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }

        public ActionResult DataDelete(int dataID)
        {
            ActionResult result = null;
            try
            {
                using (var scope = DB.GetTransaction())
                {
                    Delete(dataID);

                    InsertActivityLog("Delete Employees ID " + dataID + " succes");
                    scope.Complete();
                    result = new ActionResult(true, "Hapus data sukses!");
                }
            }
            catch (Exception ex)
            {
                InsertErrorLog("EmployeeList", ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message);
                result = new ActionResult(false, Commons.ClearMessage(ex.Message));
                result.ErrorMessage = Commons.ClearMessage(ex.Message);
            }
            return result;
        }
    }
}
