﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PetaPoco;
using System.Data;

namespace NorthWindBackend.Facade
{
    public class BaseFacade<T> : IDisposable
    {
        public Database DB;

        public BaseFacade()
        {
            DB = new Database("NorthWindBackend");
        }

        public int Insert(T obj)
        {
            object id = DB.Insert(obj);
            return Convert.ToInt32(id);
        }

        public void Update(T obj)
        {
            DB.Update(obj);
        }

        public void Delete(T obj)
        {
            DB.Delete(obj);
        }

        public void Delete(int id)
        {
            DB.Delete<T>(id);
        }
        
        public T GetObject(int id)
        {
            return DB.SingleOrDefault<T>(id);
        }
        
        public T GetObject(string column, object value)
        {
            return DB.SingleOrDefault<T>(new Sql().Where(string.Format(" {0} = @0", column), value));
        }

        public T GetObject(Dictionary<string,object> param)
        {
            string sql = " 1=1 ";
            object[] args = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                args[i] = param[key];
                i++;
            }
            return DB.SingleOrDefault<T>(new Sql().Where(sql,args));
        }

        public List<T> GetListItems()
        {
            return DB.Fetch<T>(new Sql().Where("1=1"));
        }

        public List<T> GetListItems(Dictionary<string, object> param)
        {
            string sql = " 1=1 ";
            object[] args = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                args[i] = param[key];
                i++;
            }
            return DB.Fetch<T>(new Sql().Where(sql, args));
        }

        public List<T> GetListItems(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = " 1=1 ";
            object[] args = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                args[i] = param[key];
                i++;
            }
            var list = DB.Fetch<T>(new Sql().Where(sql, args).OrderBy(orderBy));
            list = DB.Page<T>(page, itemsPerPage, new Sql().Where(sql, args).OrderBy(orderBy)).Items;
            return list;
        }
        
        public DataTable GetListItemsAsDataTable()
        {
            return DB.FetchDataTable<T>(new Sql().Where("1=1"));
        }

        public DataTable GetListItemsAsDataTable(Dictionary<string, object> param)
        {
            string sql = " 1=1 ";
            object[] args = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                args[i] = param[key];
                i++;
            }
            return DB.FetchDataTable<T>(new Sql().Where(sql, args));
        }

        public DataTable GetListItemsAsDataTable(Dictionary<string, object> param, int page, int itemsPerPage, string orderBy)
        {
            string sql = " 1=1 ";
            object[] args = new object[param.Count];
            int i = 0;
            foreach (string key in param.Keys)
            {
                sql += string.Format("AND {0} = @{1} ", key, i);
                args[i] = param[key];
                i++;
            }
            return DB.PageDataTable<T>(page, itemsPerPage, new Sql().Where(sql, args).OrderBy(orderBy));
        }

        public void InsertActivityLog(string logDescription)
        {
            var log = new NorthWindBackend.Entity.ActivityLog()
            {
                ActivityUser = Common.Commons.GetCurrentLoginUser(),
                ActivityIP = Common.Commons.GetHostAddress(),
                ActivityDate = DateTime.Now,
                ActivityDescription = logDescription
            };
            DB.Insert(log);
        }

        public void InsertErrorLog(string sourcePage, string errorDescription, string remark)
        {
            var log = new NorthWindBackend.Entity.ErrorLog()
            {
                ErrorDate = DateTime.Now,
                ErrorSourceIP = Common.Commons.GetHostAddress(),
                ErrorSourceUser = Common.Commons.GetCurrentLoginUser(),
                ErrorSourcePage = sourcePage,
                ErrorDescription = errorDescription,
                ErrorRemark = remark
            };
            DB.Insert(log);
        }

        public void Dispose()
        {
            
        }
    }
}
