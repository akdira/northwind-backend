﻿using NorthWind.Entity;
using NorthWindBackend.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NorthWindBackend.WebMVC.Controllers
{
    public class AppParameterController : Controller
    {
        // GET: AppParameter
        public ActionResult Index()
        {
            return View(new AppParameterFacade().ListAll());
        }

        // GET: AppParameter/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AppParameter/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AppParameter/Create
        [HttpPost]
        public ActionResult Create(AppParameter OAppParameter)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                //OEmployee.EmployeeID = Request.Form[""];
                // TODO: Add update logic here

                AppParameterFacade facade = new AppParameterFacade();
                facade.Insert(OAppParameter);

                return RedirectToAction("Index");


                // TODO: Add insert logic here

                //return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: AppParameter/Edit/5
        public ActionResult Edit(int id)
        {
            AppParameterFacade AppParameterFcd = new AppParameterFacade();
            AppParameter OAppParameter = AppParameterFcd.GetObject(id);
            return View(OAppParameter);
        }

        // POST: AppParameter/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AppParameter OAppParameter)
        {
            try
            {
                //OEmployee.EmployeeID = Request.Form[""];
                // TODO: Add update logic here

                AppParameterFacade facade = new AppParameterFacade();
                facade.DataUpdate(OAppParameter);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }


        }

        // GET: AppParameter/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AppParameter/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, AppParameter OAppParameter)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
