﻿using NorthWind.Entity;
using NorthWindBackend.Facade;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace NorthWindBackend.WebMVC.Controllers
{
    public class EmployeeController : Controller
    {

        // GET: Employee
        public ActionResult Index()
        {
            var employees = from e in GetEmployeeList()
                            orderby e.FirstName
                            select e;
            return View(employees);
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employees OEmployee)
        {

            

            if (!ModelState.IsValid)
            {
                return View();
            }

            try
            {
                //OEmployee.EmployeeID = Request.Form[""];
                // TODO: Add update logic here

                EmployeeFacade facade = new EmployeeFacade();
                OEmployee.Photo = new byte[] { 1 };
                facade.Insert(OEmployee);

                return RedirectToAction("Index");


                // TODO: Add insert logic here

                //return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            EmployeeFacade EmployeeFcd = new EmployeeFacade();
            Employees employee = EmployeeFcd.GetObject(id);
            return View(employee);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Employees OEmployee)
        {
            try
            {
                //OEmployee.EmployeeID = Request.Form[""];
                // TODO: Add update logic here
                Debug.WriteLine(new JavaScriptSerializer().Serialize(OEmployee));
                Debug.WriteLine("Employees");

                EmployeeFacade facade = new EmployeeFacade();
                facade.DataUpdatePartial(OEmployee);
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {

            EmployeeFacade facade = new EmployeeFacade();

            Employees OEmployee = facade.GetObject(id);
            facade.Delete(OEmployee);

            return RedirectToAction("Index");
            //return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [NonAction]
        public List<Employees> GetEmployeeList()
        {
            EmployeeFacade EmployeeFcd = new EmployeeFacade();
            return EmployeeFcd.ListAll();
           // return new List<Employees>{
           //   new Employees{
           //      EmployeeID = 1,
           //      FirstName = "Allan",
           //      HireDate = DateTime.Today.ToString(),
           //      BirthDate = DateTime.Today.ToString()
           //   },

           //   new Employees{
           //      EmployeeID = 2,
           //      FirstName = "Carson",
           //      HireDate = DateTime.Today.ToString(),
           //      BirthDate = DateTime.Today.ToString()
           //   },

           //   new Employees{
           //      EmployeeID = 3,
           //      FirstName = "Carson",
           //      HireDate = DateTime.Today.ToString(),
           //      BirthDate = DateTime.Today.ToString()
           //   },

           //   new Employees{
           //      EmployeeID = 4,
           //      FirstName = "Laura",
           //      HireDate = DateTime.Today.ToString(),
           //      BirthDate = DateTime.Today.ToString()
           //   },
           //};
        }
    }
}
