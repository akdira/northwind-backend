﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;  

namespace NorthWindBackend.WebUI.WebControl
{
    public partial class WebGridView : UserControl
    {
        #region Delegate

        public delegate PetaPoco.Page GetPageData();
        public GetPageData PageFunction;
        public delegate object GetData();
        public GetData DataFunction;
        public delegate int GetDataTotalCount();
        public GetDataTotalCount DataTotalCountFunction;
        public delegate void GridRowDataBound(object sender, GridViewRowEventArgs e);
        public delegate void GridDeleting(int index);
        public delegate void GridEditing(int index);
        public delegate void GridCancelEditing(int index);
        public delegate void GridUpdating(int index);
        public delegate void GridRowCommand(object sender, GridViewCommandEventArgs e);
        public delegate void GridDataBound(object sender, EventArgs e);
        public delegate void GridSelectedIndexChanged(object sender, EventArgs e);

        #endregion

        #region Event

        public event GridRowDataBound RowDataBound;
        public event GridEditing Editing;
        public event GridDeleting Deleting;
        public event GridCancelEditing CancelEditing;
        public event GridUpdating Updating;
        public event GridRowCommand RowCommand;
        public event GridDataBound DataBound;
        public event GridSelectedIndexChanged SelectedIndexChanged;

        #endregion

        #region ViewState

        private String SortExpression
        {
            get
            {
                return Convert.ToString(ViewState["SortExpression"]);
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        private String SortDirection
        {
            get
            {
                return Convert.ToString(ViewState["SortDirection"]);
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        private int ActualRowCount
        {
            get
            {
                return Convert.ToInt32(ViewState["ActualRowCount"]);
            }
            set
            {
                ViewState["ActualRowCount"] = value;
            }
        }

        private bool IsDeleteButtonCreated
        {
            get
            {
                return Convert.ToBoolean(ViewState["IsDeleteButtonCreated"]);
            }
            set
            {
                ViewState["IsDeleteButtonCreated"] = value;
            }
        }

        private bool IsEditButtonCreated
        {
            get
            {
                return Convert.ToBoolean(ViewState["IsEditButtonCreated"]);
            }
            set
            {
                ViewState["IsEditButtonCreated"] = value;
            }
        }

        private bool IsNumberFieldCreated
        {
            get
            {
                return Convert.ToBoolean(ViewState["IsNumberFieldCreated"]);
            }
            set
            {
                ViewState["IsNumberFieldCreated"] = value;
            }
        }

        private DataTable SearchCriterias
        {
            get
            {
                return (DataTable)ViewState["SearchCriterias"];
            }
            set
            {
                ViewState["SearchCriterias"] = value;
            }
        }

        public String SortKey
        {
            get
            {
                string sortKey = string.Empty;
                if (string.IsNullOrEmpty(SortExpression) || string.IsNullOrEmpty(SortDirection))
                {
                    sortKey = string.Format("{0} {1}", DefaultSortExpression, DefaultSortDirection).Trim();
                    //sortKey = CreateSortKey(DefaultSortExpression, DefaultSortDirection);
                }
                else if (SortExpression == DefaultSortExpression && SortDirection == DefaultSortDirection)
                {
                    sortKey = string.Format("{0} {1}", DefaultSortExpression, DefaultSortDirection).Trim();
                }
                else
                {
                    //sortKey = CreateSortKey(SortExpression, SortDirection);
                    sortKey = string.Format("{0} {1}", SortExpression, SortDirection).Trim();

                }

                if (string.IsNullOrEmpty(sortKey))
                {
                    sortKey = String.Format("{0} {1}", DefaultSortExpression, DefaultSortDirection);
                }
                return sortKey;
                //return string.Format("CONVERT(varchar,{0}) {1}", SortExpression, SortDirection).Trim();;
            }
        }

        #endregion

        #region Fields

        private int _freezeLeftMostColumn = 0;
        private String _defaultSortExpression;
        private String _defaultSortDirection;
        private String _title;
        private String _imagePath;
        private bool _showFooter = false;
        private bool _allowPaging = true;
        private bool _allowSorting = true;
        private bool _allowEdit = false;
        private bool _allowDelete = false;
        private bool _allowSearch = false;
        private bool _showTotalRecords = true;
        private bool _useCustomSearchCriterias = false;
        private bool _showBorder = true;
        private List<Criteria> _criterias = new List<Criteria>();

        #endregion

        #region Properties

        public bool IsDisplayed = false;

        public bool DisplayLegend
        {
            get
            {
                if (ViewState["DisplayLegend"] == null)
                {
                    ViewState["DisplayLegend"] = true;
                }
                return Convert.ToBoolean(ViewState["DisplayLegend"]);
            }
            set
            {
                ViewState["DisplayLegend"] = value;
            }
        }

        //Additional
        public Panel SearchBox
        {
            get
            {
                if (ViewState["SortExpression"] == null)
                    ViewState["SortExpression"] = FindControl("SearchBox");
                return ViewState["SortExpression"] as Panel;
            }

            set
            {
                ViewState["SortExpression"] = value;
            }

        }


        //END Additional
        public GridView GridView
        {
            get
            {
                return grid;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public DataControlFieldCollection Columns
        {
            get
            {
                return grid.Columns;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public GridViewRowCollection getRow
        {
            get
            {
                return grid.Rows;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        public List<Criteria> Criterias
        {
            get
            {
                return _criterias;
            }
        }

        public String DataKeyNames
        {

            set
            {
                grid.DataKeyNames = value.Split(',');
            }
            get
            {
                return grid.DataKeyNames[0];
            }
        }

        public int FreezeLeftMostColumn
        {
            set
            {
                _freezeLeftMostColumn = value;
            }
            get
            {
                return _freezeLeftMostColumn;
            }
        }

        public bool ShowBorder
        {
            set
            {
                _showBorder = value;

            }
            get
            {
                return _showBorder;
            }

        }
        public bool UseCustomSearchCriterias
        {
            set
            {
                _useCustomSearchCriterias = value;
            }
            get
            {
                return _useCustomSearchCriterias;
            }
        }

        public String DefaultSortExpression
        {
            set
            {
                _defaultSortExpression = value;
            }
            get
            {
                return _defaultSortExpression;
            }
        }

        public String DefaultSortDirection
        {
            set
            {
                _defaultSortDirection = value;
            }
            get
            {
                return _defaultSortDirection;
            }
        }

        public String ImagePath
        {
            set
            {
                _imagePath = value;
            }
            get
            {
                return _imagePath;
            }
        }

        public bool ShowFooter
        {
            set
            {
                _showFooter = value;
            }
            get
            {
                return _showFooter;
            }
        }

        public bool AllowPaging
        {
            set
            {
                _allowPaging = value;
            }
            get
            {
                return _allowPaging;
            }
        }

        public bool AllowSorting
        {
            set
            {
                _allowSorting = value;
            }
            get
            {
                return _allowSorting;
            }
        }

        public bool AllowEdit
        {
            set
            {
                _allowEdit = value;
            }
            get
            {
                return _allowEdit;
            }
        }

        public bool AllowDelete
        {
            set
            {
                _allowDelete = value;
            }
            get
            {
                return _allowDelete;
            }
        }

        public String Title
        {
            set
            {
                _title = value;
            }
            get
            {
                return _title;
            }
        }

        public bool AllowSearch
        {
            set
            {
                _allowSearch = value;
            }
            get
            {
                return _allowSearch;
            }
        }

        public bool ShowTotalRecords
        {
            set
            {
                _showTotalRecords = value;
            }
            get
            {
                return _showTotalRecords;
            }
        }

        public Unit GridWidth
        {
            set
            {
                grid.Width = value;
            }
            get
            {
                return grid.Width;
            }
        }

        #endregion

        #region Private Methods

        private void LoadPage(int pageNumber)
        {
            GridViewPageEventArgs e = new GridViewPageEventArgs(pageNumber);
            grid_PageIndexChanging(this, e);
        }

        private void adjustNextAndPrevButton(int pageNum)
        {
            int pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(this.ActualRowCount) / grid.PageSize));
            if (pageNum == 0)
            {
                btnPrev.Enabled = false;
                btnPrev.CssClass = "btn btn-sm previous_disable";
            }
            else
            {
                btnPrev.Enabled = true;
                btnPrev.CssClass = "btn btn-primary btn-sm previous";
            }
            if (pageNum + 1 == pageCount)
            {
                btnNext.Enabled = false;
                btnNext.CssClass = "btn btn-sm next_disable";
            }
            else
            {
                btnNext.Enabled = true;
                btnNext.CssClass = "btn btn-primary btn-sm next-paging";
            }
        }

        private object[] ConvertToObjects(IOrderedDictionary primaryValue)
        {
            object[] values = new object[primaryValue.Count];
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = primaryValue[i];
            }
            return values;
        }

        private GridFilterDataType GetCriteriaTypeByField(string field)
        {
            GridFilterDataType type = GridFilterDataType.Text;
            foreach (Criteria criteria in _criterias)
            {
                if (criteria.Field == field)
                    return criteria.DataType;
            }
            return type;
        }

        private string CreateCriteriaQuery(DataRow row)
        {
            string searchValue = Convert.ToString(row["Value"]).Replace("'", "''").ToUpper();
            string result = string.Format("UPPER(CAST({0} AS Varchar(MAX))) LIKE '%{1}%'", row["Field"].ToString(), searchValue.Replace(" ", "%"));
            GridFilterDataType dataType = GetCriteriaTypeByField(Convert.ToString(row["Field"]));
            if (dataType == GridFilterDataType.Boolean)
            {
                result = string.Format("CASE WHEN ISNULL({0},0) = 1 THEN 'YA' ELSE 'TIDAK' END  LIKE '%{1}%'", row["Field"].ToString(), searchValue);
            }
            else if (dataType == GridFilterDataType.DateTime)
            {
                result = string.Format("({0} OR {1})", result, string.Format("RIGHT('0' + CONVERT(varchar, day({0})), 2) + ' ' + LEFT(DATENAME(M, {0}),3) + ' ' + CONVERT(varchar, DATEPART(YEAR, {0})) + ' ' + + RIGHT('0'+CONVERT(varchar, datepart(HH, {0})),2) + ':' + RIGHT('0'+CONVERT(varchar, datepart(MI, {0})),2) + ':' + RIGHT('0'+CONVERT(varchar, datepart(SS, {0})),2) LIKE '%{1}%'", row["Field"], searchValue));
            }
            else if (dataType == GridFilterDataType.Date)
            {
                result = string.Format("({0} OR {1})", result, string.Format("RIGHT('0' + CONVERT(varchar, day({0})), 2) + ' ' + LEFT(DATENAME(M, {0}),3) + ' ' + CONVERT(varchar, DATEPART(YEAR, {0})) LIKE '%{1}%' OR ( convert(varchar, {0}, 103) LIKE '%{1}%')", row["Field"], searchValue));
            }

            return result;
        }

        private string CreateSortKey(string expression, string direction)
        {
            string result = string.Format("CONVERT(varchar,{0}) {1}", SortExpression, SortDirection).Trim(); ;
            GridFilterDataType dataType = GetCriteriaTypeByField(expression);
            if (dataType != GridFilterDataType.Text)
            {
                result = string.Format("{0} {1}", expression, direction).Trim(); ;
            }
            return result;
        }

        #endregion

        #region Public Methods

        public void Delete(int index)
        {
            grid.DeleteRow(index);
        }

        public BoundField AddColumn(String field, String header)
        {
            return AddColumn(field, header, String.Empty, String.Empty);
        }

        public BoundField AddColumn(String field, String header, String formatString)
        {
            return AddColumn(field, header, formatString, String.Empty);
        }

        public BoundField AddColumn(String field, String header, String formatString, String sortExpression)
        {
            BoundField col = new BoundField();
            if (!String.IsNullOrEmpty(formatString))
            {
                col.HtmlEncode = false;
            }
            else
            {
                col.HtmlEncode = true;
            }
            col.DataField = field;
            if (!String.IsNullOrEmpty(sortExpression))
            {
                col.SortExpression = sortExpression;
            }
            col.HeaderText = header;
            grid.Columns.Add(col);
            return col;
        }

        public void Hide()
        {
            gridContainer.Visible = false;
        }

        public void Show()
        {
            gridContainer.Visible = true;
        }

        public System.Web.UI.WebControls.LinkButton C_btnPrev
        {
            get { return btnPrev; }
        }

        public System.Web.UI.WebControls.LinkButton C_btnNext
        {
            get { return btnNext; }
        }

        public IDictionary<String, object> GetSearchParams(bool withPaging)
        {
            IDictionary<String, object> parameters = new Dictionary<String, object>();
            if (withPaging)
            {
                parameters.Add("Start", this.GridView.PageIndex * this.GridView.PageSize);
                parameters.Add("Limit", this.GridView.PageSize);
            }
            if (_allowSearch && SearchCriterias != null)
            {

                foreach (DataRow row in SearchCriterias.Rows)
                {
                    string paramName = row["Field"].ToString();
                    if (row["Field"].ToString().Contains("."))
                    {
                        string[] fields = row["Field"].ToString().Split('.');
                        paramName = string.Format("{0}.{1}", fields[0], fields[1]);
                    }
                    object paramValue = row["Value"];
                    parameters.Add(paramName, paramValue);
                }
            }
            return parameters;

        }

        public IDictionary<String, object> GetSearchParams()
        {
            return GetSearchParams(true);
            //IDictionary<String, object> parameters = new Dictionary<String, object>();
            //parameters.Add(VarConst.FILTER_PAGE_START, this.GridView.PageIndex * this.GridView.PageSize);
            //parameters.Add(VarConst.FILTER_PAGE_LIMIT, this.GridView.PageSize);
            //if (_allowSearch && SearchCriterias != null)
            //{
            //    foreach (DataRow row in SearchCriterias.Rows)
            //    {
            //        string paramName = row["Field"].ToString();
            //        if (row["Field"].ToString().Contains("."))
            //        {
            //            string[] fields = row["Field"].ToString().Split('.');
            //            paramName = string.Format("{0}.{1}", fields[0], fields[1]);
            //        }
            //        object paramValue = row["Value"];
            //        parameters.Add(paramName, paramValue);
            //    }
            //}
            //return parameters;
        }

        public String GetSqlSearchCriterias()
        {
            if (Session["SessionCriteria"] != null)
            {
                SearchCriterias = ((DataTable)Session["SessionCriteria"]).Copy();
                criterias.DataSource = SearchCriterias;
                criterias.DataBind();
                Session["SessionCriteria"] = null;
            }


            if (_allowSearch && SearchCriterias != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("1=1");
                foreach (DataRow row in SearchCriterias.Rows)
                {
                    sb.Append(" AND ");
                    if (row["Field"].ToString().Contains("."))
                    {
                        sb.AppendFormat("UPPER(CAST({0}.{1} AS Char(100))) LIKE '%{2}%'", row["Field"].ToString().Split('.')[0]
                            , row["Field"].ToString().Split('.')[1], row["Value"].ToString().ToUpper());
                    }
                    //else if (row["Value"].ToString().ToUpper().Contains("TRUE") || row["Value"].ToString().ToUpper().Contains("YA"))
                    //{
                    //    sb.AppendFormat("((UPPER(CAST({0} AS Char(100))) LIKE '%{1}%') OR (UPPER(CAST({0} AS Char(100))) = 1))", row["Field"].ToString(), row["Value"].ToString().ToUpper());
                    //}
                    //else if (row["Value"].ToString().ToUpper().Contains("FALSE") || row["Value"].ToString().ToUpper().Contains("TIDAK"))
                    //{
                    //    sb.AppendFormat("((UPPER(CAST({0} AS Char(100))) LIKE '%{1}%') OR (UPPER(CAST({0} AS Char(100))) = 0))", row["Field"].ToString(), row["Value"].ToString().ToUpper());
                    //}
                    else
                    {
                        sb.Append(CreateCriteriaQuery(row));
                        //sb.AppendFormat("UPPER(CAST({0} AS Char(100))) LIKE '%{1}%'", row["Field"].ToString(), row["Value"].ToString().ToUpper());
                    }
                }

                return sb.ToString();
            }
            else
            {
                return " 1=1 ";
            }
        }

        public void DisplayData()
        {
            DisplayData(true);
        }

        private bool IsDataBinD = false;
        private void DisplayOnPostBack()
        {
            if (GetInt(txtGoToPage.Text) == 0)
            {
                txtGoToPage.Text = "1";
            }
            if (txtGoToPage.Text != string.Empty)
            {
                grid.PageIndex = Convert.ToInt32(txtGoToPage.Text) - 1;
                if (grid.PageIndex == 0)
                {
                    return;
                }
            }
            if (IsDataBinD)
            {
                return;
            }
            else
            {
                if (!_allowSearch)
                {
                    searchBox.Visible = false;
                }
                else
                {
                    searchBox.Visible = true;
                }
                grid.AllowSorting = _allowSorting;
                grid.AllowPaging = _allowPaging;
                grid.DataSource = PageFunction == null ? DataFunction() : PageFunction().DataTable;
                if (_allowPaging)
                {
                    this.ActualRowCount = PageFunction == null ? DataTotalCountFunction() : GetInt(PageFunction().TotalItems);
                }
                if (_allowPaging)
                {
                    try
                    {
                        int tempPageIndex;
                        tempPageIndex = grid.PageIndex;
                        grid.DataBind();
                        grid.PageIndex = tempPageIndex;
                    }
                    catch (Exception ex) { }
                }
                else
                {
                    grid.PageIndex = 0;
                    grid.DataBind();
                }
                IsDataBinD = true;
            }

        }

        public void DisplayData(bool retainPage)
        {
            if (GetInt(txtGoToPage.Text) == 0)
            {
                txtGoToPage.Text = "1";
            }
            if (retainPage)
            {
                if (txtGoToPage.Text != string.Empty)
                {
                    grid.PageIndex = Convert.ToInt32(txtGoToPage.Text) - 1;
                }
            }
            string currentPos = HttpContext.Current.Request.Path;

            if (currentPos.Contains("M14_ResponsibleItemTypeList.aspx") == true)
            {
                searchBox.Dispose();
            }
            else
            {
                if (!_allowSearch)
                {
                    searchBox.Visible = false;
                }
                else
                {
                    searchBox.Visible = true;
                }
            }

            grid.PageSize = int.Parse(ddlPageSize.SelectedValue);
            if (IsDisplayed)
            {
                return;
            }
            if ((DataFunction == null || (_allowPaging && DataTotalCountFunction == null)) && PageFunction == null)
            {
                grid.DataSource = null;
                this.ActualRowCount = 0;
            }
            else
            {
                if (_allowSorting)
                {
                    if (String.IsNullOrEmpty(this.SortExpression))
                    {
                        this.SortExpression = _defaultSortExpression;
                    }
                    if (String.IsNullOrEmpty(this.SortDirection))
                    {
                        this.SortDirection = _defaultSortDirection;
                    }
                }
                grid.AllowSorting = _allowSorting;
                grid.AllowPaging = _allowPaging;
                object table = PageFunction == null ? DataFunction() : PageFunction().DataTable;
                if (table == null)
                {
                    grid.DataSource = null;
                    this.ActualRowCount = 0;
                }
                else
                {
                    if (_allowEdit && !IsEditButtonCreated)
                    {
                        CommandField cmdEdit = new CommandField();
                        cmdEdit.ButtonType = ButtonType.Link;
                        cmdEdit.ShowEditButton = true;
                        cmdEdit.HeaderText = "Edit";
                        cmdEdit.EditText = String.Format("<img alt='Edit' src='{0}/WebGridViewImages/edit.gif'>", _imagePath);
                        cmdEdit.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        cmdEdit.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        grid.Columns.Insert(0, cmdEdit);
                        IsEditButtonCreated = true;
                    }
                    if (_allowDelete && !IsDeleteButtonCreated)
                    {
                        CommandField cmdDelete = new CommandField();
                        cmdDelete.ButtonType = ButtonType.Link;
                        cmdDelete.ShowDeleteButton = true;
                        cmdDelete.HeaderText = "Delete";
                        cmdDelete.DeleteText = String.Format("<img alt='Delete' src='{0}/WebGridViewImages/delete.gif' onclick='return confirm(\"Are you sure to delete this record ?\");'>", _imagePath);
                        cmdDelete.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                        cmdDelete.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                        if (_allowEdit)
                        {
                            grid.Columns.Insert(1, cmdDelete);
                        }
                        else
                        {
                            grid.Columns.Insert(0, cmdDelete);
                        }
                        IsDeleteButtonCreated = true;
                    }
                    grid.DataSource = table;
                    if (_allowPaging)
                    {
                        this.ActualRowCount = PageFunction == null ? DataTotalCountFunction() : GetInt(PageFunction().TotalItems);
                    }
                }
            }
            if (_allowPaging)
            {
                try
                {
                    int tempPageIndex;
                    tempPageIndex = grid.PageIndex;
                    grid.DataBind();
                    grid.PageIndex = tempPageIndex;
                }
                catch (Exception ex) { }
            }
            else
            {
                grid.PageIndex = 0;
                grid.DataBind();
            }
            if (_showTotalRecords)
            {
                if (_allowPaging)
                {
                    lblTotalRecords.Text = String.Format("Total Data : {0}", this.ActualRowCount);
                }
                else
                {
                    lblTotalRecords.Text = String.Format("Total Data : {0}", grid.Rows.Count);
                }
            }
            if (grid.Rows.Count > 0 && _allowPaging)
            {
                paging.Visible = true;
                int pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(this.ActualRowCount) / grid.PageSize));
                lblTotalNumberOfPages.Text = pageCount.ToString();
                txtGoToPage.Text = (grid.PageIndex + 1).ToString();
                //ddlPageSize.SelectedValue = grid.PageSize.ToString();
            }
            else
            {
                paging.Visible = false;
            }
            adjustNextAndPrevButton(grid.PageIndex);
            IsDisplayed = true;
        }

        #endregion

        #region Event Handler

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            grid.PageSize = int.Parse(ddlPageSize.SelectedValue);
            grid.PageIndex = 0;
            DisplayData(false);
        }

        protected void GoToPage_TextChanged(object sender, EventArgs e)
        {
            int pageNumber;
            int pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(this.ActualRowCount) / grid.PageSize));
            if (int.TryParse(txtGoToPage.Text.Trim(), out pageNumber) && pageNumber > 0 && pageNumber <= pageCount)
            {
                LoadPage(pageNumber - 1);
            }
            else
            {
                LoadPage(0);
            }
        }

        protected void btnPrev_OnClick(object sender, EventArgs e)
        {
            int pageNumber;
            if (int.TryParse(txtGoToPage.Text.Trim(), out pageNumber) && pageNumber > 1)
            {
                LoadPage(pageNumber - 2);
            }
            else
            {
                LoadPage(grid.PageIndex);
            }
        }

        protected void btnNext_OnClick(object sender, EventArgs e)
        {
            int pageNumber;
            int pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(this.ActualRowCount) / grid.PageSize));
            if (int.TryParse(txtGoToPage.Text.Trim(), out pageNumber) && pageNumber < pageCount)
            {
                LoadPage(pageNumber);
            }
            else
            {
                LoadPage(grid.PageIndex);
            }
        }

        protected void grid_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (String.IsNullOrEmpty(this.SortExpression))
            {
                this.SortDirection = "ASC";
            }
            else if (e.SortExpression == this.SortExpression)
            {
                this.SortDirection = (this.SortDirection == "ASC" ? "DESC" : "ASC");
            }
            else
            {
                this.SortDirection = "ASC";
            }
            this.SortExpression = e.SortExpression;
            DisplayData(false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PanelLegend.Visible = DisplayLegend;
                grid.ShowFooter = _showFooter;
                for (int i = 0; i < _freezeLeftMostColumn; i++)
                {
                    grid.Columns[i].ItemStyle.CssClass = "freeze-left";
                    grid.Columns[i].HeaderStyle.CssClass = "freeze-left freeze-header";
                }
                if (!String.IsNullOrEmpty(_title) || _allowSearch)
                {
                    gridTitle.Visible = true;
                }
                else
                {
                    gridTitle.Visible = false;
                }
                if (!String.IsNullOrEmpty(_title))
                {
                    headerLabel.Visible = true;
                    lblGridTitle.Visible = true;
                    lblGridTitle.Text = _title;
                }
                else
                {
                    headerLabel.Visible = false;
                    lblGridTitle.Visible = false;
                    lblGridTitle.Text = String.Empty;
                }
                if (_allowSearch)
                {
                    searchBox.Visible = true;
                    if (_useCustomSearchCriterias)
                    {
                        foreach (Criteria criteria in _criterias)
                        {
                            ddlSearchCriteria.Items.Add(new ListItem(criteria.Description, criteria.Field));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < grid.Columns.Count; i++)
                        {
                            if (grid.Columns[i] is BoundField)
                            {
                                ddlSearchCriteria.Items.Add(new ListItem(grid.Columns[i].HeaderText, ((BoundField)grid.Columns[i]).DataField));
                            }
                        }
                    }
                }
                else
                {
                    searchBox.Visible = false;
                }
                if (_allowPaging || _showTotalRecords)
                {
                    dataPager.Visible = true;
                }
                else
                {
                    dataPager.Visible = false;
                }
                if (_showTotalRecords)
                {
                    lblTotalRecords.Visible = true;
                }
                else
                {
                    lblTotalRecords.Visible = false;
                }
                if (_showBorder)
                {
                    //gridBorder.Attributes.Add("style", "border-top: solid 1px #DDD");

                }
            }
            else
            {
                DisplayOnPostBack();
            }
        }

        protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (RowDataBound != null)
            {
                RowDataBound(sender, e);
            }

            if (!String.IsNullOrEmpty(this.SortExpression) && !String.IsNullOrEmpty(this.SortDirection))
            {
                int cellIndex = -1;
                foreach (DataControlField field in grid.Columns)
                {
                    if (field.SortExpression == this.SortExpression)
                    {
                        cellIndex = grid.Columns.IndexOf(field);
                        break;
                    }
                }
                if (cellIndex > -1)
                {
                    if (e.Row.RowType == DataControlRowType.Header)
                    {
                        if (cellIndex < _freezeLeftMostColumn)
                        {
                            e.Row.Cells[cellIndex].CssClass = this.SortDirection == "ASC" ? "freeze-left freeze-header sortascheaderstyle" : "freeze-left freeze-header sortdescheaderstyle";
                        }
                        else
                        {
                            e.Row.Cells[cellIndex].CssClass = this.SortDirection == "ASC" ? "sortascheaderstyle" : "sortdescheaderstyle";
                        }
                    }
                    else if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        if (cellIndex < _freezeLeftMostColumn)
                        {
                            e.Row.Cells[cellIndex].CssClass = e.Row.RowIndex % 2 == 0 ? "freeze-left sortalternatingrowstyle" : "freeze-left sortrowstyle";
                        }
                        else
                        {
                            e.Row.Cells[cellIndex].CssClass = e.Row.RowIndex % 2 == 0 ? "sortalternatingrowstyle" : "sortrowstyle";
                        }
                    }
                }
            }
        }

        protected void grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grid.PageIndex = e.NewPageIndex;
            DisplayData(false);
        }

        protected void grid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (Editing != null)
            {
                Editing(e.NewEditIndex);
            }
        }

        protected void grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (Deleting != null)
            {
                Deleting(e.RowIndex);
                DisplayData(false);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataRow dr;
            if (SearchCriterias == null)
            {
                SearchCriterias = new DataTable("SearchCriterias");
                SearchCriterias.Columns.Add("Key", typeof(String));
                SearchCriterias.Columns.Add("Desc", typeof(String));
                SearchCriterias.Columns.Add("Field", typeof(String));
                SearchCriterias.Columns.Add("Value", typeof(String));
            }
            //SearchCriterias.Rows.Clear();
            if (txtSearchCriteria.Text.Trim() != string.Empty)
            {
                dr = SearchCriterias.NewRow();
                dr["Key"] = SearchCriterias.Rows.Count.ToString();
                dr["Desc"] = ddlSearchCriteria.SelectedItem.Text.Trim();
                dr["Field"] = ddlSearchCriteria.SelectedValue;
                dr["Value"] = txtSearchCriteria.Text.Trim();
                SearchCriterias.Rows.Add(dr);
                criterias.DataSource = SearchCriterias;
                criterias.DataBind();

                txtSearchCriteria.Text = "";
            }
            grid.PageIndex = 0;
            DisplayData(false);
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (SearchCriterias != null)
            {
                SearchCriterias.Rows.Clear();
            }
            ddlSearchCriteria.SelectedIndex = 0;
            txtSearchCriteria.Text = String.Empty;
            DisplayData(false);
        }

        //multi filter
        //protected void btnAdd_Click(object sender, EventArgs e)
        //{
        //    DataRow dr;
        //    if (SearchCriterias == null)
        //    {
        //        SearchCriterias = new DataTable("SearchCriterias");
        //        SearchCriterias.Columns.Add("Key", typeof(String));
        //        SearchCriterias.Columns.Add("Desc", typeof(String));
        //        SearchCriterias.Columns.Add("Field", typeof(String));
        //        SearchCriterias.Columns.Add("Value", typeof(String));
        //    }
        //    if (SearchCriterias.Select(String.Format("Field = '{0}' AND Value = '{1}'"
        //        , ddlSearchCriteria.SelectedValue, txtSearchCriteria.Text.Trim())).Length == 0)
        //    {
        //        dr = SearchCriterias.NewRow();
        //        dr["Key"] = SearchCriterias.Rows.Count.ToString();
        //        dr["Desc"] = ddlSearchCriteria.SelectedItem.Text.Trim();
        //        dr["Field"] = ddlSearchCriteria.SelectedValue;
        //        dr["Value"] = txtSearchCriteria.Text.Trim();
        //        SearchCriterias.Rows.Add(dr);
        //        criterias.DataSource = SearchCriterias;
        //        criterias.DataBind();
        //        DisplayData();
        //    }
        //    ddlSearchCriteria.SelectedIndex = 0;
        //    txtSearchCriteria.Text = String.Empty;
        //}

        protected void LinkRemove_Click(object sender, EventArgs e)
        {
            LinkButton link = sender as LinkButton;
            string key = link.CommandArgument;
            SearchCriterias.DefaultView.Sort = "Key";
            SearchCriterias.Rows.RemoveAt(SearchCriterias.DefaultView.Find(key));
            criterias.DataSource = SearchCriterias;
            criterias.DataBind();
            DisplayData(false);
            txtSearchCriteria.Text = String.Empty;
        }

        protected void criterias_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Remove")
            {
                String key = e.CommandArgument.ToString();
                SearchCriterias.DefaultView.Sort = "Key";
                SearchCriterias.Rows.RemoveAt(SearchCriterias.DefaultView.Find(key));
                criterias.DataSource = SearchCriterias;
                criterias.DataBind();
                DisplayData(false);
                txtSearchCriteria.Text = String.Empty;
            }
        }

        protected void grid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (CancelEditing != null)
            {
                CancelEditing(e.RowIndex);
            }
        }

        protected void grid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (Updating != null)
            {
                Updating(e.RowIndex);
            }
        }

        protected void grid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (RowCommand != null)
            {
                RowCommand(sender, e);
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            if (DataBound != null)
            {
                DataBound(sender, e);
            }
        }

        protected void grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedIndexChanged != null)
            {
                SelectedIndexChanged(sender, e);
            }
        }

        #endregion

        public void DisplayDataBySessionCriteria()
        {
            if (Session["SessionCriteria"] != null)
            {
                SearchCriterias = ((DataTable)Session["SessionCriteria"]).Copy();
                criterias.DataSource = SearchCriterias;
                criterias.DataBind();
                DisplayData(false);
            }
            Session["SessionCriteria"] = null;
        }

        public static int GetInt(object obj)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public String GetStringSearchCriterias()
        {
            if (Session["SessionCriteria"] != null)
            {
                SearchCriterias = ((DataTable)Session["SessionCriteria"]).Copy();
                criterias.DataSource = SearchCriterias;
                criterias.DataBind();
                Session["SessionCriteria"] = null;
            }

            if (_allowSearch && SearchCriterias != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("1=1");
                foreach (DataRow row in SearchCriterias.Rows)
                {
                    sb.Append(string.Format(" AND {0} LIKE '%{1}%'", row["Field"], row["Value"]));
                }

                return sb.ToString();
            }
            return null;
        }

        protected void criterias_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            ScriptManager scriptMan = ScriptManager.GetCurrent(this.Page);
            LinkButton btn = e.Item.FindControl("lb") as LinkButton;
            if (btn != null)
            {
                scriptMan.RegisterAsyncPostBackControl(btn);
            }
        } 
    }
}