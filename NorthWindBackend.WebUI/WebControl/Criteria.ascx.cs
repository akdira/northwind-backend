﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls; 

namespace NorthWindBackend.WebUI.WebControl
{
    public partial class Criteria : UserControl
    {
        #region Fields

        private String _field;
        private String _description;
        private GridFilterDataType _dataType = GridFilterDataType.Text;

        #endregion

        #region Properties

        public String Field
        {
            get
            {
                return _field;
            }
            set
            {
                _field = value;
            }
        }

        public String Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public GridFilterDataType DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }

    public enum GridFilterDataType
    {
        Text = 0,
        DateTime,
        Boolean,
        Int,
        Date
    }
}