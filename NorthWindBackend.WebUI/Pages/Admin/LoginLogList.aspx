<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="LoginLogList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Admin.LoginLogList" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('LoginLog', 3); 
        } 
        
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">LoginLog List</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>                            
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridViewLoginLogList" runat="server" AllowPaging="true" DefaultSortExpression="LoginID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("LoginID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("LoginID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("LoginID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
									<asp:BoundField HeaderText="LoginID" DataField="LoginID" SortExpression="LoginID" >
										<HeaderStyle Width="16.7%" />
										<ItemStyle Width="16.7%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="LoginUser" DataField="LoginUser" SortExpression="LoginUser" >
										<HeaderStyle Width="16.7%" />
										<ItemStyle Width="16.7%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="LoginIP" DataField="LoginIP" SortExpression="LoginIP" >
										<HeaderStyle Width="16.7%" />
										<ItemStyle Width="16.7%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="LoginTime" DataField="LoginTime" SortExpression="LoginTime" DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="false">
										<HeaderStyle Width="16.7%" />
										<ItemStyle Width="16.7%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="LoginOutTime" DataField="LoginOutTime" SortExpression="LoginOutTime" DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="false">
										<HeaderStyle Width="16.7%" />
										<ItemStyle Width="16.7%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="LoginDescription" DataField="LoginDescription" SortExpression="LoginDescription" >
										<HeaderStyle Width="16.7%" />
										<ItemStyle Width="16.7%" />
									</asp:BoundField>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="loginlogdetail" class="modal fade" role="dialog">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="modal-dialog"> 
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;LoginLog Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">LoginID</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextLoginID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">LoginUser </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextLoginUser" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">LoginIP </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextLoginIP" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">LoginTime </label>
                                        <div class="col-md-4">
                                            <div class="input-group date">
                                                <asp:TextBox ID="TextLoginTime" runat="server" CssClass="form-control"  ></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">LoginOutTime </label>
                                        <div class="col-md-4">
                                            <div class="input-group date">
                                                <asp:TextBox ID="TextLoginOutTime" runat="server" CssClass="form-control"  placeholder="Harus diisi"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">LoginDescription </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextLoginDescription" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>                                            
                                        </div>
                                    </div>
									 
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
                            <asp:LinkButton ID="ButtonClose" runat="server" CssClass="btn btn-danger" OnClientClick="$('#loginlogdetail').modal('hide')"><i class="fa fa-remove">&nbsp;</i>Tutup</asp:LinkButton>                            
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
