using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.WebControl;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class ActivityLogDetail : UserControl
    { 
        public PageState PageState
        {
            get
            {
                if (ViewState["PAGESTATE"] == null)
                {
                    ViewState["PAGESTATE"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE"];
            }
            set
            {
                ViewState["PAGESTATE"] = value;
            }
        }

        public ActivityLog OActivityLog
        {
            get
            {
                if(ViewState["OACTIVITYLOG"] == null)
                {
                    return null;
                }
               return  ViewState["OACTIVITYLOG"] as ActivityLog;
            }
            set
            {
                ViewState["OACTIVITYLOG"] = value;
            }
        }

        public int DataID
        {
            get
            {
                if(ViewState["DATA_ID"] == null)
                {
                    return 0;
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        public WebGridView GridViewList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        } 

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessSave();
            }
        }
		
        public void ProcessDetail()
        {
            switch (PageState)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OActivityLog = new ActivityLog();
        }

        private void PopulateControlsInNewState()
        {
            TextActivityID.Text = "";
			TextActivityUser.Text = "";
			TextActivityIP.Text = "";
			TextActivityDescription.Text = "";
			TextActivityDate.Text = "";
			
        }

        private void AdjustControlInNewState()
        {
            TextActivityID.ReadOnly = false;
			TextActivityUser.ReadOnly = false;
			TextActivityIP.ReadOnly = false;
			TextActivityDescription.ReadOnly = false;
			TextActivityDate.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
            var facade = new ActivityLogFacade();
            OActivityLog = facade.GetObject(DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OActivityLog != null)
            {
                TextActivityID.Text = OActivityLog.ActivityID + "";
				TextActivityUser.Text = OActivityLog.ActivityUser;
				TextActivityIP.Text = OActivityLog.ActivityIP;
				TextActivityDescription.Text = OActivityLog.ActivityDescription;
				TextActivityDate.Text = Commons.GetDateString(OActivityLog.ActivityDate);
				
            }
        }

        private void AdjustControlInEditState()
        {
            TextActivityID.ReadOnly = false;
			TextActivityUser.ReadOnly = false;
			TextActivityIP.ReadOnly = false;
			TextActivityDescription.ReadOnly = false;
			TextActivityDate.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new ActivityLogFacade();
            OActivityLog = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OActivityLog != null)
            {
                TextActivityID.Text = OActivityLog.ActivityID + "";
				TextActivityUser.Text = OActivityLog.ActivityUser;
				TextActivityIP.Text = OActivityLog.ActivityIP;
				TextActivityDescription.Text = OActivityLog.ActivityDescription;
				TextActivityDate.Text = Commons.GetDateString(OActivityLog.ActivityDate);
				
            }
        }

        private void AdjustControlInViewState()
        {
            TextActivityID.ReadOnly = true;
			TextActivityUser.ReadOnly = true;
			TextActivityIP.ReadOnly = true;
			TextActivityDescription.ReadOnly = true;
			TextActivityDate.ReadOnly = true;
			
            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = null;
                PopulateObject();
                if(PageState == PageState.New)
                {
                    result = facade.DataInsert(OActivityLog);
                }
                else
                {
                    result = facade.DataUpdate(OActivityLog);
                }
                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage, "$('#activitylogdetail').modal('hide')");
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
					OActivityLog.ActivityUser = TextActivityUser.Text;
				OActivityLog.ActivityIP = TextActivityIP.Text;
				OActivityLog.ActivityDescription = TextActivityDescription.Text;
				OActivityLog.ActivityDate = Commons.GetDateTime(TextActivityDate.Text);
				
            }
            catch (Exception ex) { }
        } 

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty; 
			if (string.IsNullOrEmpty(TextActivityUser.Text))
            { 
                msgList.Add(new ErrorMessage(TextActivityUser.ClientID, "ActivityUser harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextActivityIP.Text))
            { 
                msgList.Add(new ErrorMessage(TextActivityIP.ClientID, "ActivityIP harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextActivityDescription.Text))
            { 
                msgList.Add(new ErrorMessage(TextActivityDescription.ClientID, "ActivityDescription harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextActivityDate.Text))
            { 
                msgList.Add(new ErrorMessage(TextActivityDate.ClientID, "ActivityDate harus diisi!", true));
            } 
			 

            if (msgList.Count == 0)
            {
                return true;
            }
            else { 
                Message.ShowErrorMessage(this.Page, msgList); 
                return false;
            }
        }

        private void ProcessDelete()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = facade.DataDelete(DataID);
                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage);
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        } 
    }
}
