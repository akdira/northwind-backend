<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="ErrorLogList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Admin.ErrorLogList" %>
<%@ Register Src="~/Pages/Admin/ErrorLogDetail.ascx" TagName="ErrorLogDetail" TagPrefix="dtl" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Error Log', 3); 
        } 
        
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">ErrorLog List</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>                            
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridViewErrorLogList" runat="server" AllowPaging="true" DefaultSortExpression="ErrorLogID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("ErrorLogID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("ErrorLogID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("ErrorLogID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
									<asp:BoundField HeaderText="ErrorLogID" DataField="ErrorLogID" SortExpression="ErrorLogID" >
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ErrorDate" DataField="ErrorDate" SortExpression="ErrorDate" DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="false">
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ErrorSourceIP" DataField="ErrorSourceIP" SortExpression="ErrorSourceIP" >
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ErrorSourceUser" DataField="ErrorSourceUser" SortExpression="ErrorSourceUser" >
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ErrorSourcePage" DataField="ErrorSourcePage" SortExpression="ErrorSourcePage" >
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ErrorDescription" DataField="ErrorDescription" SortExpression="ErrorDescription" >
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="ErrorRemark" DataField="ErrorRemark" SortExpression="ErrorRemark" >
										<HeaderStyle Width="14.3%" />
										<ItemStyle Width="14.3%" />
									</asp:BoundField>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="errorlogdetail" class="modal fade" role="dialog">
        <dtl:ErrorLogDetail ID="ErrorLogDetail1" runat="server" />
    </div>
</asp:Content>
