using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.Pages.MasterPages;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class ApplicationParameterGroupList : System.Web.UI.Page
    { 
        private PageState PageStateDetail
        {
            get
            {
                if (ViewState["PAGESTATE_DETAIL"] == null)
                {
                    ViewState["PAGESTATE_DETAIL"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE_DETAIL"];
            }
            set
            {
                ViewState["PAGESTATE_DETAIL"] = value;
            }
        }

        private ApplicationParameterGroup OApplicationParameterGroup
        {
            get
            {
                if(ViewState["OAPPLICATIONPARAMETERGROUP"] == null)
                {
                    return null;
                }
               return  ViewState["OAPPLICATIONPARAMETERGROUP"] as ApplicationParameterGroup;
            }
            set
            {
                ViewState["OAPPLICATIONPARAMETERGROUP"] = value;
            }
        }

        private int DataID
        {
            get
            {
                if(ViewState["DATA_ID"] == null)
                {
                    return 0;
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Admin");
             
            GridViewApplicationParameterGroupList.PageFunction = PageFunction;
            GridViewApplicationParameterList.PageFunction = PageFunctionList;
             
            if (!Page.IsPostBack)
            {
                GridViewApplicationParameterGroupList.DisplayData(); 
            }
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            PageStateDetail = PageState.View;
            ProcessDetail();
            Message.RunScript(this, "$('#applicationparametergroupdetail').modal()");
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessSave();
            }
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            PageStateDetail = PageState.Edit;
            ProcessDetail();
            Message.RunScript(this, "$('#applicationparametergroupdetail').modal()"); 
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            DataID = 0;
            PageStateDetail = PageState.New;
            ProcessDetail();
            Message.RunScript(this, "$('#applicationparametergroupdetail').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridViewApplicationParameterGroupList;
            var facade = new ApplicationParameterGroupFacade(); 
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);            
        } 

        private PetaPoco.Page PageFunctionList()
        {
            var grid = GridViewApplicationParameterList;
            var facade = new ApplicationParameterFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias() + " AND ParamGroup = "+OApplicationParameterGroup.ParamGroupID, grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        private void ProcessDetail()
        {
            switch (PageStateDetail)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
            GridViewApplicationParameterList.DisplayData();
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OApplicationParameterGroup = new ApplicationParameterGroup();
        }

        private void PopulateControlsInNewState()
        {
            TextParamGroupID.Text = "";
			TextParamGroupName.Text = "";
			TextParamGroupDescription.Text = "";
			TextParamGroupCode.Text = "";
			CheckParamGroupAddNewFlag.Checked = false; 
			TextModifiedBy.Text = Commons.GetCurrentLoginUser();
            TextModifiedDate.Text = DateTime.Now.ToString(Constants.DATE_FORMAT_STRING);

        }

        private void AdjustControlInNewState()
        {
            TextParamGroupID.ReadOnly = false;
			TextParamGroupName.ReadOnly = false;
			TextParamGroupDescription.ReadOnly = false;
			TextParamGroupCode.ReadOnly = false;
			CheckParamGroupAddNewFlag.Enabled = true; 
			TextModifiedBy.ReadOnly = false;
			TextModifiedDate.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
            var facade = new ApplicationParameterGroupFacade();
            OApplicationParameterGroup = facade.GetObject(DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OApplicationParameterGroup != null)
            {
                TextParamGroupID.Text = OApplicationParameterGroup.ParamGroupID + "";
				TextParamGroupName.Text = OApplicationParameterGroup.ParamGroupName;
				TextParamGroupDescription.Text = OApplicationParameterGroup.ParamGroupDescription;
				TextParamGroupCode.Text = OApplicationParameterGroup.ParamGroupCode;
				CheckParamGroupAddNewFlag.Checked = OApplicationParameterGroup.ParamGroupAddNewFlag; 
				TextModifiedBy.Text = OApplicationParameterGroup.ModifiedBy;
				TextModifiedDate.Text = Commons.GetDateString(OApplicationParameterGroup.ModifiedDate);
				
            }
        }

        private void AdjustControlInEditState()
        {
            TextParamGroupID.ReadOnly = false;
			TextParamGroupName.ReadOnly = false;
			TextParamGroupDescription.ReadOnly = false;
			TextParamGroupCode.ReadOnly = false;
			CheckParamGroupAddNewFlag.Enabled = true; 
			TextModifiedBy.ReadOnly = false;
			TextModifiedDate.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new ApplicationParameterGroupFacade();
            OApplicationParameterGroup = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OApplicationParameterGroup != null)
            {
                TextParamGroupID.Text = OApplicationParameterGroup.ParamGroupID + "";
				TextParamGroupName.Text = OApplicationParameterGroup.ParamGroupName;
				TextParamGroupDescription.Text = OApplicationParameterGroup.ParamGroupDescription;
				TextParamGroupCode.Text = OApplicationParameterGroup.ParamGroupCode;
				CheckParamGroupAddNewFlag.Checked = OApplicationParameterGroup.ParamGroupAddNewFlag; 
				TextModifiedBy.Text = OApplicationParameterGroup.ModifiedBy;
				TextModifiedDate.Text = Commons.GetDateString(OApplicationParameterGroup.ModifiedDate);
				
            }
        }

        private void AdjustControlInViewState()
        {
            TextParamGroupID.ReadOnly = true;
			TextParamGroupName.ReadOnly = true;
			TextParamGroupDescription.ReadOnly = true;
			TextParamGroupCode.ReadOnly = true;
			CheckParamGroupAddNewFlag.Enabled = false; 
			TextModifiedBy.ReadOnly = true;
			TextModifiedDate.ReadOnly = true;
			
            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ApplicationParameterGroupFacade();
                ActionResult result = null;
                PopulateObject();
                if(PageStateDetail == PageState.New)
                {
                    result = facade.DataInsert(OApplicationParameterGroup);
                }
                else
                {
                    result = facade.DataUpdate(OApplicationParameterGroup);
                }
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage, "$('#applicationparametergroupdetail').modal('hide')");
                    GridViewApplicationParameterGroupList.DisplayData();
                }
                else
                {
                    Message.Show(this, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            { 
				OApplicationParameterGroup.ParamGroupName = TextParamGroupName.Text;
				OApplicationParameterGroup.ParamGroupDescription = TextParamGroupDescription.Text;
				OApplicationParameterGroup.ParamGroupCode = TextParamGroupCode.Text;
				OApplicationParameterGroup.ParamGroupAddNewFlag = CheckParamGroupAddNewFlag.Checked;
				OApplicationParameterGroup.CreatedBy = PageStateDetail == PageState.New ? Commons.GetCurrentLoginUser() : OApplicationParameterGroup.CreatedBy;
				OApplicationParameterGroup.CreatedDate = PageStateDetail == PageState.New ? DateTime.Now : OApplicationParameterGroup.CreatedDate;
				OApplicationParameterGroup.ModifiedBy = TextModifiedBy.Text;
				OApplicationParameterGroup.ModifiedDate = Commons.GetDateTime(TextModifiedDate.Text);
				
            }
            catch (Exception ex) { }
        } 

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty; 
			if (string.IsNullOrEmpty(TextParamGroupName.Text))
            { 
                msgList.Add(new ErrorMessage(TextParamGroupName.ClientID, "Nama group harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextParamGroupDescription.Text))
            { 
                msgList.Add(new ErrorMessage(TextParamGroupDescription.ClientID, "Deskripsi harus diisi!"));
            }
            if (string.IsNullOrEmpty(TextParamGroupCode.Text))
            {
                msgList.Add(new ErrorMessage(TextParamGroupCode.ClientID, "Kode parameter harus diisi!"));
            } 
			 

            if (msgList.Count == 0)
            {
                return true;
            }
            else { 
                Message.ShowErrorMessage(this, msgList); 
                return false;
            }
        }

        private void ProcessDelete()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ApplicationParameterGroupFacade();
                ActionResult result = facade.DataDelete(DataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridViewApplicationParameterGroupList.DisplayData();
                }
                else
                {
                    Message.Show(this, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        } 
    }
}
