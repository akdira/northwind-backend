using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.WebControl;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class ErrorLogDetail : UserControl
    { 
        public PageState PageState
        {
            get
            {
                if (ViewState["PAGESTATE"] == null)
                {
                    ViewState["PAGESTATE"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE"];
            }
            set
            {
                ViewState["PAGESTATE"] = value;
            }
        }

        public ErrorLog OErrorLog
        {
            get
            {
                if(ViewState["OERRORLOG"] == null)
                {
                    return null;
                }
               return  ViewState["OERRORLOG"] as ErrorLog;
            }
            set
            {
                ViewState["OERRORLOG"] = value;
            }
        }

        public int DataID
        {
            get
            {
                if(ViewState["DATA_ID"] == null)
                {
                    return 0;
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        public WebGridView GridViewList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        } 

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessSave();
            }
        }
		
        public void ProcessDetail()
        {
            switch (PageState)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OErrorLog = new ErrorLog();
        }

        private void PopulateControlsInNewState()
        {
            TextErrorLogID.Text = "";
			TextErrorDate.Text = "";
			TextErrorSourceIP.Text = "";
			TextErrorSourceUser.Text = "";
			TextErrorSourcePage.Text = "";
			TextErrorDescription.Text = "";
			TextErrorRemark.Text = "";
			
        }

        private void AdjustControlInNewState()
        {
            TextErrorLogID.ReadOnly = false;
			TextErrorDate.ReadOnly = false;
			TextErrorSourceIP.ReadOnly = false;
			TextErrorSourceUser.ReadOnly = false;
			TextErrorSourcePage.ReadOnly = false;
			TextErrorDescription.ReadOnly = false;
			TextErrorRemark.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
            var facade = new ErrorLogFacade();
            OErrorLog = facade.GetObject(DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OErrorLog != null)
            {
                TextErrorLogID.Text = OErrorLog.ErrorLogID + "";
				TextErrorDate.Text = Commons.GetDateString(OErrorLog.ErrorDate);
				TextErrorSourceIP.Text = OErrorLog.ErrorSourceIP;
				TextErrorSourceUser.Text = OErrorLog.ErrorSourceUser;
				TextErrorSourcePage.Text = OErrorLog.ErrorSourcePage;
				TextErrorDescription.Text = OErrorLog.ErrorDescription;
				TextErrorRemark.Text = OErrorLog.ErrorRemark;
				
            }
        }

        private void AdjustControlInEditState()
        {
            TextErrorLogID.ReadOnly = false;
			TextErrorDate.ReadOnly = false;
			TextErrorSourceIP.ReadOnly = false;
			TextErrorSourceUser.ReadOnly = false;
			TextErrorSourcePage.ReadOnly = false;
			TextErrorDescription.ReadOnly = false;
			TextErrorRemark.ReadOnly = false;
			
            ButtonSave.Visible = true;
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new ErrorLogFacade();
            OErrorLog = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OErrorLog != null)
            {
                TextErrorLogID.Text = OErrorLog.ErrorLogID + "";
				TextErrorDate.Text = Commons.GetDateString(OErrorLog.ErrorDate);
				TextErrorSourceIP.Text = OErrorLog.ErrorSourceIP;
				TextErrorSourceUser.Text = OErrorLog.ErrorSourceUser;
				TextErrorSourcePage.Text = OErrorLog.ErrorSourcePage;
				TextErrorDescription.Text = OErrorLog.ErrorDescription;
				TextErrorRemark.Text = OErrorLog.ErrorRemark;
				
            }
        }

        private void AdjustControlInViewState()
        {
            TextErrorLogID.ReadOnly = true;
			TextErrorDate.ReadOnly = true;
			TextErrorSourceIP.ReadOnly = true;
			TextErrorSourceUser.ReadOnly = true;
			TextErrorSourcePage.ReadOnly = true;
			TextErrorDescription.ReadOnly = true;
			TextErrorRemark.ReadOnly = true;
			
            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ErrorLogFacade();
                ActionResult result = null;
                PopulateObject();
                if(PageState == PageState.New)
                {
                    result = facade.DataInsert(OErrorLog);
                }
                else
                {
                    result = facade.DataUpdate(OErrorLog);
                }
                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage, "$('#errorlogdetail').modal('hide')");
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
					OErrorLog.ErrorDate = Commons.GetDateTime(TextErrorDate.Text);
				OErrorLog.ErrorSourceIP = TextErrorSourceIP.Text;
				OErrorLog.ErrorSourceUser = TextErrorSourceUser.Text;
				OErrorLog.ErrorSourcePage = TextErrorSourcePage.Text;
				OErrorLog.ErrorDescription = TextErrorDescription.Text;
				OErrorLog.ErrorRemark = TextErrorRemark.Text;
				
            }
            catch (Exception ex) { }
        } 

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty; 
			if (string.IsNullOrEmpty(TextErrorDate.Text))
            { 
                msgList.Add(new ErrorMessage(TextErrorDate.ClientID, "ErrorDate harus diisi!", true));
            } 
			if (string.IsNullOrEmpty(TextErrorSourceIP.Text))
            { 
                msgList.Add(new ErrorMessage(TextErrorSourceIP.ClientID, "ErrorSourceIP harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextErrorSourceUser.Text))
            { 
                msgList.Add(new ErrorMessage(TextErrorSourceUser.ClientID, "ErrorSourceUser harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextErrorSourcePage.Text))
            { 
                msgList.Add(new ErrorMessage(TextErrorSourcePage.ClientID, "ErrorSourcePage harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextErrorDescription.Text))
            { 
                msgList.Add(new ErrorMessage(TextErrorDescription.ClientID, "ErrorDescription harus diisi!"));
            } 
			if (string.IsNullOrEmpty(TextErrorRemark.Text))
            { 
                msgList.Add(new ErrorMessage(TextErrorRemark.ClientID, "ErrorRemark harus diisi!"));
            } 
			 

            if (msgList.Count == 0)
            {
                return true;
            }
            else { 
                Message.ShowErrorMessage(this.Page, msgList); 
                return false;
            }
        }

        private void ProcessDelete()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ErrorLogFacade();
                ActionResult result = facade.DataDelete(DataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        } 
    }
}
