﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.WebUI.Pages.MasterPages;
using System.Data;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;

namespace NorthWindBackend.WebUI.Pages.Admin
{
    public partial class WorkingDayList : System.Web.UI.Page
    {
        private PageState PageStateDetail
        {
            get
            {
                if (ViewState["PAGESTATE_DETAIL"] == null)
                {
                    ViewState["PAGESTATE_DETAIL"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE_DETAIL"];
            }
            set
            {
                ViewState["PAGESTATE_DETAIL"] = value;
            }
        }

        private WorkingDays OWorkingDays
        {
            get 
            {
                if (ViewState["OWorkingDays"] == null)
                {
                    return null;
                }
                return ViewState["OWorkingDays"] as WorkingDays;
            }
            set
            {
                ViewState["OWorkingDays"] = value;
            }
        }

        private int DataID
        {
            get
            {
                if (ViewState["DATA_ID"] == null)
                {
                    return 0;
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Admin");

            GridViewWorkingDayList.PageFunction = PageFunction;

            if (!Page.IsPostBack)
            {
                GridViewWorkingDayList.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridViewWorkingDayList;
            var facade = new WorkingDaysFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        private void ProcessDetail()
        {
            switch (PageStateDetail)
            {
                case PageState.New:
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                default:
                    break;
            }
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void ProcessEdit()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInEditState();
        }

        private void AdjustControlInEditState()
        {
            TextWKID.Enabled = false;
            TextWKDate.ReadOnly = true; 
            TextWKDayName.ReadOnly = true;
            CheckWKIsWorkingDay.Enabled = true;
            TextWKRemark.ReadOnly = false;
            TextModifiedBy.ReadOnly = true;
            TextModifiedDate.ReadOnly = true;
            ButtonSave.Visible = true;
        }

        private void DataReadInViewState()
        {
            var facade = new WorkingDaysFacade();
            OWorkingDays = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OWorkingDays != null)
            {
                TextWKID.Text = OWorkingDays.WKID + "";
                TextWKDate.Text = Commons.GetDateString(OWorkingDays.WKDate); 
                TextWKDayName.Text = OWorkingDays.WKDayName;
                CheckWKIsWorkingDay.Checked = OWorkingDays.WKIsWorkingDay;
                TextWKRemark.Text = OWorkingDays.WKRemark;
                TextModifiedBy.Text = OWorkingDays.ModifiedBy;
                TextModifiedDate.Text = Commons.GetDateString(OWorkingDays.ModifiedDate);
            }
        }

        private void AdjustControlInViewState()
        {

            TextWKID.Enabled = false;
            TextWKDate.ReadOnly = true; 
            TextWKDayName.ReadOnly = true;
            CheckWKIsWorkingDay.Enabled = false;
            TextWKRemark.ReadOnly = true;
            TextModifiedBy.ReadOnly = true;
            TextModifiedDate.ReadOnly = true;
            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        { 
            OWorkingDays.WKIsWorkingDay = CheckWKIsWorkingDay.Checked;
            OWorkingDays.WKRemark = TextWKRemark.Text;
            try
            {
                var facade = new WorkingDaysFacade();
                var result = facade.DataUpdate(OWorkingDays);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage, "$('#wkdetail').modal('hide')");
                    GridViewWorkingDayList.DisplayData();
                }
            }
            catch (Exception ex) { }
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            PageStateDetail = PageState.View;
            ProcessDetail();
            Message.RunScript(this, "$('#wkdetail').modal()");
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            ProcessSave();
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            DataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            PageStateDetail = PageState.Edit;
            ProcessDetail();
            Message.RunScript(this, "$('#wkdetail').modal()"); 
        }
    }
}