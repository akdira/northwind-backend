﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="WorkingDayList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Admin.WorkingDayList" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Hari Kerja', 3);
        }
    </script>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Hari Kerja</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridViewWorkingDayList" runat="server" AllowPaging="true" DefaultSortExpression="WKID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("WKID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("WKID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px; display: none" CommandArgument='<%# Eval("WKID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Are you sure want to delete this data?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="WKID" HeaderText="WK ID" SortExpression="WKID" />
                                    <asp:BoundField DataField="WKDate" HeaderText="Tanggal" SortExpression="WKDate" HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}"/>
                                    <asp:BoundField DataField="WKDayName" HeaderText="Day Name" SortExpression="WKDayName" />
                                    <asp:TemplateField HeaderText="Hari Kerja" SortExpression="WKIsWorkingDay">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckIsWorkingDay" runat="server" Enabled="false" Checked='<%# Eval("WKIsWorkingDay").ToString().Equals("1") %>'/>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="WKRemark" HeaderText="WKRemark" SortExpression="WKRemark" />
                                    <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"/>
                                    <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" SortExpression="ModifiedDate" HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}"/>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="wkdetail" class="modal fade" role="dialog">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="modal-dialog modal-md">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Hari Kerja Detail</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ID Hari Kerja</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextWKID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Tanggal</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextWKDate" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Nama Hari </label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextWKDayName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">&nbsp;</label>
                                        <div class="checkbox">
                                          <label style="padding-left: 34px;">
                                              <asp:CheckBox ID="CheckWKIsWorkingDay" runat="server" /> Hari Kerja
                                          </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Remark</label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextWKRemark" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Modified By </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextModifiedBy" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Modified Date </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextModifiedDate" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer"> 
                            <asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click" ><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>                            
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove">&nbsp;</i>Close</button>
                        </div>
                    </div> 
                </div> 
            </ContentTemplate>
        </asp:UpdatePanel>
    </div> 
</asp:Content>
