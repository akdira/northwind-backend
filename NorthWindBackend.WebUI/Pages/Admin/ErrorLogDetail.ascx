
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorLogDetail.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Admin.ErrorLogDetail" %>

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
	<ContentTemplate>
		<div class="modal-dialog"> 
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;ErrorLog Detail</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-horizontal">
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorLogID</label>
								<div class="col-md-4">
									<asp:TextBox ID="TextErrorLogID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorDate </label>
								<div class="col-md-4">
									<div class="input-group date">
										<asp:TextBox ID="TextErrorDate" runat="server" CssClass="form-control"  ></asp:TextBox>
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorSourceIP </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextErrorSourceIP" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorSourceUser </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextErrorSourceUser" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorSourcePage </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextErrorSourcePage" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorDescription </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextErrorDescription" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ErrorRemark </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextErrorRemark" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							 
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
					<asp:LinkButton ID="ButtonClose" runat="server" CssClass="btn btn-danger" OnClientClick="$('#errorlogdetail').modal('hide')"><i class="fa fa-remove">&nbsp;</i>Tutup</asp:LinkButton>                            
				</div>
			</div>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>
