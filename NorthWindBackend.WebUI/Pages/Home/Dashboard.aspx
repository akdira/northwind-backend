﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPages/Main.Master" CodeBehind="Dashboard.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Home.Dashboard" %>


<asp:Content ID="HeadContent" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu("Dashboard", 1);
        }
    </script>
</asp:Content>

<asp:Content ID="MainContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dashboard</h3>
                    </div>
                    <div class="form-horizontal">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="text-align: left" for="exampleInputEmail1">Email address 1</label>
                                    <div class="col-md-8"> 
                                        <asp:TextBox ID="TextNumeric" runat="server" CssClass="form-control numeric" placeholder="Enter Number" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="text-align: left" for="exampleInputEmail1">Email address</label>
                                    <div class="col-md-8">
                                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="text-align: left" for="exampleInputEmail1">Email address</label>
                                    <div class="col-md-8">
                                        <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Enter email" />
                                    </div>
                                </div>
                                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="Button" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
