﻿using NorthWindBackend.Common;
using NorthWindBackend.WebUI.WebControl;
using NorthWindBackend.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWind.Entity;
using PetaPoco;
using NorthWindBackend.Entity;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class OrderDetailItem : UserControl
    {
        public delegate void FormSubmitAdd(object sender, EventArgs e);
        public event FormSubmitAdd FormSubmit_Add;

        public delegate void FormSubmitEdit(object sender, EventArgs e);
        public event FormSubmitEdit FormSubmit_Edit;

        public int OrderID
        {
            get
            {
                if (TextOrderID.Text == null)
                {
                    TextOrderID.Text = "";
                }
                return Convert.ToInt32(TextOrderID.Text);
            }
            set
            {
                TextOrderID.Text = Convert.ToString(value);
            }
        }

        public Products SelectedProducts
        {
            get
            {
                if (ViewState["SelectedProducts"] == null)
                {
                    ViewState["SelectedProducts"] = new Products();
                }
                return (Products)ViewState["SelectedProducts"];
            }
            set
            {
                ViewState["SelectedProducts"] = value;
            }
        }

        public OrderDetailsMap OOrderDetailsMap
        {
            get
            {
                if (ViewState["OORDERDETAILSMAP"] == null)
                {
                    return null;
                }
                return ViewState["OORDERDETAILSMAP"] as OrderDetailsMap;
            }
            set
            {
                ViewState["OORDERDETAILSMAP"] = value;
            }
        }

        public PageState PageState
        {
            get
            {
                if (ViewState["PAGESTATE"] == null)
                {
                    ViewState["PAGESTATE"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE"];
            }
            set
            {
                ViewState["PAGESTATE"] = value;
            }
        }

        public string DataID
        {
            get
            {
                if (ViewState["DATA_ID"] == null)
                {
                    return String.Empty;
                }
                return Convert.ToString(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        internal void ProcessDetail()
        {
            switch (PageState)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
        }

        private void PopulateControlsInViewState()
        {
            if (OOrderDetailsMap != null)
            {

                TextOrderID.Text = Convert.ToString(OOrderDetailsMap.OrderID);
                TextProductName.Text = OOrderDetailsMap.ProductName;
                TextUnitPrice.Text = Convert.ToString(OOrderDetailsMap.UnitPrice);
                TextQuantity.Text = Convert.ToString(OOrderDetailsMap.Quantity);
                TextDiscount.Text = Convert.ToString(OOrderDetailsMap.Discount);



            }
        }

        private void AdjustControlInViewState()
        {
            TextProductName.ReadOnly = true;
            TextUnitPrice.ReadOnly = true;
            TextQuantity.ReadOnly = true;
            TextDiscount.ReadOnly = true;

            ButtonSave.Visible = false;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
        }

        private void PopulateControlsInEditState()
        {
            if (OOrderDetailsMap != null)
            {

                TextOrderID.Text = Convert.ToString(OOrderDetailsMap.OrderID);
                TextProductName.Text = OOrderDetailsMap.ProductName;
                TextUnitPrice.Text = Convert.ToString(OOrderDetailsMap.UnitPrice);
                TextQuantity.Text = Convert.ToString(OOrderDetailsMap.Quantity);
                TextDiscount.Text = Convert.ToString(OOrderDetailsMap.Discount);

            }
        }

        private void AdjustControlInEditState()
        {
            TextUnitPrice.ReadOnly = false;
            TextQuantity.ReadOnly = false;
            TextDiscount.ReadOnly = false;

            ButtonSave.Visible = true;
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
        }

        private void PopulateControlsInNewState()
        {

            TextProductName.Text = "";
            TextUnitPrice.Text = "";
            TextQuantity.Text = "";
            TextDiscount.Text = "";

        }

        private void AdjustControlInNewState()
        {
            AdjustControlInEditState();
        }

        public WebGridView GridViewList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessOnSave(sender, e);
            }
        }

        private void ProcessOnSave(object sender, EventArgs e)
        {
            string errorMsg = string.Empty;
            try
            {
                //var facade = new CustomerFacade();
                ActionResult result = null;
                PopulateObject();
                if (PageState == PageState.New)
                {
                    FormSubmit_Add(sender, e);
                    //result = facade.DataInsert(OOrderDetailsMap);
                }
                else
                {
                    FormSubmit_Edit(sender, e);
                    //result = facade.DataUpdate(OOrderDetailsMap);
                }
                Message.Show(this.Page, "Done", "$('#orderdetailitem').modal('hide')");
                //GridViewList.DisplayData();
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
                OOrderDetailsMap.OrderID = Convert.ToInt32(TextOrderID.Text);
                OOrderDetailsMap.UnitPrice = Convert.ToDouble(TextUnitPrice.Text);
                OOrderDetailsMap.Quantity = Convert.ToInt32(TextQuantity.Text);
                OOrderDetailsMap.Discount = Convert.ToDouble(TextDiscount.Text);



            }
            catch (Exception ex) { }
        }

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty;

            if (string.IsNullOrEmpty(TextOrderID.Text)) { msgList.Add(new ErrorMessage(TextOrderID.ClientID, "OrderID harus diisi!")); }
            if (string.IsNullOrEmpty(TextProductName.Text)) { msgList.Add(new ErrorMessage(TextProductName.ClientID, "ProductName harus diisi!")); }
            if (string.IsNullOrEmpty(TextUnitPrice.Text)) { msgList.Add(new ErrorMessage(TextUnitPrice.ClientID, "UnitPrice harus diisi!")); }
            if (string.IsNullOrEmpty(TextQuantity.Text)) { msgList.Add(new ErrorMessage(TextQuantity.ClientID, "Quantity harus diisi!")); }
            if (string.IsNullOrEmpty(TextDiscount.Text)) { msgList.Add(new ErrorMessage(TextDiscount.ClientID, "Discount harus diisi!")); }



            if (msgList.Count == 0)
            {
                return true;
            }
            else
            {
                Message.ShowErrorMessage(this.Page, msgList);
                return false;
            }
        }

        protected void ButtonFindProduct_Click(object sender, EventArgs e)
        {

            Message.RunScript(this.Parent.Page, "$('#orderdetailproduct').modal()");
        }

        protected void OrderDetailProduct1_Selected_Changed(object sender, EventArgs e)
        {
            SelectedProducts = OrderDetailProduct1.SelectedProducts;

            OOrderDetailsMap.ProductID = SelectedProducts.ProductID;
            OOrderDetailsMap.ProductName = SelectedProducts.ProductName;
            TextProductName.Text = SelectedProducts.ProductName;
            
        }
    }
}