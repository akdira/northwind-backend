﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="ProductsList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.ProductsList" %>
<%@ Register Src="~/Pages/Master/ProductsDetail.ascx" TagName="ProductDetail" TagPrefix="dtl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Products List', 3);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Products List</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>                            
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridProductsList" runat="server" AllowPaging="true" DefaultSortExpression="ProductID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("ProductID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("ProductID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("ProductID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
									<asp:BoundField HeaderText="Product ID" DataField="ProductID" SortExpression="ProductID">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Product Name" DataField="ProductName" SortExpression="ProductName">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Supplier" DataField="Supplier" SortExpression="Supplier">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Category ID" DataField="Category" SortExpression="Category">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Quantity Per Unit" DataField="QuantityPerUnit" SortExpression="QuantityPerUnit">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Unit Price" DataField="UnitPrice" SortExpression="UnitPrice">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Units In Stock" DataField="UnitsInStock" SortExpression="UnitsInStock">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Units On Stock" DataField="UnitsOnOrder" SortExpression="UnitsOnOrder">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Reorder Level" DataField="ReorderLevel" SortExpression="ReorderLevel">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Discontinued" DataField="Discontinued" SortExpression="Discontinued">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div id="productdetail" class="modal fade" role="dialog">
        <dtl:ProductDetail ID="ProductDetail1" runat="server" />
    </div>
    
</asp:Content>
