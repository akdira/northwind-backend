﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="CustomersList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.CustomersList" %>
<%@ Register Src="~/Pages/Master/CustomersDetail.ascx" TagName="CustomerDetail" TagPrefix="dtl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Customer List', 3);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">ActivityLog List</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>                            
                            <button id="buttonTest" class="btn btn-primary btn-sm">Click Me</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridCustomersList" runat="server" AllowPaging="true" DefaultSortExpression="CustomerID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("CustomerID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("CustomerID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px; " CommandArgument='<%# Eval("CustomerID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
									<asp:BoundField HeaderText="Customer ID" DataField="CustomerID" SortExpression="CustomerID">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Company Name" DataField="CompanyName" SortExpression="CompanyName">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Contact Name" DataField="ContactName" SortExpression="ContactName">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Title" DataField="ContactTitle" SortExpression="ContactTitle">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="City" DataField="City" SortExpression="City">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Country" DataField="Country" SortExpression="Country">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
									<asp:BoundField HeaderText="Phone" DataField="Phone" SortExpression="Phone">
										<HeaderStyle Width="20.0%" />
										<ItemStyle Width="20.0%" />
									</asp:BoundField>
                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div id="customerdetail" class="modal fade" role="dialog">
        <dtl:CustomerDetail ID="CustomerDetail1" runat="server" />
    </div>
</asp:Content>
