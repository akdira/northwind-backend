﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomersDetail.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.CustomerDetail" %>

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
		<div class="modal-dialog"> 
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Customer Detail</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 form-horizontal">
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">CustomerID</label>
								<div class="col-md-4">
									<asp:TextBox ID="TextCustomerID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">CompanyName </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextCompanyName" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ContactName </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextContactName" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">ContactTitle </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextContactTitle" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">Address </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextAddress" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">City </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextCity" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">Region </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextRegion" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">Postal Code </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextPostalCode" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">Country </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextCountry" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">Phone </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextPhone" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" style="text-align: left">Fax </label>
								<div class="col-md-8">
									<asp:TextBox ID="TextFax" runat="server" CssClass="form-control" placeholder="Harus diisi" ></asp:TextBox>                                            
								</div>
							</div>
							 
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
					<asp:LinkButton ID="ButtonClose" runat="server" CssClass="btn btn-danger" OnClientClick="$('#customerdetail').modal('hide')"><i class="fa fa-remove">&nbsp;</i>Tutup</asp:LinkButton>                            
				</div>
			</div>
		</div>
    </ContentTemplate>
</asp:UpdatePanel>