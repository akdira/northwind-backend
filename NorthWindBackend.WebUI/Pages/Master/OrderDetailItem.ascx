﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderDetailItem.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.OrderDetailItem" %>

<%@ Register Src="~/Pages/Master/OrderDetailsProduct.ascx" TagName="OrderDetailsProduct" TagPrefix="dtl" %>

<asp:UpdatePanel ID="UpdatePanelOrderDetailItem" runat="server">
    <ContentTemplate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Order Detail Item</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">OrderID </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextOrderID" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Harus diisi"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">ProductName </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <%--<asp:TextBox ID="TextCustomerID" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>--%>
                                        <asp:TextBox ID="TextProductName" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Harus diisi"></asp:TextBox>
                                        <span class="input-group-btn">
                                            <asp:LinkButton ID="ButtonFindProduct" CssClass="btn btn-secondary btn-success" runat="server" OnClick="ButtonFindProduct_Click"><span class="fa fa-search"></span> </asp:LinkButton>
                                        </span>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">UnitPrice </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextUnitPrice" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Quantity </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextQuantity" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Discount </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextDiscount" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
                    <asp:LinkButton ID="ButtonClose" runat="server" CssClass="btn btn-danger" OnClientClick="$('#orderdetailitem').modal('hide')"><i class="fa fa-remove">&nbsp;</i>Tutup</asp:LinkButton>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>


    <div id="orderdetailproduct" class="modal fade" role="dialog">
        <dtl:OrderDetailsProduct ID="OrderDetailProduct1" runat="server" OnSelected_Changed="OrderDetailProduct1_Selected_Changed" />
    </div>
