﻿using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using NorthWindBackend.Facade;
using NorthWindBackend.WebUI.Pages.MasterPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class OrderDetailsProduct : System.Web.UI.UserControl
    {
        public delegate void SelectedChanged(object sender, EventArgs e);
        public event SelectedChanged Selected_Changed;

        public Products SelectedProducts
        {
            get
            {
                if (ViewState["SelectedProducts"] == null)
                {
                    ViewState["SelectedProducts"] = new Products();
                }
                return (Products)ViewState["SelectedProducts"];
            }
            set
            {
                ViewState["SelectedProducts"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridOrderDetailsProduct.PageFunction = PageFunction;


            if (!Page.IsPostBack)
            {
                GridOrderDetailsProduct.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridOrderDetailsProduct;
            var facade = new ProductFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonChooseProduct_Click(object sender, EventArgs e)
        {

            var facade = new ProductFacade();
            var ProductID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            SelectedProducts = facade.GetObject(ProductID);
            Selected_Changed(sender, e);
            Message.RunScript(this.Parent.Page, "$('#orderdetailproduct').modal('hide')");

        }
    }
}