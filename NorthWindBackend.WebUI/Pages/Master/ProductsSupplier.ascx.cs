﻿using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using NorthWindBackend.Facade;
using NorthWindBackend.WebUI.Pages.MasterPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class ProductsSupplier : System.Web.UI.UserControl
    {
        public delegate void SelectedChanged(object sender, EventArgs e);
        public event SelectedChanged Selected_Changed;

        public Suppliers SelectedSuppliers
        {
            get
            {
                if (ViewState["SelectedSuppliers"] == null)
                {
                    ViewState["SelectedSuppliers"] = new Suppliers();
                }
                return (Suppliers)ViewState["SelectedSuppliers"];
            }
            set
            {
                ViewState["SelectedSuppliers"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridProductsSupplier.PageFunction = PageFunction;


            if (!Page.IsPostBack)
            {
                GridProductsSupplier.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridProductsSupplier;
            var facade = new SupplierFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonChooseSupplier_Click(object sender, EventArgs e)
        {

            var facade = new SupplierFacade();
            var SupplierID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            SelectedSuppliers = facade.GetObject(SupplierID); 
            Selected_Changed(sender, e); 
            Message.RunScript(this.Parent.Page, "$('#productsupplier').modal('hide')");

        }
    }
}