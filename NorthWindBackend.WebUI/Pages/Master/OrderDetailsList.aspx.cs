﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.WebUI.Pages.Master;
using NorthWindBackend.WebUI.Pages.MasterPages;
using PetaPoco;
using NorthWindBackend.Entity;
using NorthWindBackend.WebUI.WebControl;
using NorthWind.Entity;
using System.Diagnostics;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class OrderDetailsList : System.Web.UI.Page
    {


        public PageState PageState
        {
            get
            {
                if (ViewState["PAGESTATE"] == null)
                {
                    if (Request.QueryString["PageState"] == "New")
                    {
                        ViewState["PAGESTATE"] = PageState.New;
                    }
                    else if (Request.QueryString["PageState"] == "Edit")
                    {
                        ViewState["PAGESTATE"] = PageState.Edit;
                    }
                    else if (Request.QueryString["PageState"] == "View")
                    {
                        ViewState["PAGESTATE"] = PageState.View;
                    }
                    return (PageState)ViewState["PAGESTATE"];
                }
                return (PageState)ViewState["PAGESTATE"];
            }
            set
            {
                ViewState["PAGESTATE"] = value;
            }
        }

        public Customers SelectedCustomers
        {
            get
            {
                if (ViewState["SelectedCustomers"] == null)
                {
                    ViewState["SelectedCustomers"] = new Customers();
                }
                return (Customers)ViewState["SelectedCustomers"];
            }
            set
            {
                ViewState["SelectedCustomers"] = value;
            }
        }

        public Employees SelectedEmployees
        {
            get
            {
                if (ViewState["SelectedEmployees"] == null)
                {
                    ViewState["SelectedEmployees"] = new Employees();
                }
                return (Employees)ViewState["SelectedEmployees"];
            }
            set
            {
                ViewState["SelectedEmployees"] = value;
            }
        }

        public List<OrderDetailsMap> ListOOrderDetails
        {
            get
            {
                if (ViewState["LISTOORDERDETAILS"] == null)
                {
                    return null;
                }
                return ViewState["LISTOORDERDETAILS"] as List<OrderDetailsMap>;
            }
            set

            {
                ViewState["LISTOORDERDETAILS"] = value;
            }
        }

        public Orders OOrder
        {
            get
            {
                if (ViewState["OORDER"] == null)
                {
                    return null;
                }
                return ViewState["OORDER"] as Orders;
            }
            set
            {
                ViewState["OORDER"] = value;
            }
        }

        public int DataID
        {
            get
            {
                if (ViewState["DATA_ID"] == null)
                {
                    ViewState["DATA_ID"] = Request.QueryString["OrderID"];
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        public WebGridView GridViewList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Master");
            GridOrderDetailsList.DataFunction = DataFunction;
            GridOrderDetailsList.DataTotalCountFunction = CountFunction;


            if (!Page.IsPostBack)
            {
                ProcessDetail();
                GridOrderDetailsList.DisplayData();


                var facade = new ShipperFacade();
                ComboShipVia.DataSource = facade.PageDataAll();

                ComboShipVia.DataTextField = "CompanyName";
                ComboShipVia.DataValueField = "ShipperID";
                ComboShipVia.DataBind();


            }
        }

        private List<OrderDetailsMap> DataFunction()
        {
            var grid = GridOrderDetailsList;
            var facade = new OrderDetailFacade();

            //int OrderID = Convert.ToInt32(Request.QueryString["OrderID"]);
            //string PageState = Request.QueryString["PageState"];
            if (!Page.IsPostBack)
            {
                ListOOrderDetails = facade.PageRuntime(DataID, grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey).ToList();
            }

            return ListOOrderDetails; // Tambahkan skip dan take
        }

        private int CountFunction()
        {
            var grid = GridOrderDetailsList;
            var facade = new OrderDetailFacade();

            //int OrderID = Convert.ToInt32(Request.QueryString["OrderID"]);
            //string PageState = Request.QueryString["PageState"];

            return ListOOrderDetails.Count;
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridOrderDetailsList;
            var facade = new OrderDetailFacade();

            //int OrderID = Convert.ToInt32(Request.QueryString["OrderID"]);
            //string PageState = Request.QueryString["PageState"];

            return facade.PageOrderDataByCriteria(DataID, grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessSave();
            }
        }

        public void ProcessDetail()
        {
            switch (PageState)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OOrder = new Orders();
        }

        private void PopulateControlsInNewState()
        {
            TextOrderID.Text = "";
            TextCustomer.Text = "";
            TextEmployee.Text = "";
            TextOrderDate.Text = "";
            TextRequiredDate.Text = "";
            TextShippedDate.Text = "";
            TextFreight.Text = "";
            TextShipName.Text = "";
            TextShipAddress.Text = "";
            TextShipCity.Text = "";
            TextShipRegion.Text = "";
            TextShipPostalCode.Text = "";
            TextShipCountry.Text = "";

            ComboShipVia.SelectedValue = "";

        }

        private void AdjustControlInNewState()
        {
            TextOrderDate.ReadOnly = false;
            TextRequiredDate.ReadOnly = false;
            TextShippedDate.ReadOnly = false;
            TextFreight.ReadOnly = false;
            TextShipName.ReadOnly = false;
            TextShipAddress.ReadOnly = false;
            TextShipCity.ReadOnly = false;
            TextShipRegion.ReadOnly = false;
            TextShipPostalCode.ReadOnly = false;
            TextShipCountry.ReadOnly = false;

            ComboShipVia.Enabled = true;

            ButtonSave.Visible = true;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
            var facade = new OrderFacade();
            OOrder = facade.GetObject(DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OOrder != null)
            {


                //Get Customer
                var CustomerFcd = new CustomerFacade();
                SelectedCustomers = CustomerFcd.GetObject("CustomerID", OOrder.CustomerID);

                //Get Employee
                var EmployeeFcd = new EmployeeFacade();
                SelectedEmployees = EmployeeFcd.GetObject(OOrder.EmployeeID);

                // Pull Text
                TextOrderID.Text = Convert.ToString(OOrder.OrderID);
                TextOrderDate.Text = Convert.ToString(OOrder.OrderDate);
                TextRequiredDate.Text = Convert.ToString(OOrder.RequiredDate);
                TextShippedDate.Text = Convert.ToString(OOrder.ShippedDate);
                TextFreight.Text = Convert.ToString(OOrder.Freight);
                TextShipName.Text = OOrder.ShipName;
                TextShipAddress.Text = OOrder.ShipAddress;
                TextShipCity.Text = OOrder.ShipCity;
                TextShipRegion.Text = OOrder.ShipRegion;
                TextShipPostalCode.Text = OOrder.ShipPostalCode;
                TextShipCountry.Text = OOrder.ShipCountry;

                ComboShipVia.SelectedValue = Convert.ToString(OOrder.ShipVia);



                //If Customers found
                if (SelectedCustomers != null)
                {
                    TextCustomer.Text = SelectedCustomers.ContactName;
                }
                // If supplier not found
                else
                {
                    TextCustomer.Text = "";
                }

                //If Employees found
                if (SelectedEmployees != null)
                {
                    TextEmployee.Text = SelectedEmployees.FirstName + " " + SelectedEmployees.LastName;
                }
                // If supplier not found
                else
                {
                    TextEmployee.Text = "";
                }





                // If supplier found
                //if (SelectedSuppliers != null)
                //{
                //    TextSupplier.Text = SelectedSuppliers.CompanyName;
                //}
                //// If supplier not found
                //else
                //{
                //    TextSupplier.Text = "";
                //}
            }
        }

        private void AdjustControlInEditState()
        {
            AdjustControlInNewState();

            ButtonSave.Visible = true;
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new OrderFacade();
            OOrder = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OOrder != null)
            {

                // Get Customer
                var CustomerFcd = new CustomerFacade();
                SelectedCustomers = CustomerFcd.GetObject("CustomerID", OOrder.CustomerID);

                // Get Employee
                var EmployeeFcd = new EmployeeFacade();
                SelectedEmployees = EmployeeFcd.GetObject(OOrder.EmployeeID);

                // Pull Text
                TextOrderID.Text = Convert.ToString(OOrder.OrderID);
                TextCustomer.Text = OOrder.CustomerID;
                TextEmployee.Text = Convert.ToString(OOrder.EmployeeID);
                TextOrderDate.Text = Convert.ToString(OOrder.OrderDate);
                TextRequiredDate.Text = Convert.ToString(OOrder.RequiredDate);
                TextShippedDate.Text = Convert.ToString(OOrder.ShippedDate);
                TextFreight.Text = Convert.ToString(OOrder.Freight);
                TextShipName.Text = OOrder.ShipName;
                TextShipAddress.Text = OOrder.ShipAddress;
                TextShipCity.Text = OOrder.ShipCity;
                TextShipRegion.Text = OOrder.ShipRegion;
                TextShipPostalCode.Text = OOrder.ShipPostalCode;
                TextShipCountry.Text = OOrder.ShipCountry;


                ComboShipVia.SelectedValue = Convert.ToString(OOrder.ShipVia);


                // If Customers found
                if (SelectedCustomers != null)
                {
                    TextCustomer.Text = SelectedCustomers.ContactName;
                }
                // If Customers not found
                else
                {
                    TextCustomer.Text = "";
                }

                // If Employees found
                if (SelectedEmployees != null)
                {
                    TextEmployee.Text = SelectedEmployees.FirstName + " " + SelectedEmployees.LastName;
                }
                // If supplier not found
                else
                {
                    TextEmployee.Text = "";
                }




                //ComboDiscontinued.SelectedValue = Convert.ToString(OOrder.Discontinued);
            }
        }

        private void AdjustControlInViewState()
        {
            TextOrderDate.ReadOnly = true;
            TextRequiredDate.ReadOnly = true;
            TextShippedDate.ReadOnly = true;
            TextFreight.ReadOnly = true;
            TextShipName.ReadOnly = true;
            TextShipAddress.ReadOnly = true;
            TextShipCity.ReadOnly = true;
            TextShipRegion.ReadOnly = true;
            TextShipPostalCode.ReadOnly = true;
            TextShipCountry.ReadOnly = true;

            ComboShipVia.Enabled = false;


            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        {
            // Declare default
            string errorMsg = string.Empty;

            try
            {

                // Import Facade
                var facade = new OrderFacade();
                var facadelistitem = new OrderDetailFacade();

                // Declare default
                ActionResult result = null;

                // Prepare
                PopulateObject();

                // If insert
                if (PageState == PageState.New)
                {
                    // Insert Orders 
                    result = facade.DataInsert(OOrder);
                }

                // If edit
                else
                {
                    // Update Orders
                    result = facade.DataUpdate(OOrder);

                    // Clean existing Order Details
                    facadelistitem.Clean(OOrder.OrderID);

                    //var NewListOOrderDetails

                    //List<OrderDetails> InsertListOOrderDetails = ListOOrderDetails.Cast<OrderDetails>().ToList();

                    List<OrderDetails> InsertListOOrderDetails = ListOOrderDetails.ConvertAll(x => new OrderDetails { OrderID = x.OrderID,
                                                                                                                      ProductID = x.ProductID,
                                                                                                                      Discount = x.Discount,
                                                                                                                      Quantity = x.Quantity,
                                                                                                                      UnitPrice = x.UnitPrice
                                                                                                                     });
                    facadelistitem.InsertMass(InsertListOOrderDetails);

                }

                

                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage, "$('#productdetail').modal('hide')");
                    GridOrderDetailsList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
                OOrder.OrderID = Convert.ToInt32(TextOrderID.Text);
                OOrder.OrderDate = Convert.ToDateTime(TextOrderDate.Text);
                OOrder.RequiredDate = Convert.ToDateTime(TextRequiredDate.Text);
                OOrder.ShippedDate = Convert.ToDateTime(TextShippedDate.Text);
                OOrder.ShipVia = Convert.ToInt32(ComboShipVia.SelectedValue);
                OOrder.Freight = Convert.ToDouble(TextFreight.Text);
                OOrder.ShipName = TextShipName.Text;
                OOrder.ShipAddress = TextShipAddress.Text;
                OOrder.ShipCity = TextShipCity.Text;
                OOrder.ShipRegion = TextShipRegion.Text;
                OOrder.ShipPostalCode = TextShipPostalCode.Text;
                OOrder.ShipCountry = TextShipCountry.Text;

            }
            catch (Exception ex) { }
        }

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty;

            if (string.IsNullOrEmpty(TextOrderID.Text)) { msgList.Add(new ErrorMessage(TextOrderID.ClientID, "OrderID harus diisi!")); }
            if (string.IsNullOrEmpty(TextCustomer.Text)) { msgList.Add(new ErrorMessage(TextCustomer.ClientID, "CustomerID harus diisi!")); }
            if (string.IsNullOrEmpty(TextEmployee.Text)) { msgList.Add(new ErrorMessage(TextEmployee.ClientID, "EmployeeID harus diisi!")); }
            if (string.IsNullOrEmpty(TextOrderDate.Text)) { msgList.Add(new ErrorMessage(TextOrderDate.ClientID, "OrderDate harus diisi!")); }
            if (string.IsNullOrEmpty(TextRequiredDate.Text)) { msgList.Add(new ErrorMessage(TextRequiredDate.ClientID, "RequiredDate harus diisi!")); }
            if (string.IsNullOrEmpty(TextShippedDate.Text)) { msgList.Add(new ErrorMessage(TextShippedDate.ClientID, "ShippedDate harus diisi!")); }
            if (string.IsNullOrEmpty(ComboShipVia.SelectedValue)) { msgList.Add(new ErrorMessage(ComboShipVia.ClientID, "ShipVia harus diisi!")); }
            if (string.IsNullOrEmpty(TextFreight.Text)) { msgList.Add(new ErrorMessage(TextFreight.ClientID, "Freight harus diisi!")); }
            if (string.IsNullOrEmpty(TextShipName.Text)) { msgList.Add(new ErrorMessage(TextShipName.ClientID, "ShipName harus diisi!")); }
            if (string.IsNullOrEmpty(TextShipAddress.Text)) { msgList.Add(new ErrorMessage(TextShipAddress.ClientID, "ShipAddress harus diisi!")); }
            if (string.IsNullOrEmpty(TextShipCity.Text)) { msgList.Add(new ErrorMessage(TextShipCity.ClientID, "ShipCity harus diisi!")); }
            if (string.IsNullOrEmpty(TextShipRegion.Text)) { msgList.Add(new ErrorMessage(TextShipRegion.ClientID, "ShipRegion harus diisi!")); }
            if (string.IsNullOrEmpty(TextShipPostalCode.Text)) { msgList.Add(new ErrorMessage(TextShipPostalCode.ClientID, "ShipPostalCode harus diisi!")); }
            if (string.IsNullOrEmpty(TextShipCountry.Text)) { msgList.Add(new ErrorMessage(TextShipCountry.ClientID, "ShipCountry harus diisi!")); }



            if (msgList.Count == 0)
            {
                return true;
            }
            else
            {
                Message.ShowErrorMessage(this.Page, msgList);
                return false;
            }
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            //OrderDetailDetail1.DataID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            //OrderDetailDetail1.PageState = PageState.View;
            //OrderDetailDetail1.ProcessDetail();
            //Message.RunScript(this, "$('#orderdetaildetail').modal()");
            var ParamDataID = Convert.ToInt32((sender as LinkButton).CommandArgument);

            OrderDetailItem1.PageState = PageState.View;
            OrderDetailItem1.OOrderDetailsMap = ListOOrderDetails.FirstOrDefault<OrderDetailsMap>(item => item.ProductID == ParamDataID);
            OrderDetailItem1.ProcessDetail();
            Message.RunScript(this, "$('#orderdetailitem').modal()");
            //OrderDetailDetail1.PageState = PageState.View;
            //OrderDetailDetail1.ProcessDetail();
            //Message.RunScript(this, "$('#orderdetaildetail').modal()");

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            //OrderDetailDetail1.DataID = Convert.ToInt32((sender as LinkButton).CommandArgument) ;
            //OrderDetailDetail1.PageState = PageState.Edit;
            //OrderDetailDetail1.ProcessDetail();
            //Message.RunScript(this, "$('#orderdetaildetail').modal()");


            var ParamDataID = Convert.ToInt32((sender as LinkButton).CommandArgument);

            OrderDetailItem1.PageState = PageState.Edit;
            OrderDetailItem1.OOrderDetailsMap = ListOOrderDetails.FirstOrDefault<OrderDetailsMap>(item => item.ProductID == ParamDataID);
            OrderDetailItem1.ProcessDetail();
            Message.RunScript(this, "$('#orderdetailitem').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                //ProcessDelete(dataID);

                var ObjectRemove = ListOOrderDetails.FirstOrDefault<OrderDetailsMap>(item => item.ProductID == dataID);
                ListOOrderDetails.Remove(ObjectRemove);

                GridOrderDetailsList.DisplayData();
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            //OrderDetailDetail1.DataID = 0;
            //OrderDetailDetail1.PageState = PageState.New;
            //OrderDetailDetail1.ProcessDetail();
            //Message.RunScript(this, "$('#orderdetaildetail').modal()");

            OrderDetailItem1.OrderID = OOrder.OrderID;
            OrderDetailItem1.PageState = PageState.New;
            OrderDetailItem1.OOrderDetailsMap = new OrderDetailsMap();
            OrderDetailItem1.ProcessDetail();
            Message.RunScript(this, "$('#orderdetailitem').modal()");

        }

        public void ProcessDelete(int dataID)
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = facade.DataDelete(dataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridOrderDetailsList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        }

        protected void ComboShipVia_SelectedIndexChanged(object sender, EventArgs e)
        {
            OOrder.ShipVia = Convert.ToInt32(ComboShipVia.SelectedValue);
        }

        protected void Customer_OnSelectedChanged(object sender, EventArgs e)
        {
            TextCustomer.Text = OrdersCustomer1.SelectedCustomers.ContactName;
            OOrder.CustomerID = OrdersCustomer1.SelectedCustomers.CustomerID;
        }

        protected void Employee_OnSelectedChanged(object sender, EventArgs e)
        {
            TextEmployee.Text = OrdersEmployee1.SelectedEmployees.FirstName + " " + OrdersEmployee1.SelectedEmployees.LastName;
            OOrder.EmployeeID = OrdersEmployee1.SelectedEmployees.EmployeeID;
        }

        protected void ButtonFindCustomer_Click(object sender, EventArgs e)
        {
            Message.RunScript(this, "$('#orderscustomer').modal()");
        }

        protected void ButtonFindEmployee_Click(object sender, EventArgs e)
        {
            Message.RunScript(this, "$('#ordersemployee').modal()");
        }

        protected void Item_FormSubmit_Add(object sender, EventArgs e)
        {
            var NewOOrderDetailsMap = OrderDetailItem1.OOrderDetailsMap;
            ListOOrderDetails.Add(NewOOrderDetailsMap);

            GridOrderDetailsList.DisplayData();
        }

        protected void Item_FormSubmit_Edit(object sender, EventArgs e)
        {

            var ParamDataID = OrderDetailItem1.OOrderDetailsMap.ProductID;

            var i = ListOOrderDetails.FindIndex(item => item.ProductID == ParamDataID);

            var NewOOrderDetailsMap = OrderDetailItem1.OOrderDetailsMap;

            //ListOOrderDetails.Where<OrderDetailsMap>(item => item.ProductID == ParamDataID).ToList().ForEach(o => o = NewOOrderDetailsMap);

            ListOOrderDetails.Where<OrderDetailsMap>(item => item.ProductID == ParamDataID).ToList().ForEach(o => o.ProductID = NewOOrderDetailsMap.ProductID);
            ListOOrderDetails.Where<OrderDetailsMap>(item => item.ProductID == ParamDataID).ToList().ForEach(o => o.ProductName = NewOOrderDetailsMap.ProductName);
            ListOOrderDetails.Where<OrderDetailsMap>(item => item.ProductID == ParamDataID).ToList().ForEach(o => o.UnitPrice = NewOOrderDetailsMap.UnitPrice);
            ListOOrderDetails.Where<OrderDetailsMap>(item => item.ProductID == ParamDataID).ToList().ForEach(o => o.Quantity = NewOOrderDetailsMap.Quantity);
            ListOOrderDetails.Where<OrderDetailsMap>(item => item.ProductID == ParamDataID).ToList().ForEach(o => o.Discount = NewOOrderDetailsMap.Discount);

            Debug.WriteLine("ListOOrderDetails: ");
            Debug.WriteLine(ListOOrderDetails);

            //ListOOrderDetails.

            //ListOOrderDetails.FirstOrDefault<OrderDetailsMap>(item => item.ProductID == ParamDataID).UnitPrice = OrderDetailItem1.OOrderDetailsMap.UnitPrice;
            //ListOOrderDetails.FirstOrDefault<OrderDetailsMap>(item => item.ProductID == ParamDataID).Quantity = OrderDetailItem1.OOrderDetailsMap.Quantity;
            //ListOOrderDetails.FirstOrDefault<OrderDetailsMap>(item => item.ProductID == ParamDataID).Discount = OrderDetailItem1.OOrderDetailsMap.Discount;

            GridOrderDetailsList.DataFunction = DataFunction;
            GridOrderDetailsList.DataTotalCountFunction = CountFunction;
            GridOrderDetailsList.DisplayData();
        }
        
    }
}