﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.WebUI.Pages.Master;
using NorthWindBackend.WebUI.Pages.MasterPages;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class ProductsList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Master");
            GridProductsList.PageFunction = PageFunction;


            if (!Page.IsPostBack)
            {
                GridProductsList.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridProductsList;
            var facade = new ProductFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            ProductDetail1.DataID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            ProductDetail1.PageState = PageState.View;
            ProductDetail1.ProcessDetail();
            Message.RunScript(this, "$('#productdetail').modal()");

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            ProductDetail1.DataID = Convert.ToInt32((sender as LinkButton).CommandArgument) ;
            ProductDetail1.PageState = PageState.Edit;
            ProductDetail1.ProcessDetail();
            Message.RunScript(this, "$('#productdetail').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete(dataID);
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ProductDetail1.DataID = 0;
            ProductDetail1.PageState = PageState.New;
            ProductDetail1.ProcessDetail();
            Message.RunScript(this, "$('#productdetail').modal()");

        }

        public void ProcessDelete(int dataID)
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = facade.DataDelete(dataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridProductsList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        }
        
    }
}