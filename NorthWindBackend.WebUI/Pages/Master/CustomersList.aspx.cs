﻿using NorthWindBackend.WebUI.Pages.MasterPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class CustomersList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Master");
            GridCustomersList.PageFunction = PageFunction;


            if (!Page.IsPostBack)
            {
                GridCustomersList.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridCustomersList;
            var facade = new CustomerFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            CustomerDetail1.DataID = (sender as LinkButton).CommandArgument;
            CustomerDetail1.PageState = PageState.View;
            CustomerDetail1.ProcessDetail();
            Message.RunScript(this, "$('#customerdetail').modal()");

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            CustomerDetail1.DataID = (sender as LinkButton).CommandArgument;
            CustomerDetail1.PageState = PageState.Edit;
            CustomerDetail1.ProcessDetail();
            Message.RunScript(this, "$('#customerdetail').modal()");
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete(dataID);
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            CustomerDetail1.DataID = String.Empty;
            CustomerDetail1.PageState = PageState.New;
            CustomerDetail1.ProcessDetail();
            Message.RunScript(this, "$('#customerdetail').modal()");

        }

        public void ProcessDelete(int dataID)
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = facade.DataDelete(dataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridCustomersList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        }
    }

}
