﻿using NorthWind.Entity;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using NorthWindBackend.Facade;
using NorthWindBackend.WebUI.Pages.MasterPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class OrdersEmployee : System.Web.UI.UserControl
    {
        public delegate void SelectedChanged(object sender, EventArgs e);
        public event SelectedChanged Selected_Changed;

        public Employees SelectedEmployees
        {
            get
            {
                if (ViewState["SelectedEmployees"] == null)
                {
                    ViewState["SelectedEmployees"] = new Employees();
                }
                return (Employees)ViewState["SelectedEmployees"];
            }
            set
            {
                ViewState["SelectedEmployees"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridOrdersEmployee.PageFunction = PageFunction;


            if (!Page.IsPostBack)
            {
                GridOrdersEmployee.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridOrdersEmployee;
            var facade = new EmployeeFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonChooseEmployee_Click(object sender, EventArgs e)
        {

            var facade = new EmployeeFacade();
            var EmployeeID = (sender as LinkButton).CommandArgument;
            SelectedEmployees = facade.GetObject("EmployeeID", EmployeeID); 
            Selected_Changed(sender, e); 
            Message.RunScript(this.Parent.Page, "$('#ordersemployee').modal('hide')");

        }
    }
}