﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="OrdersList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.OrdersList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Orders List', 3);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Orders List</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="ButtonAdd" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridOrdersList" runat="server" AllowPaging="true" DefaultSortExpression="OrderID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("OrderID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("OrderID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("OrderID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="Order ID" DataField="OrderID" SortExpression="OrderID">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Customer Name" DataField="CustomerName" SortExpression="CustomerName">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Employee Name" DataField="EmployeeName" SortExpression="EmployeeName">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Order Date" DataField="OrderDate" SortExpression="OrderDate">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Required Date" DataField="RequiredDate" SortExpression="RequiredDate">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Shipped Date" DataField="ShippedDate" SortExpression="ShippedDate">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship Via" DataField="ShipVia" SortExpression="ShipVia">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Freight" DataField="Freight" SortExpression="Freight">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship Name" DataField="ShipName" SortExpression="ShipName">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship Address" DataField="ShipAddress" SortExpression="ShipAddress">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship City" DataField="ShipCity" SortExpression="ShipCity">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship Region" DataField="ShipRegion" SortExpression="ShipRegion">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship Postal Code" DataField="ShipPostalCode" SortExpression="ShipPostalCode">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ship Country" DataField="ShipCountry" SortExpression="ShipCountry">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>

                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
