﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsSupplier.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.ProductsSupplier" %>

<asp:UpdatePanel ID="UpdateSupllier" runat="server">
    <ContentTemplate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Product Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Supplier List</h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <grv:WebGridView ID="GridProductsSupplier" runat="server" AllowPaging="true" DefaultSortExpression="SupplierID"
                                            DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ButtonChooseSupplier" runat="server" Style="margin-right: 5px;"  OnClick="ButtonChooseSupplier_Click" CommandArgument='<%# Eval("SupplierID") %>'><i class="fa fa-check" ></i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="Supplier ID" DataField="SupplierID" SortExpression="SupplierID">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Company Name" DataField="CompanyName" SortExpression="CompanyName">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Contact Name" DataField="ContactName" SortExpression="ContactName">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Contact Title" DataField="ContactTitle" SortExpression="ContactTitle">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Address" DataField="Address" SortExpression="Address">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="City" DataField="City" SortExpression="City">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Region" DataField="Region" SortExpression="Region">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Postal Code" DataField="PostalCode" SortExpression="PostalCode">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Country" DataField="Country" SortExpression="Country">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Phone" DataField="Phone" SortExpression="Phone">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Fax" DataField="Fax" SortExpression="Fax">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Home Page" DataField="HomePage" SortExpression="HomePage">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                            </Columns>
                                        </grv:WebGridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
