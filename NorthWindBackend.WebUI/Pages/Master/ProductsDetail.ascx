﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsDetail.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.ProductsDetail" %>
<%@ Register Src="~/Pages/Master/ProductsSupplier.ascx" TagName="ProductsSupplier" TagPrefix="dtl" %>

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Product Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Product ID</label>
                                <div class="col-md-4">
                                    <asp:TextBox ID="TextProductID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Product Name </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextProductName" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Supplier</label>
                                <div class="col-md-8">
                                    <%--<input type="hidden" name="HiddenSupplierID" id="HiddenSupplierID" runat="server" />--%>
                                    
                                    <div class="input-group">
                                      <%--<asp:TextBox ID="TextSupplierID" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>--%>
                                      <asp:TextBox ID="TextSupplier" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Harus diisi"></asp:TextBox>
                                      <span class="input-group-btn">
                                        <asp:LinkButton ID="ButtonFindSupplier" CssClass="btn btn-secondary btn-success" runat="server" OnClick="ButtonFindSupplier_Click"><span class="fa fa-search"></span> </asp:LinkButton>
                                      </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Category ID </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ComboCategory" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ComboCategory_SelectedIndexChanged">
                                        <asp:ListItem Value="" Text="-- Please Select --" />
                                    </asp:DropDownList>
                                    <asp:TextBox ID="TextCategoryID" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Quantity Per Unit </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextQuantityPerUnit" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Unit Price </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextUnitPrice" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Units In Stock </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextUnitsInStock" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Text Units On Order </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextUnitsOnOrder" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Reorder Level </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TextReorderLevel" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" style="text-align: left">Discontinued </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ComboDiscontinued" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ComboDiscontinued_SelectedIndexChanged">
                                        <asp:ListItem Value="False" Text="False" />
                                        <asp:ListItem Value="True" Text="True" />
                                    </asp:DropDownList>

                                    <asp:TextBox ID="TextDiscontinued" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox></div>
                            </div>


                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
                    <asp:LinkButton ID="ButtonClose" runat="server" CssClass="btn btn-danger" OnClientClick="$('#productdetail').modal('hide')"><i class="fa fa-remove">&nbsp;</i>Tutup</asp:LinkButton>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>


    <div id="productsupplier" class="modal fade" role="dialog">
        <dtl:ProductsSupplier ID="ProductsSupplier1" runat="server" OnSelected_Changed="Supplier_OnSelectedChanged" />
    </div>
