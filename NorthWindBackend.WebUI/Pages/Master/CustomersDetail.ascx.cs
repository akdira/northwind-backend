﻿using NorthWindBackend.Common;
using NorthWindBackend.WebUI.WebControl;
using NorthWindBackend.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWind.Entity;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class CustomerDetail : UserControl
    {
        public PageState PageState
        {
            get
            {
                if (ViewState["PAGESTATE"] == null)
                {
                    ViewState["PAGESTATE"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE"];
            }
            set
            {
                ViewState["PAGESTATE"] = value;
            }
        }

        public Customers OCustomer
        {
            get
            {
                if (ViewState["OCUSTOMER"] == null)
                {
                    return null;
                }
                return ViewState["OCUSTOMER"] as Customers;
            }
            set
            {
                ViewState["OCUSTOMER"] = value;
            }
        }

        public string DataID
        {
            get
            {
                if (ViewState["DATA_ID"] == null)
                {
                    return String.Empty;
                }
                return Convert.ToString(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        internal void ProcessDetail()
        {
            switch (PageState)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new CustomerFacade();
            OCustomer = facade.GetObject("CustomerID", DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OCustomer != null)
            {
                TextCustomerID.Text = OCustomer.CustomerID;
                TextCompanyName.Text = OCustomer.CompanyName;
                TextContactName.Text = OCustomer.ContactName;
                TextContactTitle.Text = OCustomer.ContactTitle;
                TextAddress.Text = OCustomer.Address;
                TextCity.Text = OCustomer.City;
                TextRegion.Text = OCustomer.Region;
                TextPostalCode.Text = OCustomer.PostalCode;
                TextCountry.Text = OCustomer.Country;
                TextPhone.Text = OCustomer.Phone;
                TextFax.Text = OCustomer.Fax;

            }
        }

        private void AdjustControlInViewState()
        {
            TextCustomerID.ReadOnly = true;
            TextCompanyName.ReadOnly = true;
            TextContactName.ReadOnly = true;
            TextContactTitle.ReadOnly = true;
            TextAddress.ReadOnly = true;
            TextCity.ReadOnly = true;
            TextRegion.ReadOnly = true;
            TextPostalCode.ReadOnly = true;
            TextCountry.ReadOnly = true;
            TextPhone.ReadOnly = true;
            TextFax.ReadOnly = true;

            ButtonSave.Visible = false;
        }



        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }
        private void DataReadInEditState()
        {
            var facade = new CustomerFacade();
            OCustomer = facade.GetObject("CustomerID",DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OCustomer != null)
            {
                TextCustomerID.Text = OCustomer.CustomerID;
                TextCompanyName.Text = OCustomer.CompanyName;
                TextContactName.Text = OCustomer.ContactName;
                TextContactTitle.Text = OCustomer.ContactTitle;
                TextAddress.Text = OCustomer.Address;
                TextCity.Text = OCustomer.City;
                TextRegion.Text = OCustomer.Region;
                TextPostalCode.Text = OCustomer.PostalCode;
                TextCountry.Text = OCustomer.Country;
                TextPhone.Text = OCustomer.Phone;
                TextFax.Text = OCustomer.Fax;

            }
        }

        private void AdjustControlInEditState()
        {
            TextCustomerID.ReadOnly = false;
            TextCompanyName.ReadOnly = false;
            TextContactName.ReadOnly = false;
            TextContactTitle.ReadOnly = false;
            TextAddress.ReadOnly = false;
            TextCity.ReadOnly = false;
            TextRegion.ReadOnly = false;
            TextPostalCode.ReadOnly = false;
            TextCountry.ReadOnly = false;
            TextPhone.ReadOnly = false;
            TextFax.ReadOnly = false;

            ButtonSave.Visible = true;
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OCustomer = new Customers();
        }

        private void PopulateControlsInNewState()
        {
            TextCustomerID.Text = "";
            TextCompanyName.Text = "";
            TextContactName.Text = "";
            TextContactTitle.Text = "";
            TextAddress.Text = "";
            TextCity.Text = "";
            TextRegion.Text = "";
            TextPostalCode.Text = "";
            TextCountry.Text = "";
            TextPhone.Text = "";
            TextFax.Text = "";

        }

        private void AdjustControlInNewState()
        {
            AdjustControlInEditState();
        }

        public WebGridView GridViewList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessOnSave();
            }
        }

        private void ProcessOnSave()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new CustomerFacade();
                ActionResult result = null;
                PopulateObject();
                if (PageState == PageState.New)
                {
                    result = facade.DataInsert(OCustomer);
                }
                else
                {
                    result = facade.DataUpdate(OCustomer);
                }
                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage, "$('#activitylogdetail').modal('hide')");
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
                OCustomer.CustomerID = TextCustomerID.Text;
                OCustomer.CompanyName = TextCompanyName.Text;
                OCustomer.ContactName = TextContactName.Text;
                OCustomer.ContactTitle = TextContactTitle.Text;
                OCustomer.Address = TextAddress.Text;
                OCustomer.City = TextCity.Text;
                OCustomer.Region = TextRegion.Text;
                OCustomer.PostalCode = TextPostalCode.Text;
                OCustomer.Country = TextCountry.Text;
                OCustomer.Phone = TextPhone.Text;
                OCustomer.Fax = TextFax.Text;

            }
            catch (Exception ex) { }
        }

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty;
            if (string.IsNullOrEmpty(TextCompanyName.Text))
            {
                msgList.Add(new ErrorMessage(TextCompanyName.ClientID, "CompanyName harus diisi!"));
            }
            if (string.IsNullOrEmpty(TextContactName.Text))
            {
                msgList.Add(new ErrorMessage(TextContactName.ClientID, "ContactName harus diisi!"));
            }
            if (string.IsNullOrEmpty(TextContactTitle.Text))
            {
                msgList.Add(new ErrorMessage(TextContactTitle.ClientID, "ContactTitle harus diisi!"));
            }
            if (string.IsNullOrEmpty(TextAddress.Text))
            {
                msgList.Add(new ErrorMessage(TextAddress.ClientID, "Address harus diisi!", true));
            }
            if (string.IsNullOrEmpty(TextCity.Text))
            {
                msgList.Add(new ErrorMessage(TextCity.ClientID, "City harus diisi!", true));
            }
            if (string.IsNullOrEmpty(TextRegion.Text))
            {
                msgList.Add(new ErrorMessage(TextRegion.ClientID, "Region harus diisi!", true));
            }
            if (string.IsNullOrEmpty(TextPostalCode.Text))
            {
                msgList.Add(new ErrorMessage(TextPostalCode.ClientID, "PostalCode harus diisi!", true));
            }
            if (string.IsNullOrEmpty(TextCountry.Text))
            {
                msgList.Add(new ErrorMessage(TextCountry.ClientID, "Country harus diisi!", true));
            }
            if (string.IsNullOrEmpty(TextPhone.Text))
            {
                msgList.Add(new ErrorMessage(TextPhone.ClientID, "Phone harus diisi!", true));
            }
            if (string.IsNullOrEmpty(TextFax.Text))
            {
                msgList.Add(new ErrorMessage(TextFax.ClientID, "Fax harus diisi!", true));
            }


            if (msgList.Count == 0)
            {
                return true;
            }
            else
            {
                Message.ShowErrorMessage(this.Page, msgList);
                return false;
            }
        }

    }
}