﻿using NorthWind.Entity;
using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using NorthWindBackend.Facade;
using NorthWindBackend.WebUI.Pages.MasterPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class OrdersCustomer : System.Web.UI.UserControl
    {
        public delegate void SelectedChanged(object sender, EventArgs e);
        public event SelectedChanged Selected_Changed;

        public Customers SelectedCustomers
        {
            get
            {
                if (ViewState["SelectedCustomers"] == null)
                {
                    ViewState["SelectedCustomers"] = new Customers();
                }
                return (Customers)ViewState["SelectedCustomers"];
            }
            set
            {
                ViewState["SelectedCustomers"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            GridOrdersCustomer.PageFunction = PageFunction;


            if (!Page.IsPostBack)
            {
                GridOrdersCustomer.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridOrdersCustomer;
            var facade = new CustomerFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonChooseCustomer_Click(object sender, EventArgs e)
        {

            var facade = new CustomerFacade();
            var CustomerID = (sender as LinkButton).CommandArgument;
            SelectedCustomers = facade.GetObject("CustomerID", CustomerID); 
            Selected_Changed(sender, e); 
            Message.RunScript(this.Parent.Page, "$('#orderscustomer').modal('hide')");

        }
    }
}