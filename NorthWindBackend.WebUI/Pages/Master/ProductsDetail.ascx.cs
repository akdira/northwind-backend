﻿using NorthWindBackend.Common;
using NorthWindBackend.Entity;
using NorthWindBackend.Facade;
using NorthWindBackend.WebUI.WebControl;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class ProductsDetail : System.Web.UI.UserControl
    {

        
        public PageState PageState
        {
            get
            {
                if (ViewState["PAGESTATE"] == null)
                {
                    ViewState["PAGESTATE"] = PageState.New;
                }
                return (PageState)ViewState["PAGESTATE"];
            }
            set
            {
                ViewState["PAGESTATE"] = value;
            }
        }

        public Suppliers SelectedSuppliers
        {
            get
            {
                if (ViewState["SelectedSuppliers"] == null)
                {
                    ViewState["SelectedSuppliers"] = new Suppliers();
                }
                return (Suppliers)ViewState["SelectedSuppliers"];
            }
            set
            {
                ViewState["SelectedSuppliers"] = value;
            }
        }

        public Products OProduct
        {
            get
            {
                if (ViewState["OACTIVITYLOG"] == null)
                {
                    return null;
                }
                return ViewState["OACTIVITYLOG"] as Products;
            }
            set
            {
                ViewState["OACTIVITYLOG"] = value;
            }
        }

        public int DataID
        {
            get
            {
                if (ViewState["DATA_ID"] == null)
                {
                    return 0;
                }
                return Commons.GetInt(ViewState["DATA_ID"]);
            }
            set
            {
                ViewState["DATA_ID"] = value;
            }
        }

        public WebGridView GridViewList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {   
                TextDiscontinued.Style["display"] = "none";
                TextCategoryID.Style["display"] = "none";

                var facade = new CategoryFacade();
                ComboCategory.DataSource = facade.PageDataAll();
                
                ComboCategory.DataTextField = "CategoryName";
                ComboCategory.DataValueField = "CategoryID";
                ComboCategory.DataBind();
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            if (IsValidOnSave())
            {
                ProcessSave();
            }
        }

        public void ProcessDetail()
        {
            switch (PageState)
            {
                case PageState.New:
                    ProcessNew();
                    break;
                case PageState.Edit:
                    ProcessEdit();
                    break;
                case PageState.View:
                    ProcessView();
                    break;
                default:
                    break;
            }
        }

        private void ProcessNew()
        {
            DataReadInNewState();
            PopulateControlsInNewState();
            AdjustControlInNewState();
        }

        private void DataReadInNewState()
        {
            OProduct = new Products();
        }

        private void PopulateControlsInNewState()
        {
            TextProductID.Text = "";
            TextProductName.Text = "";
            TextSupplier.Text = "";
            TextCategoryID.Text = "";
            TextQuantityPerUnit.Text = "";
            TextUnitPrice.Text = "";
            TextUnitsInStock.Text = "";
            TextUnitsOnOrder.Text = "";
            TextReorderLevel.Text = "";
            TextDiscontinued.Text = "";

            ComboDiscontinued.SelectedValue = "False";


        }

        private void AdjustControlInNewState()
        {
            TextProductID.ReadOnly = false;
            TextProductName.ReadOnly = false;
            TextCategoryID.ReadOnly = false;
            TextQuantityPerUnit.ReadOnly = false;
            TextUnitPrice.ReadOnly = false;
            TextUnitsInStock.ReadOnly = false;
            TextUnitsOnOrder.ReadOnly = false;
            TextReorderLevel.ReadOnly = false;
            TextDiscontinued.ReadOnly = false;
            ComboDiscontinued.Enabled = true;

            ButtonSave.Visible = true;
        }

        private void ProcessEdit()
        {
            DataReadInEditState();
            PopulateControlsInEditState();
            AdjustControlInEditState();
        }

        private void DataReadInEditState()
        {
            var facade = new ProductFacade();
            OProduct = facade.GetObject(DataID);
        }

        private void PopulateControlsInEditState()
        {
            if (OProduct != null)
            {


                // Get Supplier
                var SupplierFcd = new SupplierFacade();
                SelectedSuppliers = SupplierFcd.GetObject(OProduct.SupplierID);
                
                // Pull Text
                TextProductID.Text = Convert.ToString(OProduct.ProductID);
                TextProductName.Text = OProduct.ProductName;
                TextCategoryID.Text = Convert.ToString(OProduct.CategoryID);
                TextQuantityPerUnit.Text = OProduct.QuantityPerUnit;
                TextUnitPrice.Text = Convert.ToString(OProduct.UnitPrice);
                TextUnitsInStock.Text = Convert.ToString(OProduct.UnitsInStock);
                TextUnitsOnOrder.Text = Convert.ToString(OProduct.UnitsOnOrder);
                TextReorderLevel.Text = Convert.ToString(OProduct.ReorderLevel);
                TextDiscontinued.Text = Convert.ToString(OProduct.Discontinued);

                ComboDiscontinued.SelectedValue = Convert.ToString(OProduct.Discontinued);


                // If supplier found
                if (SelectedSuppliers != null)
                {
                    TextSupplier.Text = SelectedSuppliers.CompanyName;
                }
                // If supplier not found
                else
                {
                    TextSupplier.Text = "";
                }
            }
        }

        private void AdjustControlInEditState()
        {
            AdjustControlInNewState();

            ButtonSave.Visible = true;
        }

        private void ProcessView()
        {
            DataReadInViewState();
            PopulateControlsInViewState();
            AdjustControlInViewState();
        }

        private void DataReadInViewState()
        {
            var facade = new ProductFacade();
            OProduct = facade.GetObject(DataID);
        }

        private void PopulateControlsInViewState()
        {
            if (OProduct != null)
            {

                // Get Supplier
                var SupplierFcd = new SupplierFacade();
                SelectedSuppliers = SupplierFcd.GetObject(OProduct.SupplierID);


                // Pull Text
                TextProductID.Text = Convert.ToString(OProduct.ProductID);
                TextProductName.Text = OProduct.ProductName;
                TextCategoryID.Text = Convert.ToString(OProduct.CategoryID);
                TextQuantityPerUnit.Text = OProduct.QuantityPerUnit;
                TextUnitPrice.Text = Convert.ToString(OProduct.UnitPrice);
                TextUnitsInStock.Text = Convert.ToString(OProduct.UnitsInStock);
                TextUnitsOnOrder.Text = Convert.ToString(OProduct.UnitsOnOrder);
                TextReorderLevel.Text = Convert.ToString(OProduct.ReorderLevel);
                TextDiscontinued.Text = Convert.ToString(OProduct.Discontinued);

                // If supplier found
                if (SelectedSuppliers != null)
                {
                    TextSupplier.Text = SelectedSuppliers.CompanyName;
                }
                // If supplier not found
                else
                {
                    TextSupplier.Text = "";
                }



                ComboDiscontinued.SelectedValue = Convert.ToString(OProduct.Discontinued);
            }
        }

        private void AdjustControlInViewState()
        {
            TextProductID.ReadOnly = true;
            TextProductName.ReadOnly = true;
            TextCategoryID.ReadOnly = true;
            TextQuantityPerUnit.ReadOnly = true;
            TextUnitPrice.ReadOnly = true;
            TextUnitsInStock.ReadOnly = true;
            TextUnitsOnOrder.ReadOnly = true;
            TextReorderLevel.ReadOnly = true;
            TextDiscontinued.ReadOnly = true;
            ComboDiscontinued.Enabled = false;


            ButtonSave.Visible = false;
        }

        private void ProcessSave()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ProductFacade();
                ActionResult result = null;
                PopulateObject();
                if (PageState == PageState.New)
                {
                    result = facade.DataInsert(OProduct);
                }
                else
                {
                    result = facade.DataUpdate(OProduct);
                }
                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage, "$('#productdetail').modal('hide')");
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private void PopulateObject()
        {
            try
            {
                OProduct.ProductID = Convert.ToInt32(TextProductID.Text);
                OProduct.ProductName = TextProductName.Text;
                OProduct.CategoryID = Convert.ToInt32(TextCategoryID.Text);
                OProduct.QuantityPerUnit = TextQuantityPerUnit.Text;
                OProduct.UnitPrice = Convert.ToInt32(TextUnitPrice.Text);
                OProduct.UnitsInStock = Convert.ToInt32(TextUnitsInStock.Text);
                OProduct.UnitsOnOrder = Convert.ToInt32(TextUnitsOnOrder.Text);
                OProduct.ReorderLevel = Convert.ToInt32(TextReorderLevel.Text);
                OProduct.Discontinued = Convert.ToBoolean(TextDiscontinued.Text);

            }
            catch (Exception ex) { }
        }

        private bool IsValidOnSave()
        {
            var msgList = new List<ErrorMessage>();
            string errorMsg = string.Empty;


            if (string.IsNullOrEmpty(TextProductName.Text)) { msgList.Add(new ErrorMessage(TextProductName.ClientID, "ProductName harus diisi!")); }
            if (string.IsNullOrEmpty(TextSupplier.Text)) { msgList.Add(new ErrorMessage(TextSupplier.ClientID, "Supplier harus diisi!")); }
            if (string.IsNullOrEmpty(TextCategoryID.Text)) { msgList.Add(new ErrorMessage(TextCategoryID.ClientID, "CategoryID harus diisi!")); }
            if (string.IsNullOrEmpty(TextQuantityPerUnit.Text)) { msgList.Add(new ErrorMessage(TextQuantityPerUnit.ClientID, "QuantityPerUnit harus diisi!")); }
            if (string.IsNullOrEmpty(TextUnitPrice.Text)) { msgList.Add(new ErrorMessage(TextUnitPrice.ClientID, "UnitPrice harus diisi!")); }
            if (string.IsNullOrEmpty(TextUnitsInStock.Text)) { msgList.Add(new ErrorMessage(TextUnitsInStock.ClientID, "UnitsInStock harus diisi!")); }
            if (string.IsNullOrEmpty(TextUnitsOnOrder.Text)) { msgList.Add(new ErrorMessage(TextUnitsOnOrder.ClientID, "UnitsOnOrder harus diisi!")); }
            if (string.IsNullOrEmpty(TextReorderLevel.Text)) { msgList.Add(new ErrorMessage(TextReorderLevel.ClientID, "ReorderLevel harus diisi!")); }
            if (string.IsNullOrEmpty(TextDiscontinued.Text)) { msgList.Add(new ErrorMessage(TextDiscontinued.ClientID, "Discontinued harus diisi!")); }



            if (msgList.Count == 0)
            {
                return true;
            }
            else
            {
                Message.ShowErrorMessage(this.Page, msgList);
                return false;
            }
        }

        private void ProcessDelete()
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ProductFacade();
                ActionResult result = facade.DataDelete(DataID);
                if (result.ActionStatus)
                {
                    Message.Show(this.Page, result.ActionMessage);
                    GridViewList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        }

        protected void ComboDiscontinued_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TextDiscontinued.Text != ComboDiscontinued.SelectedValue)
                TextDiscontinued.Text = ComboDiscontinued.SelectedValue;
        }

        protected void ComboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (TextCategoryID.Text != ComboCategory.SelectedValue)
                TextCategoryID.Text = ComboCategory.SelectedValue;
        }

        protected void ButtonFindSupplier_Click(object sender, EventArgs e)
        {
            
            Message.RunScript(this.Parent.Page, "$('#productsupplier').modal()");
        }

        protected void Supplier_OnSelectedChanged(object sender, EventArgs e)
        {
            TextSupplier.Text = ProductsSupplier1.SelectedSuppliers.CompanyName;
            OProduct.SupplierID = ProductsSupplier1.SelectedSuppliers.SupplierID;
        }
    }
}