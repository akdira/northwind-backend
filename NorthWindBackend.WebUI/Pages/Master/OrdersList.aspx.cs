﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.Facade;
using NorthWindBackend.Common;
using NorthWindBackend.WebUI.Pages.Master;
using NorthWindBackend.WebUI.Pages.MasterPages;
using PetaPoco;

namespace NorthWindBackend.WebUI.Pages.Master
{
    public partial class OrdersList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            (Master as Main).SetModuleName("Master");
            GridOrdersList.PageFunction = PageFunction;
            

            if (!Page.IsPostBack)
            {
                GridOrdersList.DisplayData();
            }
        }

        private PetaPoco.Page PageFunction()
        {
            var grid = GridOrdersList;
            var facade = new OrderFacade();
            return facade.PageDataByCriteria(grid.GetSqlSearchCriterias(), grid.GridView.PageIndex + 1, grid.GridView.PageSize, grid.SortKey);
        }

        protected void ButtonView_Click(object sender, EventArgs e)
        {
            var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            Response.Redirect("OrderDetailsList.aspx?OrderID=" + dataID + "&PageState=View", true);

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
            Response.Redirect("OrderDetailsList.aspx?OrderID=" + dataID + "&PageState=Edit", true);
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (ValidOnDelete())
            {
                var dataID = Commons.GetInt((sender as LinkButton).CommandArgument);
                ProcessDelete(dataID);
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {

        }

        public void ProcessDelete(int dataID)
        {
            string errorMsg = string.Empty;
            try
            {
                var facade = new ActivityLogFacade();
                ActionResult result = facade.DataDelete(dataID);
                if (result.ActionStatus)
                {
                    Message.Show(this, result.ActionMessage);
                    GridOrdersList.DisplayData();
                }
                else
                {
                    Message.Show(this.Page, result.ErrorMessage);
                }
            }
            catch (Exception ex) { }
        }

        private bool ValidOnDelete()
        {
            return true;
        }

    }
}