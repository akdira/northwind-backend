﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderDetailsProduct.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.OrderDetailsProduct" %>

<asp:UpdatePanel ID="UpdateSupllier" runat="server">
    <ContentTemplate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Product Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Product List</h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <grv:WebGridView ID="GridOrderDetailsProduct" runat="server" AllowPaging="true" DefaultSortExpression="ProductID"
                                            DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ButtonChooseProduct" runat="server" Style="margin-right: 5px;" OnClick="ButtonChooseProduct_Click" CommandArgument='<%# Eval("ProductID") %>'><i class="fa fa-check" ></i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="Product ID" DataField="ProductID" SortExpression="ProductID">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Product Name" DataField="ProductName" SortExpression="ProductName">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Supplier" DataField="Supplier" SortExpression="Supplier">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Category ID" DataField="Category" SortExpression="Category">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Quantity Per Unit" DataField="QuantityPerUnit" SortExpression="QuantityPerUnit">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Unit Price" DataField="UnitPrice" SortExpression="UnitPrice">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Units In Stock" DataField="UnitsInStock" SortExpression="UnitsInStock">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Units On Stock" DataField="UnitsOnOrder" SortExpression="UnitsOnOrder">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Reorder Level" DataField="ReorderLevel" SortExpression="ReorderLevel">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Discontinued" DataField="Discontinued" SortExpression="Discontinued">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>
                                            </Columns>
                                        </grv:WebGridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
