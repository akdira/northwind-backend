﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrdersEmployee.ascx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.OrdersEmployee" %>

<asp:UpdatePanel ID="UpdateSupllier" runat="server">
    <ContentTemplate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-briefcase"></i>&nbsp;Employee Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Employee List</h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-md-12">
                                        <grv:WebGridView ID="GridOrdersEmployee" runat="server" AllowPaging="true" DefaultSortExpression="EmployeeID"
                                            DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ButtonChooseEmployee" runat="server" Style="margin-right: 5px;" OnClick="ButtonChooseEmployee_Click" CommandArgument='<%# Eval("EmployeeID") %>'><i class="fa fa-check" ></i> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField HeaderText="EmployeeID" DataField="EmployeeID" SortExpression="EmployeeID">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="FirstName" DataField="FirstName" SortExpression="FirstName">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="LastName" DataField="LastName" SortExpression="LastName">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="BirthDate" DataField="BirthDate" SortExpression="BirthDate">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                                <asp:BoundField HeaderText="HireDate" DataField="HireDate" SortExpression="HireDate">
                                                    <HeaderStyle Width="20.0%" />
                                                    <ItemStyle Width="20.0%" />
                                                </asp:BoundField>

                                            </Columns>
                                        </grv:WebGridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
