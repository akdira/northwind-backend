﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/MasterPages/Main.Master" AutoEventWireup="true" CodeBehind="OrderDetailsList.aspx.cs" Inherits="NorthWindBackend.WebUI.Pages.Master.OrderDetailsList" %>
<%@ Register Src="~/Pages/Master/OrdersCustomer.ascx" TagName="OrdersCustomer" TagPrefix="dtl" %>
<%@ Register Src="~/Pages/Master/OrdersEmployee.ascx" TagName="OrdersEmployee" TagPrefix="dtl" %>
<%@ Register Src="~/Pages/Master/OrderDetailItem.ascx" TagName="OrderDetailItem" TagPrefix="dtl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            setActiveMenu('Orders List', 3);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Order Data</h3>
                    </div>
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-primary btn-sm" OnClick="ButtonAdd_Click"><i class="fa fa-plus">&nbsp;</i>Tambah</asp:LinkButton>
                            <asp:LinkButton ID="ButtonSave" runat="server" CssClass="btn btn-primary" OnClick="ButtonSave_Click"><i class="fa fa-save">&nbsp;</i>Save</asp:LinkButton>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-6 form-horizontal">

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">OrderID </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextOrderID" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Automatic"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Customer </label>
                                        <div class="col-md-8">
                                            <%--<input type="hidden" name="HiddenCustomerID" id="HiddenCustomerID" runat="server" />--%>

                                            <div class="input-group">
                                                <%--<asp:TextBox ID="TextCustomerID" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>--%>
                                                <asp:TextBox ID="TextCustomer" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Harus diisi"></asp:TextBox>
                                                <span class="input-group-btn">
                                                    <asp:LinkButton ID="ButtonFindCustomer" CssClass="btn btn-secondary btn-success" runat="server" OnClick="ButtonFindCustomer_Click"><span class="fa fa-search"></span> </asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Employee </label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <%--<asp:TextBox ID="TextEmployeeID" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>--%>
                                                <asp:TextBox ID="TextEmployee" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Harus diisi"></asp:TextBox>
                                                <span class="input-group-btn">
                                                    <asp:LinkButton ID="ButtonFindEmployee" CssClass="btn btn-secondary btn-success" runat="server" OnClick="ButtonFindEmployee_Click"><span class="fa fa-search"></span> </asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">OrderDate </label>
                                        <div class="col-md-8">
                                            <div class="input-group date">
                                                <asp:TextBox ID="TextOrderDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">RequiredDate </label>
                                        <div class="col-md-8">
                                            <div class="input-group date">
                                                <asp:TextBox ID="TextRequiredDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShippedDate </label>
                                        <div class="col-md-8">
                                            <div class="input-group date">
                                                <asp:TextBox ID="TextShippedDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipVia </label>
                                        <div class="col-md-8">
                                            <asp:DropDownList ID="ComboShipVia" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ComboShipVia_SelectedIndexChanged">
                                                <asp:ListItem Value="" Text="-- Please Select --" />
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                </div>
                                <div class="col-md-6 form-horizontal">



                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">Freight </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextFreight" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipName </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextShipName" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipAddress </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextShipAddress" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipCity </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextShipCity" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipRegion </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextShipRegion" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipPostalCode </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextShipPostalCode" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="text-align: left">ShipCountry </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="TextShipCountry" runat="server" CssClass="form-control" placeholder="Harus diisi"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <grv:WebGridView ID="GridOrderDetailsList" runat="server" AllowPaging="true" DefaultSortExpression="OrderID"
                                DefaultSortDirection="ASC" ShowTotalRecords="true" AllowSearch="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="ButtonView" runat="server" Style="margin-right: 5px;" OnClick="ButtonView_Click" CommandArgument='<%# Eval("ProductID") %>'><i class="fa fa-search" ></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonEdit" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("ProductID") %>' OnClick="ButtonEdit_Click"><i class="fa fa-edit" style="color: green"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="ButtonDelete" runat="server" Style="margin-right: 5px;" CommandArgument='<%# Eval("ProductID") %>' OnClick="ButtonDelete_Click" OnClientClick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i style="color: red" class="fa fa-remove"></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField HeaderText="Product Name" DataField="ProductName" SortExpression="ProductName">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="UnitPrice" DataField="UnitPrice" SortExpression="UnitPrice">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Quantity" DataField="Quantity" SortExpression="Quantity">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Discount" DataField="Discount" SortExpression="Discount">
                                        <HeaderStyle Width="20.0%" />
                                        <ItemStyle Width="20.0%" />
                                    </asp:BoundField>

                                </Columns>
                            </grv:WebGridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="orderscustomer" class="modal fade" role="dialog">
        <dtl:OrdersCustomer ID="OrdersCustomer1" runat="server" OnSelected_Changed="Customer_OnSelectedChanged" />
    </div>

    <div id="ordersemployee" class="modal fade" role="dialog">
        <dtl:OrdersEmployee ID="OrdersEmployee1" runat="server" OnSelected_Changed="Employee_OnSelectedChanged" />
    </div>

    <div id="orderdetailitem" class="modal fade" role="dialog">
        <dtl:OrderDetailItem ID="OrderDetailItem1" runat="server" OnFormSubmit_Add="Item_FormSubmit_Add" OnFormSubmit_Edit="Item_FormSubmit_Edit" />
    </div>


</asp:Content>

