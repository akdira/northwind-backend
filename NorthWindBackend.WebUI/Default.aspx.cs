﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NorthWindBackend.Entity;
using NorthWindBackend.Common;
using NorthWindBackend.Facade;
using System.DirectoryServices;
using System.Web.Security;

namespace NorthWindBackend.WebUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[Constants.USER_ID] != null)
            {
                Response.Redirect("~/Pages/Home/Dashboard.aspx");
            }
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (string.IsNullOrEmpty(Login1.UserName))
            {
                Login1.FailureText = "User ID is required!";
            }
            else if (string.IsNullOrEmpty(Login1.Password))
            {
                Login1.FailureText = "Password is required!";
            }
            else if (!IsUserExist(Login1.UserName))
            {
                Login1.FailureText = "User is not registered!";
            }
            else if (IsUserLogedIn(Login1.UserName))
            {
                Login1.FailureText = "User is already login on other machine!";
            }
            else
            {
                if (ProcessAuthenticate())
                {
                    e.Authenticated = true;
                    CreateSession();
                }
                else
                {
                    e.Authenticated = false;
                }
            }
        }

        private bool ProcessAuthenticate()
        {
            if (Commons.GetConfigValue("IsByPassLogin") == "1")
            {
                return IsUserExist(Login1.UserName);
            }
            else if (Commons.GetConfigValue("IsADLogin") == "1")
            {
                return VerifyUserAD(Login1.UserName, Login1.Password);
            }
            else
            {
                return VerifyUserCustom(Login1.UserName, Login1.Password);
            }

        }

        private bool IsUserExist(string userId)
        {
            var uFacade = new UsersFacade();
            var user = uFacade.GetUsersByID(userId);
            return user != null;
        }

        private bool IsUserLogedIn(string userId)
        {
            bool isLogedIn = false;
            if(Commons.GetConfigValue("IsMultipleLogin") == "1")
            {
                isLogedIn = false;
            }
            else
            {
                var facade = new LoginUserFacade();
                var loginUser = facade.GetObject("UserName", userId);
                if (loginUser == null) 
                {
                    isLogedIn = false;
                }
                else
                {
                    if(Request.UserHostAddress == loginUser.HostAddress)
                    {
                        isLogedIn = false;
                    }
                    else
                    {
                        isLogedIn = true;
                    }
                }
            }
            return isLogedIn;
        }

        private bool VerifyUserAD(string userName, string password)
        {
            DirectoryEntry d;
            DirectorySearcher s;
            d = new DirectoryEntry(Commons.GetConfigValue("ADAddress"), Login1.UserName.Trim(), Login1.Password.Trim());
            s = new DirectorySearcher(d);
            s.Filter = string.Format("(&(objectClass=user)(objectCategory=person)(sAMAccountName={0}))", Login1.UserName.Trim());
            try
            {
                SearchResult sr = s.FindOne();
                if (sr != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        private bool VerifyUserCustom(string userName, string password)
        {
            var userFacade = new UsersFacade();
            var LoginUser = userFacade.GetUsersByID(Login1.UserName);
            if (LoginUser != null)
            {
                string cryptPassword = userFacade.GetUserVerifyPassword(password);
                return LoginUser.UserPassword.Equals(cryptPassword);
            }
            return false;
        }

        private void CreateSession()
        {
            var userFacade = new UsersFacade();
            var LoginUser = userFacade.GetUsersByID(Login1.UserName);
            var RoleUser = new RoleFacade().GetUserRole(LoginUser.UserID); 
            userFacade.Update(LoginUser);
            Session[Constants.DOMAIN_NAME] = Commons.GetConfigValue("DomainName");
            Session[Constants.USER_ID] = LoginUser.UserID;
            Session[Constants.MENU_SESSION] = null;
            Session[Constants.CURRENT_USER] = LoginUser;
            Session[Constants.CURRENT_USER_NAME] = LoginUser.UserName;
            Session[Constants.USER_ROLE] = RoleUser;

            var oFacade = new LoginLogFacade();
            var loginLog = new LoginLog()
            {
                LoginDescription = "User " + LoginUser.UserName + " has login at " + DateTime.Now.ToString(),
                LoginIP = Request.UserHostAddress,
                LoginTime = DateTime.Now,
                LoginUser = LoginUser.UserName
            };
            oFacade.Insert(loginLog);

            var lFacade = new LoginUserFacade();
            var loginUser = new LoginUser()
            {
                UserName = Login1.UserName,
                SessionID = Session.SessionID,
                HostAddress = Request.UserHostAddress
            };
            lFacade.InsertLoginUser(loginUser);

            Session[Constants.LOGIN_LOG] = loginLog; 
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            FormsAuthentication.RedirectFromLoginPage(Login1.UserName, false); 
        }
    }
}