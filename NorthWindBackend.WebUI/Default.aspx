﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NorthWindBackend.WebUI.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Aladdin Printing System :: Login</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <link href="Clients/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Clients/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="Clients/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="Clients/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="Clients/dist/css/skins/skin-green.css" rel="stylesheet" />
    <link href="Clients/plugins/iCheck/square/blue.css" rel="stylesheet" />
    <link href="Clients/plugins/datepicker/datepicker3.css" rel="stylesheet" />
    <style type="text/css">
 
    </style>
</head>
<body class="hold-transition login-page">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate" OnLoggedIn="Login1_LoggedIn" CssClass="login-box">
                    <LayoutTemplate>
                        <div >
                            <div class="login-logo">
                                <a href="#"><b style="color: green">Aladdin</b><span style="font-size: 30px">
                                    <span style="color: #367FA9; font-weight: 400">Printing</span> <span style="color: maroon">System</span></span>
                                </a>
                            </div>
                            <!-- /.login-logo -->
                            <i style="font-size: 12px; float: right; margin-top: -20px; margin-right: 10px;">version 1.0</i>
                            <div class="login-box-body">
                                <p class="login-box-msg" style="font-size: 18px;">Sign in to start your session</p>
                                <div class="form-group has-feedback"> 
                                    <asp:TextBox ID="UserName" runat="server" CssClass="form-control" placeholder="User ID"/>
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback"> 
                                    <asp:TextBox ID="Password" runat="server" CssClass="form-control" TextMode="Password" placeholder="Password"/>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="checkbox">
                                            <label style="display: none">
                                                <input type="checkbox" class="check-remember" />
                                                Remember Me 
                                            </label>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-xs-4"> 
                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="Login1" CssClass="btn btn-primary btn-block btn-flat" Text="Sign In"/>
                                    </div>
                                    <div class="col-xs-12" style="color: red">
                                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    </LayoutTemplate>
                </asp:Login>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>

    <script src="Clients/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="Clients/plugins/jQueryUI/jquery-ui.min.js"></script>
    <script src="Clients/bootstrap/js/bootstrap.min.js"></script>
    <script src="Clients/plugins/iCheck/icheck.min.js"></script>
    <script src="Clients/dist/js/app.min.js"></script>
    <script>
        //$(function () {
        //    $('input').iCheck({
        //        checkboxClass: 'icheckbox_square-blue',
        //        radioClass: 'iradio_square-blue',
        //        increaseArea: '20%' // optional
        //    });
        //});
    </script>
</body>
</html>
