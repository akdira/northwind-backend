﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NorthWindBackend.Helper
{
    public static class EnumerableExtension
    {
        public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> items, string property, string direction)
        {
            bool ascending = direction.ToUpper() == "ASC";
            var myObject = System.Linq.Expressions.Expression.Parameter(typeof(T), "Object");
            var myEnumeratedObject = System.Linq.Expressions.Expression.Parameter(typeof(IEnumerable<T>), "EnumeratedObject");
            var myProperty = System.Linq.Expressions.Expression.Property(myObject, property);
            var myLambda = System.Linq.Expressions.Expression.Lambda(myProperty, myObject);
            var myMethod = System.Linq.Expressions.Expression.Call(typeof(Enumerable), ascending ? "OrderBy" : "OrderByDescending", new[] { typeof(T), myLambda.Body.Type }, myEnumeratedObject, myLambda);
            var mySortedLambda = System.Linq.Expressions.Expression.Lambda<Func<IEnumerable<T>, IOrderedEnumerable<T>>>(myMethod, myEnumeratedObject).Compile();
            return mySortedLambda(items);
        }
    }
}
